(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("42fa1fe3-d0bc-401e-872d-59c77952ea35");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '42fa1fe3-d0bc-401e-872d-59c77952ea35' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"9b0c7a5c-8a6f-4877-b970-6bec67a90e85":{"roots":{"references":[{"attributes":{"formatter":{"id":"9440e636-a53b-467e-a1bf-b5772347aae3","type":"BasicTickFormatter"},"plot":{"id":"3aa63f22-799c-44dc-8058-d8747bc77c57","subtype":"Figure","type":"Plot"},"ticker":{"id":"97e927ab-32ec-40ef-95da-c7f2b0fd9831","type":"BasicTicker"},"visible":false},"id":"ad3c7842-9e15-42bf-b33e-c169a75a5e78","type":"LinearAxis"},{"attributes":{"plot":null,"text":""},"id":"7c5c9752-2b6b-4e2c-a88f-82bdbbe5edd8","type":"Title"},{"attributes":{"dimension":1,"plot":{"id":"3aa63f22-799c-44dc-8058-d8747bc77c57","subtype":"Figure","type":"Plot"},"ticker":{"id":"acf8163e-237e-423b-88b7-679fdbc98c17","type":"BasicTicker"},"visible":false},"id":"c0b37d12-683f-4a09-a345-cddfe5a95751","type":"Grid"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"865ced52-b4e8-489f-bf23-eea7d0560cc2","type":"Toolbar"},{"attributes":{"args":{"source":{"id":"7e211e1d-aa27-4b30-9734-395c83ea3f9c","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"0db63ed4-fd02-4193-9607-df468120c57c","type":"CustomJS"},{"attributes":{},"id":"0babe553-fe6a-4574-af53-a6947a41c42d","type":"BasicTickFormatter"},{"attributes":{"callback":{"id":"0db63ed4-fd02-4193-9607-df468120c57c","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"17d04849-709d-4086-8fe2-2aee3e5e8242","type":"Slider"},{"attributes":{"data_source":{"id":"7e211e1d-aa27-4b30-9734-395c83ea3f9c","type":"ColumnDataSource"},"glyph":{"id":"8e2c378b-6b47-44ea-afa4-e77e558585bb","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"72241d45-bd40-473d-bfd5-f70a6567b6e4","type":"ImageURL"},"selection_glyph":null,"view":{"id":"ecb2ae27-1025-4140-b749-d368ea62a702","type":"CDSView"}},"id":"8032db19-ec7f-4b28-863e-c2be935ac6f0","type":"GlyphRenderer"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"72241d45-bd40-473d-bfd5-f70a6567b6e4","type":"ImageURL"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/TI2O4_000.png"],"url_num":["000"]},"selected":{"id":"f6539e4f-3302-4e9b-ae4f-a2809a341e69","type":"Selection"},"selection_policy":{"id":"7df422d0-0957-44ef-95a1-629c1ae18ac8","type":"UnionRenderers"}},"id":"7e211e1d-aa27-4b30-9734-395c83ea3f9c","type":"ColumnDataSource"},{"attributes":{"callback":null},"id":"7c8c27b1-c738-4302-9236-d1b563c34997","type":"Range1d"},{"attributes":{},"id":"f6539e4f-3302-4e9b-ae4f-a2809a341e69","type":"Selection"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"8e2c378b-6b47-44ea-afa4-e77e558585bb","type":"ImageURL"},{"attributes":{"below":[{"id":"ad3c7842-9e15-42bf-b33e-c169a75a5e78","type":"LinearAxis"}],"left":[{"id":"3a192fac-b6e4-4147-96fe-1aae6fc16561","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"ad3c7842-9e15-42bf-b33e-c169a75a5e78","type":"LinearAxis"},{"id":"53e00976-1824-4399-b7ec-bc705b9d491e","type":"Grid"},{"id":"3a192fac-b6e4-4147-96fe-1aae6fc16561","type":"LinearAxis"},{"id":"c0b37d12-683f-4a09-a345-cddfe5a95751","type":"Grid"},{"id":"8032db19-ec7f-4b28-863e-c2be935ac6f0","type":"GlyphRenderer"}],"title":{"id":"7c5c9752-2b6b-4e2c-a88f-82bdbbe5edd8","type":"Title"},"toolbar":{"id":"865ced52-b4e8-489f-bf23-eea7d0560cc2","type":"Toolbar"},"x_range":{"id":"0d5002ba-5157-4f19-9aa9-17218e3e36a6","type":"Range1d"},"x_scale":{"id":"17d6f585-3b27-40e6-9ce1-1c066e837e08","type":"LinearScale"},"y_range":{"id":"7c8c27b1-c738-4302-9236-d1b563c34997","type":"Range1d"},"y_scale":{"id":"7c27249e-9012-4cb6-b84d-c412250106d5","type":"LinearScale"}},"id":"3aa63f22-799c-44dc-8058-d8747bc77c57","subtype":"Figure","type":"Plot"},{"attributes":{"plot":{"id":"3aa63f22-799c-44dc-8058-d8747bc77c57","subtype":"Figure","type":"Plot"},"ticker":{"id":"97e927ab-32ec-40ef-95da-c7f2b0fd9831","type":"BasicTicker"},"visible":false},"id":"53e00976-1824-4399-b7ec-bc705b9d491e","type":"Grid"},{"attributes":{"children":[{"id":"17d04849-709d-4086-8fe2-2aee3e5e8242","type":"Slider"}]},"id":"802171d6-78d5-4d1d-a1c8-1fca1a4fa60d","type":"WidgetBox"},{"attributes":{"callback":null},"id":"0d5002ba-5157-4f19-9aa9-17218e3e36a6","type":"Range1d"},{"attributes":{"children":[{"id":"802171d6-78d5-4d1d-a1c8-1fca1a4fa60d","type":"WidgetBox"},{"id":"3aa63f22-799c-44dc-8058-d8747bc77c57","subtype":"Figure","type":"Plot"}]},"id":"b4db51cc-eccd-48da-b31d-bc6a9010ccc1","type":"Column"},{"attributes":{},"id":"7c27249e-9012-4cb6-b84d-c412250106d5","type":"LinearScale"},{"attributes":{},"id":"17d6f585-3b27-40e6-9ce1-1c066e837e08","type":"LinearScale"},{"attributes":{"formatter":{"id":"0babe553-fe6a-4574-af53-a6947a41c42d","type":"BasicTickFormatter"},"plot":{"id":"3aa63f22-799c-44dc-8058-d8747bc77c57","subtype":"Figure","type":"Plot"},"ticker":{"id":"acf8163e-237e-423b-88b7-679fdbc98c17","type":"BasicTicker"},"visible":false},"id":"3a192fac-b6e4-4147-96fe-1aae6fc16561","type":"LinearAxis"},{"attributes":{},"id":"acf8163e-237e-423b-88b7-679fdbc98c17","type":"BasicTicker"},{"attributes":{},"id":"7df422d0-0957-44ef-95a1-629c1ae18ac8","type":"UnionRenderers"},{"attributes":{"source":{"id":"7e211e1d-aa27-4b30-9734-395c83ea3f9c","type":"ColumnDataSource"}},"id":"ecb2ae27-1025-4140-b749-d368ea62a702","type":"CDSView"},{"attributes":{},"id":"9440e636-a53b-467e-a1bf-b5772347aae3","type":"BasicTickFormatter"},{"attributes":{},"id":"97e927ab-32ec-40ef-95da-c7f2b0fd9831","type":"BasicTicker"}],"root_ids":["b4db51cc-eccd-48da-b31d-bc6a9010ccc1"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"9b0c7a5c-8a6f-4877-b970-6bec67a90e85","roots":{"b4db51cc-eccd-48da-b31d-bc6a9010ccc1":"42fa1fe3-d0bc-401e-872d-59c77952ea35"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();