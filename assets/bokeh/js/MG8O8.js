(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("2223f78c-ccea-4ed6-b240-75da9b17c87f");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '2223f78c-ccea-4ed6-b240-75da9b17c87f' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"7e9f1c9b-d5a9-4fe4-8a9d-c0ef4d9b21ea":{"roots":{"references":[{"attributes":{"below":[{"id":"84fa4661-2557-47db-bdb7-a0fc127d2c90","type":"LinearAxis"}],"left":[{"id":"2b73ce1a-2e82-4a1c-8140-4a2103bed88d","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"84fa4661-2557-47db-bdb7-a0fc127d2c90","type":"LinearAxis"},{"id":"26005f8d-d7ab-4dd0-899d-3ae7860db099","type":"Grid"},{"id":"2b73ce1a-2e82-4a1c-8140-4a2103bed88d","type":"LinearAxis"},{"id":"fcaa2a99-f6d6-4553-8382-bf73326ac5c4","type":"Grid"},{"id":"736538aa-e504-43fe-8ba7-f131c5c13fa7","type":"GlyphRenderer"}],"title":{"id":"f62035a6-4acd-4e2a-be5f-9cff408c34c0","type":"Title"},"toolbar":{"id":"6883b8bc-f1c9-4110-ad1a-9bb871f64778","type":"Toolbar"},"x_range":{"id":"a73c89b0-7b3d-4433-8fe2-5000c1f39f0f","type":"Range1d"},"x_scale":{"id":"094db4e1-34a1-47ec-a995-c114391a322f","type":"LinearScale"},"y_range":{"id":"286d2099-4f64-4056-a69e-e583a4ecc3e3","type":"Range1d"},"y_scale":{"id":"c557feb4-4779-42dc-9c99-9f703e8ff68c","type":"LinearScale"}},"id":"c05877ee-e28b-4381-83b3-c7cf1da7859b","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"c557feb4-4779-42dc-9c99-9f703e8ff68c","type":"LinearScale"},{"attributes":{"data_source":{"id":"5f9e3663-e057-4dcc-80b0-147a517462ae","type":"ColumnDataSource"},"glyph":{"id":"60c31cf8-2e84-458b-853e-8372546be18e","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"00c68f57-6726-4688-a563-75d041a47745","type":"ImageURL"},"selection_glyph":null,"view":{"id":"e90e797f-1345-46b2-92dc-5266fbf97681","type":"CDSView"}},"id":"736538aa-e504-43fe-8ba7-f131c5c13fa7","type":"GlyphRenderer"},{"attributes":{"formatter":{"id":"dc63803a-459e-430b-9309-80ca34997336","type":"BasicTickFormatter"},"plot":{"id":"c05877ee-e28b-4381-83b3-c7cf1da7859b","subtype":"Figure","type":"Plot"},"ticker":{"id":"9138679b-3e5e-4e6b-b617-73eeef4f48fc","type":"BasicTicker"},"visible":false},"id":"2b73ce1a-2e82-4a1c-8140-4a2103bed88d","type":"LinearAxis"},{"attributes":{"callback":null},"id":"286d2099-4f64-4056-a69e-e583a4ecc3e3","type":"Range1d"},{"attributes":{"dimension":1,"plot":{"id":"c05877ee-e28b-4381-83b3-c7cf1da7859b","subtype":"Figure","type":"Plot"},"ticker":{"id":"9138679b-3e5e-4e6b-b617-73eeef4f48fc","type":"BasicTicker"},"visible":false},"id":"fcaa2a99-f6d6-4553-8382-bf73326ac5c4","type":"Grid"},{"attributes":{"source":{"id":"5f9e3663-e057-4dcc-80b0-147a517462ae","type":"ColumnDataSource"}},"id":"e90e797f-1345-46b2-92dc-5266fbf97681","type":"CDSView"},{"attributes":{"args":{"source":{"id":"5f9e3663-e057-4dcc-80b0-147a517462ae","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"03dfbf7c-e6b8-4717-b810-26eb2f367002","type":"CustomJS"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"6883b8bc-f1c9-4110-ad1a-9bb871f64778","type":"Toolbar"},{"attributes":{},"id":"094db4e1-34a1-47ec-a995-c114391a322f","type":"LinearScale"},{"attributes":{"callback":{"id":"03dfbf7c-e6b8-4717-b810-26eb2f367002","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"84baa7f4-31a4-46c8-8264-4cb6e37c907c","type":"Slider"},{"attributes":{"children":[{"id":"efc23ce2-2c9d-4753-bab7-fab29f189105","type":"WidgetBox"},{"id":"c05877ee-e28b-4381-83b3-c7cf1da7859b","subtype":"Figure","type":"Plot"}]},"id":"21ffced3-a303-4d1a-8150-aac24fb7b373","type":"Column"},{"attributes":{"children":[{"id":"84baa7f4-31a4-46c8-8264-4cb6e37c907c","type":"Slider"}]},"id":"efc23ce2-2c9d-4753-bab7-fab29f189105","type":"WidgetBox"},{"attributes":{},"id":"c7937970-ee7c-4b1f-b53c-d6fb4e0c703b","type":"BasicTickFormatter"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"00c68f57-6726-4688-a563-75d041a47745","type":"ImageURL"},{"attributes":{"plot":{"id":"c05877ee-e28b-4381-83b3-c7cf1da7859b","subtype":"Figure","type":"Plot"},"ticker":{"id":"af76e799-3406-496b-933b-9d0463858731","type":"BasicTicker"},"visible":false},"id":"26005f8d-d7ab-4dd0-899d-3ae7860db099","type":"Grid"},{"attributes":{},"id":"6cf6e2b9-e410-4714-a365-2182e13d64a3","type":"UnionRenderers"},{"attributes":{},"id":"9138679b-3e5e-4e6b-b617-73eeef4f48fc","type":"BasicTicker"},{"attributes":{"callback":null},"id":"a73c89b0-7b3d-4433-8fe2-5000c1f39f0f","type":"Range1d"},{"attributes":{},"id":"752fb4f5-599c-435c-956b-8d6eb652d40f","type":"Selection"},{"attributes":{"plot":null,"text":""},"id":"f62035a6-4acd-4e2a-be5f-9cff408c34c0","type":"Title"},{"attributes":{},"id":"dc63803a-459e-430b-9309-80ca34997336","type":"BasicTickFormatter"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"60c31cf8-2e84-458b-853e-8372546be18e","type":"ImageURL"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/MG8O8_000.png"],"url_num":["000"]},"selected":{"id":"752fb4f5-599c-435c-956b-8d6eb652d40f","type":"Selection"},"selection_policy":{"id":"6cf6e2b9-e410-4714-a365-2182e13d64a3","type":"UnionRenderers"}},"id":"5f9e3663-e057-4dcc-80b0-147a517462ae","type":"ColumnDataSource"},{"attributes":{},"id":"af76e799-3406-496b-933b-9d0463858731","type":"BasicTicker"},{"attributes":{"formatter":{"id":"c7937970-ee7c-4b1f-b53c-d6fb4e0c703b","type":"BasicTickFormatter"},"plot":{"id":"c05877ee-e28b-4381-83b3-c7cf1da7859b","subtype":"Figure","type":"Plot"},"ticker":{"id":"af76e799-3406-496b-933b-9d0463858731","type":"BasicTicker"},"visible":false},"id":"84fa4661-2557-47db-bdb7-a0fc127d2c90","type":"LinearAxis"}],"root_ids":["21ffced3-a303-4d1a-8150-aac24fb7b373"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"7e9f1c9b-d5a9-4fe4-8a9d-c0ef4d9b21ea","roots":{"21ffced3-a303-4d1a-8150-aac24fb7b373":"2223f78c-ccea-4ed6-b240-75da9b17c87f"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();