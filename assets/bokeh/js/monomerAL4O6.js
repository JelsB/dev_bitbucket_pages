(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("1065b0d5-b45c-409b-aa58-31031b0de71e");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '1065b0d5-b45c-409b-aa58-31031b0de71e' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"3784d1f1-f983-49ec-91ba-b6319397db99":{"roots":{"references":[{"attributes":{},"id":"93f1e145-db64-4379-9315-d58666b2a2cd","type":"BasicTickFormatter"},{"attributes":{},"id":"bab2caa0-de99-40ee-8857-ab674cb91903","type":"BasicTickFormatter"},{"attributes":{},"id":"43c848a6-3295-4c63-9693-63a8c0be0d0a","type":"LinearScale"},{"attributes":{"children":[{"id":"8d9e260c-6169-4788-8358-ce7a5d77dc1d","type":"WidgetBox"},{"id":"be719c6a-6f1a-4867-bd52-7a545e3a8597","subtype":"Figure","type":"Plot"}]},"id":"f20d97d5-18a9-4e00-9c2b-0437fe6e8685","type":"Column"},{"attributes":{"formatter":{"id":"93f1e145-db64-4379-9315-d58666b2a2cd","type":"BasicTickFormatter"},"plot":{"id":"be719c6a-6f1a-4867-bd52-7a545e3a8597","subtype":"Figure","type":"Plot"},"ticker":{"id":"70e66300-eb59-4d08-9c77-bf46fbb53a29","type":"BasicTicker"},"visible":false},"id":"33ba16e6-1493-40a1-b32d-dc6efa04ec60","type":"LinearAxis"},{"attributes":{"children":[{"id":"05b2a6c9-1461-4643-8b27-6bb4f1fff5b2","type":"Slider"}]},"id":"8d9e260c-6169-4788-8358-ce7a5d77dc1d","type":"WidgetBox"},{"attributes":{},"id":"822bd779-f4b9-4feb-b975-a6a7154ef19e","type":"LinearScale"},{"attributes":{},"id":"5bfe9c6c-de51-4daf-84cd-bd733def8483","type":"BasicTicker"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"2f572020-426e-4e8e-9e40-30cd8d42bd12","type":"ImageURL"},{"attributes":{"below":[{"id":"33ba16e6-1493-40a1-b32d-dc6efa04ec60","type":"LinearAxis"}],"left":[{"id":"d2b0197e-5007-4269-a378-f847cd05aa72","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"33ba16e6-1493-40a1-b32d-dc6efa04ec60","type":"LinearAxis"},{"id":"cc184996-43bd-436c-a73a-1bf0b9645390","type":"Grid"},{"id":"d2b0197e-5007-4269-a378-f847cd05aa72","type":"LinearAxis"},{"id":"3aee4443-452f-4dfe-ae69-fa4bdb079227","type":"Grid"},{"id":"c353f05f-64ef-4626-a8ae-9754700cb91b","type":"GlyphRenderer"}],"title":{"id":"ff62548d-daa3-4f69-86ce-635507c70c85","type":"Title"},"toolbar":{"id":"582153f7-6a42-44c6-94b7-37a95ed4b21b","type":"Toolbar"},"x_range":{"id":"22afaf0b-f43e-403f-833d-30d558693952","type":"Range1d"},"x_scale":{"id":"822bd779-f4b9-4feb-b975-a6a7154ef19e","type":"LinearScale"},"y_range":{"id":"a8a7d160-9cf5-40f4-abe2-391dd84bb6f3","type":"Range1d"},"y_scale":{"id":"43c848a6-3295-4c63-9693-63a8c0be0d0a","type":"LinearScale"}},"id":"be719c6a-6f1a-4867-bd52-7a545e3a8597","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"c356eae7-fe2c-4e9c-8aa1-cdf52431f58e","type":"Selection"},{"attributes":{"source":{"id":"6744a6de-bc23-44de-872e-c87c652bfc64","type":"ColumnDataSource"}},"id":"4afa11d0-6ee2-4828-9f2b-4eee9aa51c3f","type":"CDSView"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerAL4O6_000.png"],"url_num":["000"]},"selected":{"id":"c356eae7-fe2c-4e9c-8aa1-cdf52431f58e","type":"Selection"},"selection_policy":{"id":"caf3bc1c-d49f-4ef9-8cbf-875e7011a139","type":"UnionRenderers"}},"id":"6744a6de-bc23-44de-872e-c87c652bfc64","type":"ColumnDataSource"},{"attributes":{},"id":"70e66300-eb59-4d08-9c77-bf46fbb53a29","type":"BasicTicker"},{"attributes":{"data_source":{"id":"6744a6de-bc23-44de-872e-c87c652bfc64","type":"ColumnDataSource"},"glyph":{"id":"131b228c-d0fc-43f6-a3be-c57f61f539fc","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"2f572020-426e-4e8e-9e40-30cd8d42bd12","type":"ImageURL"},"selection_glyph":null,"view":{"id":"4afa11d0-6ee2-4828-9f2b-4eee9aa51c3f","type":"CDSView"}},"id":"c353f05f-64ef-4626-a8ae-9754700cb91b","type":"GlyphRenderer"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"582153f7-6a42-44c6-94b7-37a95ed4b21b","type":"Toolbar"},{"attributes":{"plot":null,"text":""},"id":"ff62548d-daa3-4f69-86ce-635507c70c85","type":"Title"},{"attributes":{"args":{"source":{"id":"6744a6de-bc23-44de-872e-c87c652bfc64","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"d7f7a0d2-91d8-4f80-b686-eafbaee4cb0f","type":"CustomJS"},{"attributes":{"dimension":1,"plot":{"id":"be719c6a-6f1a-4867-bd52-7a545e3a8597","subtype":"Figure","type":"Plot"},"ticker":{"id":"5bfe9c6c-de51-4daf-84cd-bd733def8483","type":"BasicTicker"},"visible":false},"id":"3aee4443-452f-4dfe-ae69-fa4bdb079227","type":"Grid"},{"attributes":{"formatter":{"id":"bab2caa0-de99-40ee-8857-ab674cb91903","type":"BasicTickFormatter"},"plot":{"id":"be719c6a-6f1a-4867-bd52-7a545e3a8597","subtype":"Figure","type":"Plot"},"ticker":{"id":"5bfe9c6c-de51-4daf-84cd-bd733def8483","type":"BasicTicker"},"visible":false},"id":"d2b0197e-5007-4269-a378-f847cd05aa72","type":"LinearAxis"},{"attributes":{"callback":null},"id":"22afaf0b-f43e-403f-833d-30d558693952","type":"Range1d"},{"attributes":{},"id":"caf3bc1c-d49f-4ef9-8cbf-875e7011a139","type":"UnionRenderers"},{"attributes":{"callback":null},"id":"a8a7d160-9cf5-40f4-abe2-391dd84bb6f3","type":"Range1d"},{"attributes":{"callback":{"id":"d7f7a0d2-91d8-4f80-b686-eafbaee4cb0f","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"05b2a6c9-1461-4643-8b27-6bb4f1fff5b2","type":"Slider"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"131b228c-d0fc-43f6-a3be-c57f61f539fc","type":"ImageURL"},{"attributes":{"plot":{"id":"be719c6a-6f1a-4867-bd52-7a545e3a8597","subtype":"Figure","type":"Plot"},"ticker":{"id":"70e66300-eb59-4d08-9c77-bf46fbb53a29","type":"BasicTicker"},"visible":false},"id":"cc184996-43bd-436c-a73a-1bf0b9645390","type":"Grid"}],"root_ids":["f20d97d5-18a9-4e00-9c2b-0437fe6e8685"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"3784d1f1-f983-49ec-91ba-b6319397db99","roots":{"f20d97d5-18a9-4e00-9c2b-0437fe6e8685":"1065b0d5-b45c-409b-aa58-31031b0de71e"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();