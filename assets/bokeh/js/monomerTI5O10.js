(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("29b83d5b-20a7-4626-bf7c-af1f02640a44");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '29b83d5b-20a7-4626-bf7c-af1f02640a44' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"341c232d-b1d5-49f0-a2bb-1b4b1bb5ccf7":{"roots":{"references":[{"attributes":{"children":[{"id":"0625f466-9905-4527-b30d-0f7a16973844","type":"WidgetBox"},{"id":"771a8374-b4bb-46ea-9d80-a0108e785d27","subtype":"Figure","type":"Plot"}]},"id":"3afce414-3dc6-41ef-865d-2beaf598bdf7","type":"Column"},{"attributes":{"callback":null},"id":"8d0df2a8-0ffb-4405-8746-384f72cb2461","type":"Range1d"},{"attributes":{"below":[{"id":"9b207399-3723-4dc1-b9da-2422288eaf3e","type":"LinearAxis"}],"left":[{"id":"8ed8dadf-5205-429c-82d0-bc2a8701d829","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"9b207399-3723-4dc1-b9da-2422288eaf3e","type":"LinearAxis"},{"id":"5a383920-8b3f-4961-b012-feca71c837a4","type":"Grid"},{"id":"8ed8dadf-5205-429c-82d0-bc2a8701d829","type":"LinearAxis"},{"id":"91761d05-e61b-46ea-8cb9-321a812890b0","type":"Grid"},{"id":"20ebf45f-b149-4f5e-b3c4-91b8931cdd4c","type":"GlyphRenderer"}],"title":{"id":"595439e7-e144-47cb-85ea-26cac26df3f4","type":"Title"},"toolbar":{"id":"1f1dfd46-686d-4424-9c72-307bc088fe39","type":"Toolbar"},"x_range":{"id":"8d0df2a8-0ffb-4405-8746-384f72cb2461","type":"Range1d"},"x_scale":{"id":"fc3faafe-e747-4a2f-aaea-d6cb7045187f","type":"LinearScale"},"y_range":{"id":"393445b3-dde7-4836-863b-ec926453ab22","type":"Range1d"},"y_scale":{"id":"514cf26c-724e-4dbc-ae25-490c3d66477b","type":"LinearScale"}},"id":"771a8374-b4bb-46ea-9d80-a0108e785d27","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"fc3faafe-e747-4a2f-aaea-d6cb7045187f","type":"LinearScale"},{"attributes":{"data_source":{"id":"db5a8386-3b5a-496d-a9c9-725e2e097f72","type":"ColumnDataSource"},"glyph":{"id":"5b490ba1-8b3d-47dc-a0b5-19c00480f5e3","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"e34db51a-4f40-40d3-944a-2008ff5a614c","type":"ImageURL"},"selection_glyph":null,"view":{"id":"801765a0-3ebf-4a43-bc91-6d23dab17c3d","type":"CDSView"}},"id":"20ebf45f-b149-4f5e-b3c4-91b8931cdd4c","type":"GlyphRenderer"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"1f1dfd46-686d-4424-9c72-307bc088fe39","type":"Toolbar"},{"attributes":{"callback":{"id":"3dcb7e78-47eb-4b47-ae2e-3dc0e50e4d87","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"0fee63ab-c480-4732-89e2-87c969ede003","type":"Slider"},{"attributes":{"plot":{"id":"771a8374-b4bb-46ea-9d80-a0108e785d27","subtype":"Figure","type":"Plot"},"ticker":{"id":"d6423c72-f9cd-47cf-9e5d-34b395828383","type":"BasicTicker"},"visible":false},"id":"5a383920-8b3f-4961-b012-feca71c837a4","type":"Grid"},{"attributes":{},"id":"cc72e0c8-5c82-4c92-995d-12739ef1a41d","type":"BasicTickFormatter"},{"attributes":{"formatter":{"id":"5fa54795-f91f-4a05-ad8d-b9221002f08d","type":"BasicTickFormatter"},"plot":{"id":"771a8374-b4bb-46ea-9d80-a0108e785d27","subtype":"Figure","type":"Plot"},"ticker":{"id":"8ed10aef-9217-42b6-92ed-79dfb731c04b","type":"BasicTicker"},"visible":false},"id":"8ed8dadf-5205-429c-82d0-bc2a8701d829","type":"LinearAxis"},{"attributes":{"callback":null},"id":"393445b3-dde7-4836-863b-ec926453ab22","type":"Range1d"},{"attributes":{},"id":"514cf26c-724e-4dbc-ae25-490c3d66477b","type":"LinearScale"},{"attributes":{"source":{"id":"db5a8386-3b5a-496d-a9c9-725e2e097f72","type":"ColumnDataSource"}},"id":"801765a0-3ebf-4a43-bc91-6d23dab17c3d","type":"CDSView"},{"attributes":{"plot":null,"text":""},"id":"595439e7-e144-47cb-85ea-26cac26df3f4","type":"Title"},{"attributes":{},"id":"d6423c72-f9cd-47cf-9e5d-34b395828383","type":"BasicTicker"},{"attributes":{"formatter":{"id":"cc72e0c8-5c82-4c92-995d-12739ef1a41d","type":"BasicTickFormatter"},"plot":{"id":"771a8374-b4bb-46ea-9d80-a0108e785d27","subtype":"Figure","type":"Plot"},"ticker":{"id":"d6423c72-f9cd-47cf-9e5d-34b395828383","type":"BasicTicker"},"visible":false},"id":"9b207399-3723-4dc1-b9da-2422288eaf3e","type":"LinearAxis"},{"attributes":{},"id":"0e9afbd6-729c-4156-b53f-4d7ede0e6df5","type":"Selection"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerTI5O10_000.png"],"url_num":["000"]},"selected":{"id":"0e9afbd6-729c-4156-b53f-4d7ede0e6df5","type":"Selection"},"selection_policy":{"id":"7b73d13e-949c-4f34-b7bf-a0432d852b49","type":"UnionRenderers"}},"id":"db5a8386-3b5a-496d-a9c9-725e2e097f72","type":"ColumnDataSource"},{"attributes":{},"id":"7b73d13e-949c-4f34-b7bf-a0432d852b49","type":"UnionRenderers"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"e34db51a-4f40-40d3-944a-2008ff5a614c","type":"ImageURL"},{"attributes":{},"id":"5fa54795-f91f-4a05-ad8d-b9221002f08d","type":"BasicTickFormatter"},{"attributes":{},"id":"8ed10aef-9217-42b6-92ed-79dfb731c04b","type":"BasicTicker"},{"attributes":{"args":{"source":{"id":"db5a8386-3b5a-496d-a9c9-725e2e097f72","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"3dcb7e78-47eb-4b47-ae2e-3dc0e50e4d87","type":"CustomJS"},{"attributes":{"children":[{"id":"0fee63ab-c480-4732-89e2-87c969ede003","type":"Slider"}]},"id":"0625f466-9905-4527-b30d-0f7a16973844","type":"WidgetBox"},{"attributes":{"dimension":1,"plot":{"id":"771a8374-b4bb-46ea-9d80-a0108e785d27","subtype":"Figure","type":"Plot"},"ticker":{"id":"8ed10aef-9217-42b6-92ed-79dfb731c04b","type":"BasicTicker"},"visible":false},"id":"91761d05-e61b-46ea-8cb9-321a812890b0","type":"Grid"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"5b490ba1-8b3d-47dc-a0b5-19c00480f5e3","type":"ImageURL"}],"root_ids":["3afce414-3dc6-41ef-865d-2beaf598bdf7"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"341c232d-b1d5-49f0-a2bb-1b4b1bb5ccf7","roots":{"3afce414-3dc6-41ef-865d-2beaf598bdf7":"29b83d5b-20a7-4626-bf7c-af1f02640a44"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();