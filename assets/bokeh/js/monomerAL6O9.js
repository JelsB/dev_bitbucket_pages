(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("50427f61-30dd-4050-94f2-ac34837ea9a1");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '50427f61-30dd-4050-94f2-ac34837ea9a1' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"2280dc2c-83ac-44c4-916e-5902cfccab81":{"roots":{"references":[{"attributes":{"callback":{"id":"17f95812-57bd-4020-982c-f3cfa13294a4","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"3f16d584-3075-4d61-8562-cae01970a941","type":"Slider"},{"attributes":{},"id":"d6e5f736-c086-4702-9b06-741b7e8f60c4","type":"LinearScale"},{"attributes":{},"id":"92035b9d-d3bf-4a16-8205-ab81f21f0fb4","type":"LinearScale"},{"attributes":{"callback":null},"id":"f26d43bf-8a14-45c3-bbe5-3d26cfb340e1","type":"Range1d"},{"attributes":{},"id":"479d80b4-921c-454f-bb03-8db01177888d","type":"BasicTickFormatter"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"4a7a6ebb-0c24-4663-9ab0-4a1f8c503ba2","type":"ImageURL"},{"attributes":{"below":[{"id":"9b766719-8305-491b-8ff8-057a1134e24d","type":"LinearAxis"}],"left":[{"id":"ffbb61fd-988d-4a70-85a6-5170c8d878a4","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"9b766719-8305-491b-8ff8-057a1134e24d","type":"LinearAxis"},{"id":"8eae8979-e737-452d-af2d-4ff9d7afcb52","type":"Grid"},{"id":"ffbb61fd-988d-4a70-85a6-5170c8d878a4","type":"LinearAxis"},{"id":"acdbe3fa-6020-4a25-af74-85837e039fe9","type":"Grid"},{"id":"ee8d2fba-2b45-4625-8131-dc45ba539e3f","type":"GlyphRenderer"}],"title":{"id":"70650931-1aca-4278-829f-ff8d94f5208b","type":"Title"},"toolbar":{"id":"33b37a8b-40b1-4f8e-a8fe-0a090b838abc","type":"Toolbar"},"x_range":{"id":"52a5860f-073c-49dc-8949-d75561213524","type":"Range1d"},"x_scale":{"id":"d6e5f736-c086-4702-9b06-741b7e8f60c4","type":"LinearScale"},"y_range":{"id":"f26d43bf-8a14-45c3-bbe5-3d26cfb340e1","type":"Range1d"},"y_scale":{"id":"92035b9d-d3bf-4a16-8205-ab81f21f0fb4","type":"LinearScale"}},"id":"e5753bcf-3665-4096-9e5e-7efdd496dbbf","subtype":"Figure","type":"Plot"},{"attributes":{"dimension":1,"plot":{"id":"e5753bcf-3665-4096-9e5e-7efdd496dbbf","subtype":"Figure","type":"Plot"},"ticker":{"id":"36e89ea3-4825-4f0d-9510-35889907337d","type":"BasicTicker"},"visible":false},"id":"acdbe3fa-6020-4a25-af74-85837e039fe9","type":"Grid"},{"attributes":{"children":[{"id":"3f16d584-3075-4d61-8562-cae01970a941","type":"Slider"}]},"id":"c1ae8bd9-a141-4f69-aabd-7c5d7d8e0674","type":"WidgetBox"},{"attributes":{},"id":"fa6630dc-e1ed-4f47-b8f2-f254dc2a98e9","type":"BasicTickFormatter"},{"attributes":{"args":{"source":{"id":"9276fe62-a869-422a-a384-c8159adaedfa","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"17f95812-57bd-4020-982c-f3cfa13294a4","type":"CustomJS"},{"attributes":{"plot":{"id":"e5753bcf-3665-4096-9e5e-7efdd496dbbf","subtype":"Figure","type":"Plot"},"ticker":{"id":"a8ddb8ec-4e0e-4928-8757-816487176711","type":"BasicTicker"},"visible":false},"id":"8eae8979-e737-452d-af2d-4ff9d7afcb52","type":"Grid"},{"attributes":{},"id":"e07c1f94-8c03-499c-9ad7-6bd6f0487424","type":"UnionRenderers"},{"attributes":{},"id":"4085a36d-3f8d-4cd9-82e1-c3a9628e9e94","type":"Selection"},{"attributes":{"formatter":{"id":"fa6630dc-e1ed-4f47-b8f2-f254dc2a98e9","type":"BasicTickFormatter"},"plot":{"id":"e5753bcf-3665-4096-9e5e-7efdd496dbbf","subtype":"Figure","type":"Plot"},"ticker":{"id":"a8ddb8ec-4e0e-4928-8757-816487176711","type":"BasicTicker"},"visible":false},"id":"9b766719-8305-491b-8ff8-057a1134e24d","type":"LinearAxis"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerAL6O9_000.png"],"url_num":["000"]},"selected":{"id":"4085a36d-3f8d-4cd9-82e1-c3a9628e9e94","type":"Selection"},"selection_policy":{"id":"e07c1f94-8c03-499c-9ad7-6bd6f0487424","type":"UnionRenderers"}},"id":"9276fe62-a869-422a-a384-c8159adaedfa","type":"ColumnDataSource"},{"attributes":{},"id":"a8ddb8ec-4e0e-4928-8757-816487176711","type":"BasicTicker"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"33b37a8b-40b1-4f8e-a8fe-0a090b838abc","type":"Toolbar"},{"attributes":{"formatter":{"id":"479d80b4-921c-454f-bb03-8db01177888d","type":"BasicTickFormatter"},"plot":{"id":"e5753bcf-3665-4096-9e5e-7efdd496dbbf","subtype":"Figure","type":"Plot"},"ticker":{"id":"36e89ea3-4825-4f0d-9510-35889907337d","type":"BasicTicker"},"visible":false},"id":"ffbb61fd-988d-4a70-85a6-5170c8d878a4","type":"LinearAxis"},{"attributes":{"plot":null,"text":""},"id":"70650931-1aca-4278-829f-ff8d94f5208b","type":"Title"},{"attributes":{},"id":"36e89ea3-4825-4f0d-9510-35889907337d","type":"BasicTicker"},{"attributes":{"data_source":{"id":"9276fe62-a869-422a-a384-c8159adaedfa","type":"ColumnDataSource"},"glyph":{"id":"4a7a6ebb-0c24-4663-9ab0-4a1f8c503ba2","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"c11f1c5e-26d6-439b-af25-6b01028f1339","type":"ImageURL"},"selection_glyph":null,"view":{"id":"21cb3542-eb5e-4a18-a58a-eae37a6c4fc2","type":"CDSView"}},"id":"ee8d2fba-2b45-4625-8131-dc45ba539e3f","type":"GlyphRenderer"},{"attributes":{"callback":null},"id":"52a5860f-073c-49dc-8949-d75561213524","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"c11f1c5e-26d6-439b-af25-6b01028f1339","type":"ImageURL"},{"attributes":{"source":{"id":"9276fe62-a869-422a-a384-c8159adaedfa","type":"ColumnDataSource"}},"id":"21cb3542-eb5e-4a18-a58a-eae37a6c4fc2","type":"CDSView"},{"attributes":{"children":[{"id":"c1ae8bd9-a141-4f69-aabd-7c5d7d8e0674","type":"WidgetBox"},{"id":"e5753bcf-3665-4096-9e5e-7efdd496dbbf","subtype":"Figure","type":"Plot"}]},"id":"f8ad54e5-8ab1-40c9-a8c4-e39e71e8fa97","type":"Column"}],"root_ids":["f8ad54e5-8ab1-40c9-a8c4-e39e71e8fa97"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"2280dc2c-83ac-44c4-916e-5902cfccab81","roots":{"f8ad54e5-8ab1-40c9-a8c4-e39e71e8fa97":"50427f61-30dd-4050-94f2-ac34837ea9a1"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();