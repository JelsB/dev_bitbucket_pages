(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("bc288cde-d7de-4f74-95d5-8d64ddad6362");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'bc288cde-d7de-4f74-95d5-8d64ddad6362' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"60e3b4f5-6895-43c8-9c0c-5af4b1662d35":{"roots":{"references":[{"attributes":{"callback":null},"id":"d4750fe1-7fa9-4ca1-b061-37b3381299e5","type":"Range1d"},{"attributes":{"children":[{"id":"ddfed9c1-4914-4fcc-8f4c-47eed2ea3663","type":"Slider"}]},"id":"0832acfb-10ca-4cc7-b85d-c03345a7e705","type":"WidgetBox"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"4711ba87-a84b-487f-a7a3-47bf0eec7d0f","type":"ImageURL"},{"attributes":{"callback":null},"id":"80f57cae-0944-49cb-ab6f-ad9b319bc8db","type":"Range1d"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerTIO2_000.png"],"url_num":["000"]},"selected":{"id":"e9c84875-968c-42aa-978c-6794861184e6","type":"Selection"},"selection_policy":{"id":"e063654e-1896-4b06-9ff3-87760a4208a6","type":"UnionRenderers"}},"id":"aaccb7b0-e478-415a-91b8-d67e88659abb","type":"ColumnDataSource"},{"attributes":{},"id":"49db6cc5-a517-4d3e-9b3c-1000d0df9dd0","type":"LinearScale"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"f0d2aba7-5466-41a1-a283-cddf4ef98839","type":"Toolbar"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"ba08b02d-98a0-4d71-bd02-6049a83baad0","type":"ImageURL"},{"attributes":{},"id":"77a024cc-4959-4e9a-af02-b76c2c8771e8","type":"LinearScale"},{"attributes":{"children":[{"id":"0832acfb-10ca-4cc7-b85d-c03345a7e705","type":"WidgetBox"},{"id":"216e1bf0-ccac-4f4a-912a-f8ac3265d563","subtype":"Figure","type":"Plot"}]},"id":"ea7f00c8-7c97-4dfa-bec7-8e8c1534ab92","type":"Column"},{"attributes":{"plot":null,"text":""},"id":"8ea44f4a-fe3b-4f45-a443-142604fd2a10","type":"Title"},{"attributes":{"below":[{"id":"6a002681-e898-4d67-8332-e18557e5ed96","type":"LinearAxis"}],"left":[{"id":"e53dfd56-a7a5-4fa3-9e08-5331d988e039","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"6a002681-e898-4d67-8332-e18557e5ed96","type":"LinearAxis"},{"id":"f83d3cd4-b9d6-4ff9-a55f-950ea60253bc","type":"Grid"},{"id":"e53dfd56-a7a5-4fa3-9e08-5331d988e039","type":"LinearAxis"},{"id":"576c4fcc-3b99-4f59-91c0-7209a263d6f9","type":"Grid"},{"id":"fbce3f8a-089d-4ef5-84a9-700bf5b36c85","type":"GlyphRenderer"}],"title":{"id":"8ea44f4a-fe3b-4f45-a443-142604fd2a10","type":"Title"},"toolbar":{"id":"f0d2aba7-5466-41a1-a283-cddf4ef98839","type":"Toolbar"},"x_range":{"id":"d4750fe1-7fa9-4ca1-b061-37b3381299e5","type":"Range1d"},"x_scale":{"id":"49db6cc5-a517-4d3e-9b3c-1000d0df9dd0","type":"LinearScale"},"y_range":{"id":"80f57cae-0944-49cb-ab6f-ad9b319bc8db","type":"Range1d"},"y_scale":{"id":"77a024cc-4959-4e9a-af02-b76c2c8771e8","type":"LinearScale"}},"id":"216e1bf0-ccac-4f4a-912a-f8ac3265d563","subtype":"Figure","type":"Plot"},{"attributes":{"formatter":{"id":"a9eea27f-3aff-4f69-b6dd-bf4483ee987d","type":"BasicTickFormatter"},"plot":{"id":"216e1bf0-ccac-4f4a-912a-f8ac3265d563","subtype":"Figure","type":"Plot"},"ticker":{"id":"f3df1458-3e52-4593-9189-5fc38cb4a5f0","type":"BasicTicker"},"visible":false},"id":"6a002681-e898-4d67-8332-e18557e5ed96","type":"LinearAxis"},{"attributes":{},"id":"f46b6271-c77f-44e7-a2af-85decff41911","type":"BasicTickFormatter"},{"attributes":{},"id":"f3df1458-3e52-4593-9189-5fc38cb4a5f0","type":"BasicTicker"},{"attributes":{},"id":"a9eea27f-3aff-4f69-b6dd-bf4483ee987d","type":"BasicTickFormatter"},{"attributes":{"plot":{"id":"216e1bf0-ccac-4f4a-912a-f8ac3265d563","subtype":"Figure","type":"Plot"},"ticker":{"id":"f3df1458-3e52-4593-9189-5fc38cb4a5f0","type":"BasicTicker"},"visible":false},"id":"f83d3cd4-b9d6-4ff9-a55f-950ea60253bc","type":"Grid"},{"attributes":{"callback":{"id":"bbb9db59-8262-49d6-aca2-afafa8f67fb8","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"ddfed9c1-4914-4fcc-8f4c-47eed2ea3663","type":"Slider"},{"attributes":{},"id":"e063654e-1896-4b06-9ff3-87760a4208a6","type":"UnionRenderers"},{"attributes":{"formatter":{"id":"f46b6271-c77f-44e7-a2af-85decff41911","type":"BasicTickFormatter"},"plot":{"id":"216e1bf0-ccac-4f4a-912a-f8ac3265d563","subtype":"Figure","type":"Plot"},"ticker":{"id":"c379c87a-8646-4a8c-9ddf-e90edd0e18d4","type":"BasicTicker"},"visible":false},"id":"e53dfd56-a7a5-4fa3-9e08-5331d988e039","type":"LinearAxis"},{"attributes":{},"id":"c379c87a-8646-4a8c-9ddf-e90edd0e18d4","type":"BasicTicker"},{"attributes":{},"id":"e9c84875-968c-42aa-978c-6794861184e6","type":"Selection"},{"attributes":{"dimension":1,"plot":{"id":"216e1bf0-ccac-4f4a-912a-f8ac3265d563","subtype":"Figure","type":"Plot"},"ticker":{"id":"c379c87a-8646-4a8c-9ddf-e90edd0e18d4","type":"BasicTicker"},"visible":false},"id":"576c4fcc-3b99-4f59-91c0-7209a263d6f9","type":"Grid"},{"attributes":{"data_source":{"id":"aaccb7b0-e478-415a-91b8-d67e88659abb","type":"ColumnDataSource"},"glyph":{"id":"ba08b02d-98a0-4d71-bd02-6049a83baad0","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"4711ba87-a84b-487f-a7a3-47bf0eec7d0f","type":"ImageURL"},"selection_glyph":null,"view":{"id":"2b1a58c0-d369-416d-a9b2-10097897e182","type":"CDSView"}},"id":"fbce3f8a-089d-4ef5-84a9-700bf5b36c85","type":"GlyphRenderer"},{"attributes":{"args":{"source":{"id":"aaccb7b0-e478-415a-91b8-d67e88659abb","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"bbb9db59-8262-49d6-aca2-afafa8f67fb8","type":"CustomJS"},{"attributes":{"source":{"id":"aaccb7b0-e478-415a-91b8-d67e88659abb","type":"ColumnDataSource"}},"id":"2b1a58c0-d369-416d-a9b2-10097897e182","type":"CDSView"}],"root_ids":["ea7f00c8-7c97-4dfa-bec7-8e8c1534ab92"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"60e3b4f5-6895-43c8-9c0c-5af4b1662d35","roots":{"ea7f00c8-7c97-4dfa-bec7-8e8c1534ab92":"bc288cde-d7de-4f74-95d5-8d64ddad6362"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();