(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("3d8f6c76-7535-4aa8-9230-2c47f3027ae4");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '3d8f6c76-7535-4aa8-9230-2c47f3027ae4' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"ecfff58e-ae76-4416-aaf8-9d1b1cc94d5f":{"roots":{"references":[{"attributes":{},"id":"baa57941-dc71-48df-ae4a-11d91cd7b362","type":"BasicTicker"},{"attributes":{},"id":"eae4d052-991f-4203-a293-1a4bca8eda90","type":"BasicTickFormatter"},{"attributes":{"source":{"id":"64610b1a-c09f-43e1-8e0a-eaf5efc80e72","type":"ColumnDataSource"}},"id":"dcccc9cd-e771-4267-a0a0-c780bc96c00e","type":"CDSView"},{"attributes":{"args":{"source":{"id":"64610b1a-c09f-43e1-8e0a-eaf5efc80e72","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"71aac2b6-5236-499f-95fa-f2d0d9123e18","type":"CustomJS"},{"attributes":{},"id":"d09a80ab-4497-4656-b8ec-ea8043727074","type":"BasicTickFormatter"},{"attributes":{"callback":null},"id":"a8642be5-d6fd-4e00-bc59-7fa26e3b2662","type":"Range1d"},{"attributes":{},"id":"fc27f4a3-b125-42bf-9a1c-5d6acaf4ff1a","type":"BasicTicker"},{"attributes":{"below":[{"id":"070e4179-33cd-4a03-a0e3-840d90fd8920","type":"LinearAxis"}],"left":[{"id":"d898ebaa-8105-4b6c-aecc-b83444182895","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"070e4179-33cd-4a03-a0e3-840d90fd8920","type":"LinearAxis"},{"id":"967b1247-d5c6-49ac-80b8-5b9eb37804a7","type":"Grid"},{"id":"d898ebaa-8105-4b6c-aecc-b83444182895","type":"LinearAxis"},{"id":"a4dd1229-55b7-44b6-a564-b19d3524df39","type":"Grid"},{"id":"7f263104-bf59-4018-89ca-6fe01f05b1be","type":"GlyphRenderer"}],"title":{"id":"b25ce512-9222-45cb-88e7-4dca5db813ce","type":"Title"},"toolbar":{"id":"6b63220b-e52c-412e-9326-b86a47de7856","type":"Toolbar"},"x_range":{"id":"9c8fb367-8961-4f66-a57e-8e5f2220d349","type":"Range1d"},"x_scale":{"id":"21eab6ee-2a28-4b87-882a-60513631820e","type":"LinearScale"},"y_range":{"id":"a8642be5-d6fd-4e00-bc59-7fa26e3b2662","type":"Range1d"},"y_scale":{"id":"241aecc9-992b-4208-a401-8b821b859930","type":"LinearScale"}},"id":"fa9fd74c-f06b-4b0d-af79-f55ce8cc7b12","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"241aecc9-992b-4208-a401-8b821b859930","type":"LinearScale"},{"attributes":{},"id":"37cd8d0f-7ee4-4227-8f07-c08732be01d5","type":"Selection"},{"attributes":{"dimension":1,"plot":{"id":"fa9fd74c-f06b-4b0d-af79-f55ce8cc7b12","subtype":"Figure","type":"Plot"},"ticker":{"id":"baa57941-dc71-48df-ae4a-11d91cd7b362","type":"BasicTicker"},"visible":false},"id":"a4dd1229-55b7-44b6-a564-b19d3524df39","type":"Grid"},{"attributes":{},"id":"21eab6ee-2a28-4b87-882a-60513631820e","type":"LinearScale"},{"attributes":{"callback":null},"id":"9c8fb367-8961-4f66-a57e-8e5f2220d349","type":"Range1d"},{"attributes":{"plot":null,"text":""},"id":"b25ce512-9222-45cb-88e7-4dca5db813ce","type":"Title"},{"attributes":{"data_source":{"id":"64610b1a-c09f-43e1-8e0a-eaf5efc80e72","type":"ColumnDataSource"},"glyph":{"id":"9b9397b2-ca41-46a5-8fbe-49d6d191faf8","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"0d38f263-a2be-4a1c-a16d-a8bbdf6dd230","type":"ImageURL"},"selection_glyph":null,"view":{"id":"dcccc9cd-e771-4267-a0a0-c780bc96c00e","type":"CDSView"}},"id":"7f263104-bf59-4018-89ca-6fe01f05b1be","type":"GlyphRenderer"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/TI8O16_000.png"],"url_num":["000"]},"selected":{"id":"37cd8d0f-7ee4-4227-8f07-c08732be01d5","type":"Selection"},"selection_policy":{"id":"f8a63913-a3e0-4171-a7f2-136e2adc4cb0","type":"UnionRenderers"}},"id":"64610b1a-c09f-43e1-8e0a-eaf5efc80e72","type":"ColumnDataSource"},{"attributes":{"formatter":{"id":"d09a80ab-4497-4656-b8ec-ea8043727074","type":"BasicTickFormatter"},"plot":{"id":"fa9fd74c-f06b-4b0d-af79-f55ce8cc7b12","subtype":"Figure","type":"Plot"},"ticker":{"id":"baa57941-dc71-48df-ae4a-11d91cd7b362","type":"BasicTicker"},"visible":false},"id":"d898ebaa-8105-4b6c-aecc-b83444182895","type":"LinearAxis"},{"attributes":{"children":[{"id":"7544c2c7-9de0-4fe2-a0ae-063379e5a5be","type":"WidgetBox"},{"id":"fa9fd74c-f06b-4b0d-af79-f55ce8cc7b12","subtype":"Figure","type":"Plot"}]},"id":"f2a36ce3-8456-406d-a037-3076e234ecbe","type":"Column"},{"attributes":{"callback":{"id":"71aac2b6-5236-499f-95fa-f2d0d9123e18","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"bab73f15-1096-441f-834d-fb70a12ec2eb","type":"Slider"},{"attributes":{},"id":"f8a63913-a3e0-4171-a7f2-136e2adc4cb0","type":"UnionRenderers"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"9b9397b2-ca41-46a5-8fbe-49d6d191faf8","type":"ImageURL"},{"attributes":{"plot":{"id":"fa9fd74c-f06b-4b0d-af79-f55ce8cc7b12","subtype":"Figure","type":"Plot"},"ticker":{"id":"fc27f4a3-b125-42bf-9a1c-5d6acaf4ff1a","type":"BasicTicker"},"visible":false},"id":"967b1247-d5c6-49ac-80b8-5b9eb37804a7","type":"Grid"},{"attributes":{"children":[{"id":"bab73f15-1096-441f-834d-fb70a12ec2eb","type":"Slider"}]},"id":"7544c2c7-9de0-4fe2-a0ae-063379e5a5be","type":"WidgetBox"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"0d38f263-a2be-4a1c-a16d-a8bbdf6dd230","type":"ImageURL"},{"attributes":{"formatter":{"id":"eae4d052-991f-4203-a293-1a4bca8eda90","type":"BasicTickFormatter"},"plot":{"id":"fa9fd74c-f06b-4b0d-af79-f55ce8cc7b12","subtype":"Figure","type":"Plot"},"ticker":{"id":"fc27f4a3-b125-42bf-9a1c-5d6acaf4ff1a","type":"BasicTicker"},"visible":false},"id":"070e4179-33cd-4a03-a0e3-840d90fd8920","type":"LinearAxis"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"6b63220b-e52c-412e-9326-b86a47de7856","type":"Toolbar"}],"root_ids":["f2a36ce3-8456-406d-a037-3076e234ecbe"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"ecfff58e-ae76-4416-aaf8-9d1b1cc94d5f","roots":{"f2a36ce3-8456-406d-a037-3076e234ecbe":"3d8f6c76-7535-4aa8-9230-2c47f3027ae4"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();