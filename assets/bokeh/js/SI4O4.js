(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("9f5b4e2f-b3d5-4fe5-809b-6e92d60260c1");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '9f5b4e2f-b3d5-4fe5-809b-6e92d60260c1' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"9e9adeca-20b9-475e-949e-b24479df25ea":{"roots":{"references":[{"attributes":{},"id":"13b2db9f-bea2-424d-ac42-bfea66ac7e9d","type":"BasicTickFormatter"},{"attributes":{"formatter":{"id":"eb6a3fd0-95d6-4859-9ab1-889a15605dbe","type":"BasicTickFormatter"},"plot":{"id":"ac0ae9af-e4eb-4dd7-8ecb-f1449e0abaaa","subtype":"Figure","type":"Plot"},"ticker":{"id":"fb18dd97-255b-4376-b376-c7a5c5bc4eb3","type":"BasicTicker"},"visible":false},"id":"e6e15ca9-6d87-4344-9444-0f233a49a23e","type":"LinearAxis"},{"attributes":{"dimension":1,"plot":{"id":"ac0ae9af-e4eb-4dd7-8ecb-f1449e0abaaa","subtype":"Figure","type":"Plot"},"ticker":{"id":"fb18dd97-255b-4376-b376-c7a5c5bc4eb3","type":"BasicTicker"},"visible":false},"id":"d0a17d14-0b45-475e-9dd8-455af164351f","type":"Grid"},{"attributes":{"below":[{"id":"c4b53e4b-4627-4cb4-a3ed-9c5e8c80e04d","type":"LinearAxis"}],"left":[{"id":"e6e15ca9-6d87-4344-9444-0f233a49a23e","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"c4b53e4b-4627-4cb4-a3ed-9c5e8c80e04d","type":"LinearAxis"},{"id":"bde39df0-f682-4819-959b-14198fe3d105","type":"Grid"},{"id":"e6e15ca9-6d87-4344-9444-0f233a49a23e","type":"LinearAxis"},{"id":"d0a17d14-0b45-475e-9dd8-455af164351f","type":"Grid"},{"id":"6049b21d-2b88-44b0-a4b1-c3d504e9d458","type":"GlyphRenderer"}],"title":{"id":"dc8305d8-bc43-4346-b832-9d327ce86c96","type":"Title"},"toolbar":{"id":"0f0cb08e-2549-47f9-a72e-1dc25cce5ce0","type":"Toolbar"},"x_range":{"id":"cba55525-b96c-49dc-bcd0-5dad34879269","type":"Range1d"},"x_scale":{"id":"3cf27558-f241-4347-bc5e-176715460f6c","type":"LinearScale"},"y_range":{"id":"83069fd5-bea9-4118-bf40-6231f0842314","type":"Range1d"},"y_scale":{"id":"3228c0c7-15f9-4b3a-9d66-90c41205c778","type":"LinearScale"}},"id":"ac0ae9af-e4eb-4dd7-8ecb-f1449e0abaaa","subtype":"Figure","type":"Plot"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"dc9cb1a4-6e62-48cc-8eed-066687cf1c3c","type":"ImageURL"},{"attributes":{},"id":"fb18dd97-255b-4376-b376-c7a5c5bc4eb3","type":"BasicTicker"},{"attributes":{"callback":null},"id":"83069fd5-bea9-4118-bf40-6231f0842314","type":"Range1d"},{"attributes":{"data_source":{"id":"03541371-bd86-4de5-bc40-62c121163cdf","type":"ColumnDataSource"},"glyph":{"id":"dc9cb1a4-6e62-48cc-8eed-066687cf1c3c","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"614706b6-3887-48be-8c00-8b400f54e4fe","type":"ImageURL"},"selection_glyph":null,"view":{"id":"4ae7bd74-ff1b-4497-b07a-c64d91c82be6","type":"CDSView"}},"id":"6049b21d-2b88-44b0-a4b1-c3d504e9d458","type":"GlyphRenderer"},{"attributes":{},"id":"0a594688-849d-4ea1-b3ba-233c8f066369","type":"UnionRenderers"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"0f0cb08e-2549-47f9-a72e-1dc25cce5ce0","type":"Toolbar"},{"attributes":{"formatter":{"id":"13b2db9f-bea2-424d-ac42-bfea66ac7e9d","type":"BasicTickFormatter"},"plot":{"id":"ac0ae9af-e4eb-4dd7-8ecb-f1449e0abaaa","subtype":"Figure","type":"Plot"},"ticker":{"id":"cadd9964-359f-46f9-a2bb-8ab4a2485c27","type":"BasicTicker"},"visible":false},"id":"c4b53e4b-4627-4cb4-a3ed-9c5e8c80e04d","type":"LinearAxis"},{"attributes":{"args":{"source":{"id":"03541371-bd86-4de5-bc40-62c121163cdf","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"404877a7-3606-473b-9b69-b70e4202e877","type":"CustomJS"},{"attributes":{},"id":"cadd9964-359f-46f9-a2bb-8ab4a2485c27","type":"BasicTicker"},{"attributes":{},"id":"3cf27558-f241-4347-bc5e-176715460f6c","type":"LinearScale"},{"attributes":{"source":{"id":"03541371-bd86-4de5-bc40-62c121163cdf","type":"ColumnDataSource"}},"id":"4ae7bd74-ff1b-4497-b07a-c64d91c82be6","type":"CDSView"},{"attributes":{},"id":"1b62a445-b236-448d-94c7-71aef8d6a696","type":"Selection"},{"attributes":{"callback":null},"id":"cba55525-b96c-49dc-bcd0-5dad34879269","type":"Range1d"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/SI4O4_000.png"],"url_num":["000"]},"selected":{"id":"1b62a445-b236-448d-94c7-71aef8d6a696","type":"Selection"},"selection_policy":{"id":"0a594688-849d-4ea1-b3ba-233c8f066369","type":"UnionRenderers"}},"id":"03541371-bd86-4de5-bc40-62c121163cdf","type":"ColumnDataSource"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"614706b6-3887-48be-8c00-8b400f54e4fe","type":"ImageURL"},{"attributes":{},"id":"3228c0c7-15f9-4b3a-9d66-90c41205c778","type":"LinearScale"},{"attributes":{"children":[{"id":"e1df24e1-93b2-4a4b-bc9d-7ca17b2a8619","type":"Slider"}]},"id":"b6ba0dce-a129-4ac4-a0c7-52df194b281f","type":"WidgetBox"},{"attributes":{"callback":{"id":"404877a7-3606-473b-9b69-b70e4202e877","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"e1df24e1-93b2-4a4b-bc9d-7ca17b2a8619","type":"Slider"},{"attributes":{"plot":null,"text":""},"id":"dc8305d8-bc43-4346-b832-9d327ce86c96","type":"Title"},{"attributes":{"children":[{"id":"b6ba0dce-a129-4ac4-a0c7-52df194b281f","type":"WidgetBox"},{"id":"ac0ae9af-e4eb-4dd7-8ecb-f1449e0abaaa","subtype":"Figure","type":"Plot"}]},"id":"375c4bc5-d972-4601-9064-345cb1e14acb","type":"Column"},{"attributes":{"plot":{"id":"ac0ae9af-e4eb-4dd7-8ecb-f1449e0abaaa","subtype":"Figure","type":"Plot"},"ticker":{"id":"cadd9964-359f-46f9-a2bb-8ab4a2485c27","type":"BasicTicker"},"visible":false},"id":"bde39df0-f682-4819-959b-14198fe3d105","type":"Grid"},{"attributes":{},"id":"eb6a3fd0-95d6-4859-9ab1-889a15605dbe","type":"BasicTickFormatter"}],"root_ids":["375c4bc5-d972-4601-9064-345cb1e14acb"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"9e9adeca-20b9-475e-949e-b24479df25ea","roots":{"375c4bc5-d972-4601-9064-345cb1e14acb":"9f5b4e2f-b3d5-4fe5-809b-6e92d60260c1"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();