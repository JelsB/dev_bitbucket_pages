(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("edd030d3-095e-4d44-8166-387b0058f286");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'edd030d3-095e-4d44-8166-387b0058f286' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"a17e0b4d-8bea-4f99-a055-3b4a2e27145d":{"roots":{"references":[{"attributes":{"callback":null},"id":"8a80b7d3-b46f-4e17-b082-ad87b05d4b36","type":"Range1d"},{"attributes":{},"id":"0024e9e1-42d5-48f3-a939-d854c76bdaad","type":"BasicTicker"},{"attributes":{"plot":{"id":"a2d97918-d305-4b26-be77-d75f8fa88b4a","subtype":"Figure","type":"Plot"},"ticker":{"id":"602feeef-e7c1-4ea4-9658-fb50809a99ac","type":"BasicTicker"},"visible":false},"id":"e52fa527-7fc1-4042-85be-e434e4f83783","type":"Grid"},{"attributes":{"formatter":{"id":"96d6032f-e217-4c59-a8a6-b634d147e986","type":"BasicTickFormatter"},"plot":{"id":"a2d97918-d305-4b26-be77-d75f8fa88b4a","subtype":"Figure","type":"Plot"},"ticker":{"id":"0024e9e1-42d5-48f3-a939-d854c76bdaad","type":"BasicTicker"},"visible":false},"id":"914b168f-18fe-499a-95ed-25407df614c2","type":"LinearAxis"},{"attributes":{"formatter":{"id":"857ac288-adb9-4384-8a11-d247a1a31e87","type":"BasicTickFormatter"},"plot":{"id":"a2d97918-d305-4b26-be77-d75f8fa88b4a","subtype":"Figure","type":"Plot"},"ticker":{"id":"602feeef-e7c1-4ea4-9658-fb50809a99ac","type":"BasicTicker"},"visible":false},"id":"1273982a-570f-459e-a9ca-65f07dae836b","type":"LinearAxis"},{"attributes":{},"id":"02ce85d1-3ea4-493c-abca-4b9612fa9d16","type":"LinearScale"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"7b0508bb-0b64-4e43-8469-bc4d98e9d84c","type":"ImageURL"},{"attributes":{"dimension":1,"plot":{"id":"a2d97918-d305-4b26-be77-d75f8fa88b4a","subtype":"Figure","type":"Plot"},"ticker":{"id":"0024e9e1-42d5-48f3-a939-d854c76bdaad","type":"BasicTicker"},"visible":false},"id":"541a97f5-3466-408a-bb07-2f8ec55a8262","type":"Grid"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"61f4411e-f1df-428a-a236-d045942667c9","type":"ImageURL"},{"attributes":{"below":[{"id":"1273982a-570f-459e-a9ca-65f07dae836b","type":"LinearAxis"}],"left":[{"id":"914b168f-18fe-499a-95ed-25407df614c2","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"1273982a-570f-459e-a9ca-65f07dae836b","type":"LinearAxis"},{"id":"e52fa527-7fc1-4042-85be-e434e4f83783","type":"Grid"},{"id":"914b168f-18fe-499a-95ed-25407df614c2","type":"LinearAxis"},{"id":"541a97f5-3466-408a-bb07-2f8ec55a8262","type":"Grid"},{"id":"59ecf6e3-a55a-4c36-af97-63657834ef44","type":"GlyphRenderer"}],"title":{"id":"41aed137-95e6-4dab-8c32-91228758f25d","type":"Title"},"toolbar":{"id":"234d7578-a262-42bb-82c2-22335a348f8e","type":"Toolbar"},"x_range":{"id":"72be1a26-406a-4f1c-b3b7-ee3ddf370502","type":"Range1d"},"x_scale":{"id":"02ce85d1-3ea4-493c-abca-4b9612fa9d16","type":"LinearScale"},"y_range":{"id":"8a80b7d3-b46f-4e17-b082-ad87b05d4b36","type":"Range1d"},"y_scale":{"id":"1a38d252-04e7-4f84-a80f-cf6c7f26b953","type":"LinearScale"}},"id":"a2d97918-d305-4b26-be77-d75f8fa88b4a","subtype":"Figure","type":"Plot"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerAL14O21_000.png"],"url_num":["000"]},"selected":{"id":"a9a6293e-c60e-4e3f-b188-8eed568ae1df","type":"Selection"},"selection_policy":{"id":"4fb9c346-5f72-4224-8f81-11c6851ed22e","type":"UnionRenderers"}},"id":"84d35420-330c-48dd-bbee-fcc76a898b3f","type":"ColumnDataSource"},{"attributes":{},"id":"857ac288-adb9-4384-8a11-d247a1a31e87","type":"BasicTickFormatter"},{"attributes":{"callback":{"id":"1e36dcde-10eb-478d-9ce0-0fe67f502cd3","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"2ee3bd84-ceab-4b3b-925d-b6cce542a4e4","type":"Slider"},{"attributes":{"children":[{"id":"1c067010-88c1-4c88-932d-389fda81c369","type":"WidgetBox"},{"id":"a2d97918-d305-4b26-be77-d75f8fa88b4a","subtype":"Figure","type":"Plot"}]},"id":"0046295b-5c99-4670-80a7-c2eea85d0524","type":"Column"},{"attributes":{"plot":null,"text":""},"id":"41aed137-95e6-4dab-8c32-91228758f25d","type":"Title"},{"attributes":{},"id":"96d6032f-e217-4c59-a8a6-b634d147e986","type":"BasicTickFormatter"},{"attributes":{"children":[{"id":"2ee3bd84-ceab-4b3b-925d-b6cce542a4e4","type":"Slider"}]},"id":"1c067010-88c1-4c88-932d-389fda81c369","type":"WidgetBox"},{"attributes":{},"id":"a9a6293e-c60e-4e3f-b188-8eed568ae1df","type":"Selection"},{"attributes":{"data_source":{"id":"84d35420-330c-48dd-bbee-fcc76a898b3f","type":"ColumnDataSource"},"glyph":{"id":"61f4411e-f1df-428a-a236-d045942667c9","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"7b0508bb-0b64-4e43-8469-bc4d98e9d84c","type":"ImageURL"},"selection_glyph":null,"view":{"id":"fd824d8e-ada1-4dc2-8c2a-222a5fafde3a","type":"CDSView"}},"id":"59ecf6e3-a55a-4c36-af97-63657834ef44","type":"GlyphRenderer"},{"attributes":{"args":{"source":{"id":"84d35420-330c-48dd-bbee-fcc76a898b3f","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"1e36dcde-10eb-478d-9ce0-0fe67f502cd3","type":"CustomJS"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"234d7578-a262-42bb-82c2-22335a348f8e","type":"Toolbar"},{"attributes":{"callback":null},"id":"72be1a26-406a-4f1c-b3b7-ee3ddf370502","type":"Range1d"},{"attributes":{},"id":"1a38d252-04e7-4f84-a80f-cf6c7f26b953","type":"LinearScale"},{"attributes":{"source":{"id":"84d35420-330c-48dd-bbee-fcc76a898b3f","type":"ColumnDataSource"}},"id":"fd824d8e-ada1-4dc2-8c2a-222a5fafde3a","type":"CDSView"},{"attributes":{},"id":"602feeef-e7c1-4ea4-9658-fb50809a99ac","type":"BasicTicker"},{"attributes":{},"id":"4fb9c346-5f72-4224-8f81-11c6851ed22e","type":"UnionRenderers"}],"root_ids":["0046295b-5c99-4670-80a7-c2eea85d0524"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"a17e0b4d-8bea-4f99-a055-3b4a2e27145d","roots":{"0046295b-5c99-4670-80a7-c2eea85d0524":"edd030d3-095e-4d44-8166-387b0058f286"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();