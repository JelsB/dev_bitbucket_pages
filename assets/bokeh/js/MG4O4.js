(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("1e806c78-46e8-4ad6-8952-f2f4fd3787f9");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '1e806c78-46e8-4ad6-8952-f2f4fd3787f9' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"1f6db1ec-1f16-4736-877c-5f15b7ed8b57":{"roots":{"references":[{"attributes":{},"id":"ec0d6577-f13c-46c7-b400-704271adfe73","type":"Selection"},{"attributes":{"plot":{"id":"508809af-8786-42fc-aed4-c00156f1d0d4","subtype":"Figure","type":"Plot"},"ticker":{"id":"9209e41f-9935-4e45-849d-0a777bf5e889","type":"BasicTicker"},"visible":false},"id":"679150c8-9565-4168-9f17-8b04c4b1951d","type":"Grid"},{"attributes":{},"id":"4f3c3f9a-f015-4ff3-96fb-a707ea4f7637","type":"LinearScale"},{"attributes":{"callback":null},"id":"39d39834-f1d0-47be-8b8a-6b51d3241296","type":"Range1d"},{"attributes":{},"id":"4b6ae8d4-bf1d-4c6a-a98d-f25cae74d663","type":"LinearScale"},{"attributes":{"callback":null},"id":"0ef1996c-4910-4ece-95f3-08775f85cedb","type":"Range1d"},{"attributes":{},"id":"9209e41f-9935-4e45-849d-0a777bf5e889","type":"BasicTicker"},{"attributes":{"callback":{"id":"d0ff8ab7-b3cb-43e0-8147-8359f47e8d00","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"c0a50e4e-837a-4f67-b7ec-25bb76a16cd4","type":"Slider"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"1c8cfff0-ee8a-4e22-baa2-81579a1f400f","type":"ImageURL"},{"attributes":{"formatter":{"id":"e7cb5c69-f090-4872-a9d6-2b6f2278aa5b","type":"BasicTickFormatter"},"plot":{"id":"508809af-8786-42fc-aed4-c00156f1d0d4","subtype":"Figure","type":"Plot"},"ticker":{"id":"9209e41f-9935-4e45-849d-0a777bf5e889","type":"BasicTicker"},"visible":false},"id":"f3f2086d-897a-4e34-8eb8-cb98f568f48d","type":"LinearAxis"},{"attributes":{"children":[{"id":"c0a50e4e-837a-4f67-b7ec-25bb76a16cd4","type":"Slider"}]},"id":"99f94132-915f-40fe-961a-1b40d616f719","type":"WidgetBox"},{"attributes":{"dimension":1,"plot":{"id":"508809af-8786-42fc-aed4-c00156f1d0d4","subtype":"Figure","type":"Plot"},"ticker":{"id":"1a55cfef-f7c3-49f3-91f8-33092518d659","type":"BasicTicker"},"visible":false},"id":"5cae9d51-234a-43df-9660-07ad347f2d37","type":"Grid"},{"attributes":{},"id":"0224f21a-a29c-4435-8266-8934cc26610c","type":"UnionRenderers"},{"attributes":{"source":{"id":"c2fccb13-29ca-4222-ab7a-91e70476dba9","type":"ColumnDataSource"}},"id":"765eff66-8e39-4f48-acb3-7b65800dbf2c","type":"CDSView"},{"attributes":{"formatter":{"id":"09cfa93b-2e20-4ee0-a80a-e923ce47c740","type":"BasicTickFormatter"},"plot":{"id":"508809af-8786-42fc-aed4-c00156f1d0d4","subtype":"Figure","type":"Plot"},"ticker":{"id":"1a55cfef-f7c3-49f3-91f8-33092518d659","type":"BasicTicker"},"visible":false},"id":"4871e234-ea5d-4d81-8d72-b51eed64013b","type":"LinearAxis"},{"attributes":{"args":{"source":{"id":"c2fccb13-29ca-4222-ab7a-91e70476dba9","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"d0ff8ab7-b3cb-43e0-8147-8359f47e8d00","type":"CustomJS"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"b7d66f53-2e2f-493b-9a81-70441a76bf4b","type":"ImageURL"},{"attributes":{"data_source":{"id":"c2fccb13-29ca-4222-ab7a-91e70476dba9","type":"ColumnDataSource"},"glyph":{"id":"1c8cfff0-ee8a-4e22-baa2-81579a1f400f","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"b7d66f53-2e2f-493b-9a81-70441a76bf4b","type":"ImageURL"},"selection_glyph":null,"view":{"id":"765eff66-8e39-4f48-acb3-7b65800dbf2c","type":"CDSView"}},"id":"372958dc-a71b-4e52-8b2e-318cf61faab6","type":"GlyphRenderer"},{"attributes":{},"id":"1a55cfef-f7c3-49f3-91f8-33092518d659","type":"BasicTicker"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/MG4O4_000.png"],"url_num":["000"]},"selected":{"id":"ec0d6577-f13c-46c7-b400-704271adfe73","type":"Selection"},"selection_policy":{"id":"0224f21a-a29c-4435-8266-8934cc26610c","type":"UnionRenderers"}},"id":"c2fccb13-29ca-4222-ab7a-91e70476dba9","type":"ColumnDataSource"},{"attributes":{"children":[{"id":"99f94132-915f-40fe-961a-1b40d616f719","type":"WidgetBox"},{"id":"508809af-8786-42fc-aed4-c00156f1d0d4","subtype":"Figure","type":"Plot"}]},"id":"b21ecdd9-bce1-4f98-b83f-661d703472be","type":"Column"},{"attributes":{"plot":null,"text":""},"id":"c5299e5d-669b-4ea5-b8ee-ce610d51fca8","type":"Title"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"15ba2629-c940-4d81-910f-7202769c0cb8","type":"Toolbar"},{"attributes":{"below":[{"id":"f3f2086d-897a-4e34-8eb8-cb98f568f48d","type":"LinearAxis"}],"left":[{"id":"4871e234-ea5d-4d81-8d72-b51eed64013b","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"f3f2086d-897a-4e34-8eb8-cb98f568f48d","type":"LinearAxis"},{"id":"679150c8-9565-4168-9f17-8b04c4b1951d","type":"Grid"},{"id":"4871e234-ea5d-4d81-8d72-b51eed64013b","type":"LinearAxis"},{"id":"5cae9d51-234a-43df-9660-07ad347f2d37","type":"Grid"},{"id":"372958dc-a71b-4e52-8b2e-318cf61faab6","type":"GlyphRenderer"}],"title":{"id":"c5299e5d-669b-4ea5-b8ee-ce610d51fca8","type":"Title"},"toolbar":{"id":"15ba2629-c940-4d81-910f-7202769c0cb8","type":"Toolbar"},"x_range":{"id":"0ef1996c-4910-4ece-95f3-08775f85cedb","type":"Range1d"},"x_scale":{"id":"4b6ae8d4-bf1d-4c6a-a98d-f25cae74d663","type":"LinearScale"},"y_range":{"id":"39d39834-f1d0-47be-8b8a-6b51d3241296","type":"Range1d"},"y_scale":{"id":"4f3c3f9a-f015-4ff3-96fb-a707ea4f7637","type":"LinearScale"}},"id":"508809af-8786-42fc-aed4-c00156f1d0d4","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"09cfa93b-2e20-4ee0-a80a-e923ce47c740","type":"BasicTickFormatter"},{"attributes":{},"id":"e7cb5c69-f090-4872-a9d6-2b6f2278aa5b","type":"BasicTickFormatter"}],"root_ids":["b21ecdd9-bce1-4f98-b83f-661d703472be"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"1f6db1ec-1f16-4736-877c-5f15b7ed8b57","roots":{"b21ecdd9-bce1-4f98-b83f-661d703472be":"1e806c78-46e8-4ad6-8952-f2f4fd3787f9"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();