(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("ba175d63-87e6-4701-9a97-9c258bd598e1");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'ba175d63-87e6-4701-9a97-9c258bd598e1' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"50d6f75b-65ff-4aeb-bbfd-163ab198f8ec":{"roots":{"references":[{"attributes":{"callback":{"id":"6b3b3e2f-2034-4829-bd3a-7b1d7557a4c8","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"f078cf25-6e67-4979-9bdf-2a63cfa28bd8","type":"Slider"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/TI3O6_000.png"],"url_num":["000"]},"selected":{"id":"5720f3db-1d6b-4e32-b693-ef27ee696e67","type":"Selection"},"selection_policy":{"id":"8cbdd1f9-f2cb-4d7e-86d7-425cb90eb0e0","type":"UnionRenderers"}},"id":"346b1efd-330e-4d47-b736-4725155fd183","type":"ColumnDataSource"},{"attributes":{"formatter":{"id":"b2bcf199-8b5f-44a3-98eb-12f4a1635080","type":"BasicTickFormatter"},"plot":{"id":"1c58c9a4-1363-48c9-b2ae-d2344236a74e","subtype":"Figure","type":"Plot"},"ticker":{"id":"6e9d1d2a-4421-4147-9256-3c4afd5bdd82","type":"BasicTicker"},"visible":false},"id":"00504e8a-e43b-4552-acd2-8c12b68528a7","type":"LinearAxis"},{"attributes":{"callback":null},"id":"d937682e-8503-413f-82ee-1cafed9ca3f2","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"59e80b7f-52d9-4368-a3b6-3663665771a2","type":"ImageURL"},{"attributes":{"plot":null,"text":""},"id":"5ff4be55-0a27-46b0-9c96-496f7a0c0f95","type":"Title"},{"attributes":{"callback":null},"id":"7535cdb1-3b51-4163-8667-9cb25238c3af","type":"Range1d"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"6371e7b2-dfa3-4046-b3b7-7054fdc4d541","type":"Toolbar"},{"attributes":{},"id":"1899431c-3491-4214-8956-922302957195","type":"LinearScale"},{"attributes":{},"id":"01fbe1d6-5e03-486f-a25c-1f52f69edb52","type":"BasicTickFormatter"},{"attributes":{"dimension":1,"plot":{"id":"1c58c9a4-1363-48c9-b2ae-d2344236a74e","subtype":"Figure","type":"Plot"},"ticker":{"id":"173aec26-05c9-4740-b4f2-57f8a2028cc4","type":"BasicTicker"},"visible":false},"id":"ec18d9f4-7e22-48f6-a2d9-01112f90ab89","type":"Grid"},{"attributes":{"children":[{"id":"f078cf25-6e67-4979-9bdf-2a63cfa28bd8","type":"Slider"}]},"id":"1e82b1be-e56a-467e-b72b-b1a30d12d1d1","type":"WidgetBox"},{"attributes":{"plot":{"id":"1c58c9a4-1363-48c9-b2ae-d2344236a74e","subtype":"Figure","type":"Plot"},"ticker":{"id":"6e9d1d2a-4421-4147-9256-3c4afd5bdd82","type":"BasicTicker"},"visible":false},"id":"fe026dc7-07af-4521-a0eb-f46a65fe03e9","type":"Grid"},{"attributes":{},"id":"5720f3db-1d6b-4e32-b693-ef27ee696e67","type":"Selection"},{"attributes":{"children":[{"id":"1e82b1be-e56a-467e-b72b-b1a30d12d1d1","type":"WidgetBox"},{"id":"1c58c9a4-1363-48c9-b2ae-d2344236a74e","subtype":"Figure","type":"Plot"}]},"id":"f763a010-9c58-4eba-9c95-5a30c82ed744","type":"Column"},{"attributes":{},"id":"b2bcf199-8b5f-44a3-98eb-12f4a1635080","type":"BasicTickFormatter"},{"attributes":{"formatter":{"id":"01fbe1d6-5e03-486f-a25c-1f52f69edb52","type":"BasicTickFormatter"},"plot":{"id":"1c58c9a4-1363-48c9-b2ae-d2344236a74e","subtype":"Figure","type":"Plot"},"ticker":{"id":"173aec26-05c9-4740-b4f2-57f8a2028cc4","type":"BasicTicker"},"visible":false},"id":"e0479c0d-292e-459a-a06b-ba64870fc91a","type":"LinearAxis"},{"attributes":{},"id":"173aec26-05c9-4740-b4f2-57f8a2028cc4","type":"BasicTicker"},{"attributes":{"args":{"source":{"id":"346b1efd-330e-4d47-b736-4725155fd183","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"6b3b3e2f-2034-4829-bd3a-7b1d7557a4c8","type":"CustomJS"},{"attributes":{},"id":"8cbdd1f9-f2cb-4d7e-86d7-425cb90eb0e0","type":"UnionRenderers"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"12119255-dfc4-4691-9110-4365de8bdae6","type":"ImageURL"},{"attributes":{"data_source":{"id":"346b1efd-330e-4d47-b736-4725155fd183","type":"ColumnDataSource"},"glyph":{"id":"59e80b7f-52d9-4368-a3b6-3663665771a2","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"12119255-dfc4-4691-9110-4365de8bdae6","type":"ImageURL"},"selection_glyph":null,"view":{"id":"a2883075-4133-419e-844f-ef75a01ada6d","type":"CDSView"}},"id":"6949b5fe-f4fe-4efb-87de-fb79da5ceecf","type":"GlyphRenderer"},{"attributes":{"source":{"id":"346b1efd-330e-4d47-b736-4725155fd183","type":"ColumnDataSource"}},"id":"a2883075-4133-419e-844f-ef75a01ada6d","type":"CDSView"},{"attributes":{},"id":"5d823393-0806-4e50-a204-a11b05fe278a","type":"LinearScale"},{"attributes":{},"id":"6e9d1d2a-4421-4147-9256-3c4afd5bdd82","type":"BasicTicker"},{"attributes":{"below":[{"id":"00504e8a-e43b-4552-acd2-8c12b68528a7","type":"LinearAxis"}],"left":[{"id":"e0479c0d-292e-459a-a06b-ba64870fc91a","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"00504e8a-e43b-4552-acd2-8c12b68528a7","type":"LinearAxis"},{"id":"fe026dc7-07af-4521-a0eb-f46a65fe03e9","type":"Grid"},{"id":"e0479c0d-292e-459a-a06b-ba64870fc91a","type":"LinearAxis"},{"id":"ec18d9f4-7e22-48f6-a2d9-01112f90ab89","type":"Grid"},{"id":"6949b5fe-f4fe-4efb-87de-fb79da5ceecf","type":"GlyphRenderer"}],"title":{"id":"5ff4be55-0a27-46b0-9c96-496f7a0c0f95","type":"Title"},"toolbar":{"id":"6371e7b2-dfa3-4046-b3b7-7054fdc4d541","type":"Toolbar"},"x_range":{"id":"d937682e-8503-413f-82ee-1cafed9ca3f2","type":"Range1d"},"x_scale":{"id":"1899431c-3491-4214-8956-922302957195","type":"LinearScale"},"y_range":{"id":"7535cdb1-3b51-4163-8667-9cb25238c3af","type":"Range1d"},"y_scale":{"id":"5d823393-0806-4e50-a204-a11b05fe278a","type":"LinearScale"}},"id":"1c58c9a4-1363-48c9-b2ae-d2344236a74e","subtype":"Figure","type":"Plot"}],"root_ids":["f763a010-9c58-4eba-9c95-5a30c82ed744"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"50d6f75b-65ff-4aeb-bbfd-163ab198f8ec","roots":{"f763a010-9c58-4eba-9c95-5a30c82ed744":"ba175d63-87e6-4701-9a97-9c258bd598e1"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();