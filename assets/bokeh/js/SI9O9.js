(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("e296870a-f766-4956-be0c-f4244dba6cc9");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'e296870a-f766-4956-be0c-f4244dba6cc9' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"bd1102ed-c85e-4b6f-8672-e1822d5282c1":{"roots":{"references":[{"attributes":{"formatter":{"id":"8196e675-d06b-4a35-b5d6-1a8364a1ab1c","type":"BasicTickFormatter"},"plot":{"id":"7248ecc8-5972-4f00-a83a-8a8f4dba22dc","subtype":"Figure","type":"Plot"},"ticker":{"id":"2b0e3d35-c299-4362-8cc9-401c2493a315","type":"BasicTicker"},"visible":false},"id":"8f1af274-e4b8-45b4-91e8-f8fcc12f052a","type":"LinearAxis"},{"attributes":{},"id":"8196e675-d06b-4a35-b5d6-1a8364a1ab1c","type":"BasicTickFormatter"},{"attributes":{"children":[{"id":"49932cad-7bb0-4e74-8c90-696194d66b87","type":"WidgetBox"},{"id":"7248ecc8-5972-4f00-a83a-8a8f4dba22dc","subtype":"Figure","type":"Plot"}]},"id":"7fb7899c-2e41-45a4-b44b-f2705370f76a","type":"Column"},{"attributes":{},"id":"b83e30ce-58ea-4900-b13d-daea59029f48","type":"LinearScale"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"42ce7c92-e429-4f29-8ede-f9eb2d84c715","type":"ImageURL"},{"attributes":{},"id":"2b0e3d35-c299-4362-8cc9-401c2493a315","type":"BasicTicker"},{"attributes":{"children":[{"id":"c8bc23c9-8073-4156-a998-f47d790c6f71","type":"Slider"}]},"id":"49932cad-7bb0-4e74-8c90-696194d66b87","type":"WidgetBox"},{"attributes":{},"id":"b1fb7667-3552-4ba9-989d-8df08e4ce134","type":"BasicTickFormatter"},{"attributes":{"dimension":1,"plot":{"id":"7248ecc8-5972-4f00-a83a-8a8f4dba22dc","subtype":"Figure","type":"Plot"},"ticker":{"id":"2b0e3d35-c299-4362-8cc9-401c2493a315","type":"BasicTicker"},"visible":false},"id":"ea72ca73-34be-42d9-8fa3-eb9271e93ae4","type":"Grid"},{"attributes":{"args":{"source":{"id":"2a99b70a-34d4-404a-b976-974b4c8bb9a7","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"348a97ba-c47e-4787-b307-4498d64ab24f","type":"CustomJS"},{"attributes":{"plot":null,"text":""},"id":"241b6d49-06db-4245-8c5b-20dfef9d7d84","type":"Title"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"3b1a0823-58a8-450d-896c-db8886c3d881","type":"Toolbar"},{"attributes":{},"id":"468e2613-a3b6-4aa1-9850-619bd511e71a","type":"BasicTicker"},{"attributes":{"data_source":{"id":"2a99b70a-34d4-404a-b976-974b4c8bb9a7","type":"ColumnDataSource"},"glyph":{"id":"42ce7c92-e429-4f29-8ede-f9eb2d84c715","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"dc9c3ca9-0dcc-44f0-9fe4-4ff5a9078989","type":"ImageURL"},"selection_glyph":null,"view":{"id":"6c0fff79-1709-4dfa-b51e-de38b488310b","type":"CDSView"}},"id":"aa1026f0-ccd3-4904-8e29-c7661c701b8e","type":"GlyphRenderer"},{"attributes":{"plot":{"id":"7248ecc8-5972-4f00-a83a-8a8f4dba22dc","subtype":"Figure","type":"Plot"},"ticker":{"id":"468e2613-a3b6-4aa1-9850-619bd511e71a","type":"BasicTicker"},"visible":false},"id":"6464a142-111b-461c-bafe-49badcea106f","type":"Grid"},{"attributes":{},"id":"e319b197-cb30-4c52-8ba4-1e8b988746e4","type":"Selection"},{"attributes":{"callback":null},"id":"1ed68d65-cce0-407b-bb0d-f29b5f8fc57d","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"dc9c3ca9-0dcc-44f0-9fe4-4ff5a9078989","type":"ImageURL"},{"attributes":{"callback":{"id":"348a97ba-c47e-4787-b307-4498d64ab24f","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"c8bc23c9-8073-4156-a998-f47d790c6f71","type":"Slider"},{"attributes":{"callback":null},"id":"26e353bc-acda-4f83-b0be-fd1cd1b499b1","type":"Range1d"},{"attributes":{"formatter":{"id":"b1fb7667-3552-4ba9-989d-8df08e4ce134","type":"BasicTickFormatter"},"plot":{"id":"7248ecc8-5972-4f00-a83a-8a8f4dba22dc","subtype":"Figure","type":"Plot"},"ticker":{"id":"468e2613-a3b6-4aa1-9850-619bd511e71a","type":"BasicTicker"},"visible":false},"id":"5867870f-4af2-4c86-8fac-b93c968bc08d","type":"LinearAxis"},{"attributes":{},"id":"3129c505-667a-4bdb-a765-84a300fec64f","type":"LinearScale"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/SI9O9_000.png"],"url_num":["000"]},"selected":{"id":"e319b197-cb30-4c52-8ba4-1e8b988746e4","type":"Selection"},"selection_policy":{"id":"d172ae51-4ce9-436e-9595-bb7059e970ed","type":"UnionRenderers"}},"id":"2a99b70a-34d4-404a-b976-974b4c8bb9a7","type":"ColumnDataSource"},{"attributes":{"source":{"id":"2a99b70a-34d4-404a-b976-974b4c8bb9a7","type":"ColumnDataSource"}},"id":"6c0fff79-1709-4dfa-b51e-de38b488310b","type":"CDSView"},{"attributes":{},"id":"d172ae51-4ce9-436e-9595-bb7059e970ed","type":"UnionRenderers"},{"attributes":{"below":[{"id":"5867870f-4af2-4c86-8fac-b93c968bc08d","type":"LinearAxis"}],"left":[{"id":"8f1af274-e4b8-45b4-91e8-f8fcc12f052a","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"5867870f-4af2-4c86-8fac-b93c968bc08d","type":"LinearAxis"},{"id":"6464a142-111b-461c-bafe-49badcea106f","type":"Grid"},{"id":"8f1af274-e4b8-45b4-91e8-f8fcc12f052a","type":"LinearAxis"},{"id":"ea72ca73-34be-42d9-8fa3-eb9271e93ae4","type":"Grid"},{"id":"aa1026f0-ccd3-4904-8e29-c7661c701b8e","type":"GlyphRenderer"}],"title":{"id":"241b6d49-06db-4245-8c5b-20dfef9d7d84","type":"Title"},"toolbar":{"id":"3b1a0823-58a8-450d-896c-db8886c3d881","type":"Toolbar"},"x_range":{"id":"1ed68d65-cce0-407b-bb0d-f29b5f8fc57d","type":"Range1d"},"x_scale":{"id":"b83e30ce-58ea-4900-b13d-daea59029f48","type":"LinearScale"},"y_range":{"id":"26e353bc-acda-4f83-b0be-fd1cd1b499b1","type":"Range1d"},"y_scale":{"id":"3129c505-667a-4bdb-a765-84a300fec64f","type":"LinearScale"}},"id":"7248ecc8-5972-4f00-a83a-8a8f4dba22dc","subtype":"Figure","type":"Plot"}],"root_ids":["7fb7899c-2e41-45a4-b44b-f2705370f76a"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"bd1102ed-c85e-4b6f-8672-e1822d5282c1","roots":{"7fb7899c-2e41-45a4-b44b-f2705370f76a":"e296870a-f766-4956-be0c-f4244dba6cc9"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();