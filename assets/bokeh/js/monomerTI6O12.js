(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("6bb6fb25-7fa7-4b4f-b65c-aa0db9c943b4");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '6bb6fb25-7fa7-4b4f-b65c-aa0db9c943b4' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"a94e10a9-da3c-4166-ac68-4ad1415b815f":{"roots":{"references":[{"attributes":{"below":[{"id":"1c986b5f-16f6-4b47-b131-ac42c668a052","type":"LinearAxis"}],"left":[{"id":"841eab7d-0740-4ac0-b4c9-d71f3ba7a08f","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"1c986b5f-16f6-4b47-b131-ac42c668a052","type":"LinearAxis"},{"id":"06491b15-4cd0-44df-99db-331a03c6131a","type":"Grid"},{"id":"841eab7d-0740-4ac0-b4c9-d71f3ba7a08f","type":"LinearAxis"},{"id":"23e496b0-7901-417a-9b49-5f63868b74e9","type":"Grid"},{"id":"ac61edb4-1deb-4e26-bef9-1501fb9685fb","type":"GlyphRenderer"}],"title":{"id":"b69c1eab-92e5-4f30-830a-ddd193b9d72b","type":"Title"},"toolbar":{"id":"9848abb6-813b-4a5e-a9d6-07cd0e98ec22","type":"Toolbar"},"x_range":{"id":"046a0686-4ef3-4cfb-b8ae-0bb47de47ebe","type":"Range1d"},"x_scale":{"id":"3274399c-fc08-45aa-bba1-b910421b059a","type":"LinearScale"},"y_range":{"id":"4967f7e5-73f2-451d-90a4-de4674791d2a","type":"Range1d"},"y_scale":{"id":"0c2e4880-069e-4fe2-ad5d-72adc7a6f0f0","type":"LinearScale"}},"id":"164ea8f2-dbd3-4dee-9efd-2bd65db45936","subtype":"Figure","type":"Plot"},{"attributes":{"formatter":{"id":"f09383b4-9f1f-413c-8b3e-2ad953e6d042","type":"BasicTickFormatter"},"plot":{"id":"164ea8f2-dbd3-4dee-9efd-2bd65db45936","subtype":"Figure","type":"Plot"},"ticker":{"id":"31ce66ea-cb13-41a4-992a-7785ba9920e0","type":"BasicTicker"},"visible":false},"id":"1c986b5f-16f6-4b47-b131-ac42c668a052","type":"LinearAxis"},{"attributes":{"callback":null},"id":"046a0686-4ef3-4cfb-b8ae-0bb47de47ebe","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"7a83006a-af6e-4f3d-83b9-11d0eba6e662","type":"ImageURL"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"896c8ea3-5183-4912-be60-ac2278b69a63","type":"ImageURL"},{"attributes":{},"id":"e7fc66cf-bf3e-4313-bc87-00b05c0be99b","type":"UnionRenderers"},{"attributes":{"callback":null},"id":"4967f7e5-73f2-451d-90a4-de4674791d2a","type":"Range1d"},{"attributes":{},"id":"f09383b4-9f1f-413c-8b3e-2ad953e6d042","type":"BasicTickFormatter"},{"attributes":{},"id":"2cdf37be-ba37-4e2b-a38d-18aa04e18bf7","type":"BasicTickFormatter"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"9848abb6-813b-4a5e-a9d6-07cd0e98ec22","type":"Toolbar"},{"attributes":{},"id":"31ce66ea-cb13-41a4-992a-7785ba9920e0","type":"BasicTicker"},{"attributes":{},"id":"3274399c-fc08-45aa-bba1-b910421b059a","type":"LinearScale"},{"attributes":{"callback":{"id":"747ae964-170e-4234-bbc1-de7354f3e4db","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"e1528a8a-d3bb-4cbf-8d5a-0de615999a19","type":"Slider"},{"attributes":{"plot":null,"text":""},"id":"b69c1eab-92e5-4f30-830a-ddd193b9d72b","type":"Title"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerTI6O12_000.png"],"url_num":["000"]},"selected":{"id":"57932884-2130-4c42-b79b-8dc277ca4e29","type":"Selection"},"selection_policy":{"id":"e7fc66cf-bf3e-4313-bc87-00b05c0be99b","type":"UnionRenderers"}},"id":"0f412493-503e-4bb1-9869-5da760fb572f","type":"ColumnDataSource"},{"attributes":{},"id":"0c2e4880-069e-4fe2-ad5d-72adc7a6f0f0","type":"LinearScale"},{"attributes":{"plot":{"id":"164ea8f2-dbd3-4dee-9efd-2bd65db45936","subtype":"Figure","type":"Plot"},"ticker":{"id":"31ce66ea-cb13-41a4-992a-7785ba9920e0","type":"BasicTicker"},"visible":false},"id":"06491b15-4cd0-44df-99db-331a03c6131a","type":"Grid"},{"attributes":{"source":{"id":"0f412493-503e-4bb1-9869-5da760fb572f","type":"ColumnDataSource"}},"id":"86f752a4-b145-42f3-85e0-01ccfafc3144","type":"CDSView"},{"attributes":{"children":[{"id":"99d9f7e1-5bb8-4da0-8426-705f9b61f949","type":"WidgetBox"},{"id":"164ea8f2-dbd3-4dee-9efd-2bd65db45936","subtype":"Figure","type":"Plot"}]},"id":"eaf42083-57c7-4199-a76a-8df2888a0727","type":"Column"},{"attributes":{"formatter":{"id":"2cdf37be-ba37-4e2b-a38d-18aa04e18bf7","type":"BasicTickFormatter"},"plot":{"id":"164ea8f2-dbd3-4dee-9efd-2bd65db45936","subtype":"Figure","type":"Plot"},"ticker":{"id":"5eb140d2-7fba-4116-bad6-46069b58c70f","type":"BasicTicker"},"visible":false},"id":"841eab7d-0740-4ac0-b4c9-d71f3ba7a08f","type":"LinearAxis"},{"attributes":{"args":{"source":{"id":"0f412493-503e-4bb1-9869-5da760fb572f","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"747ae964-170e-4234-bbc1-de7354f3e4db","type":"CustomJS"},{"attributes":{},"id":"57932884-2130-4c42-b79b-8dc277ca4e29","type":"Selection"},{"attributes":{"children":[{"id":"e1528a8a-d3bb-4cbf-8d5a-0de615999a19","type":"Slider"}]},"id":"99d9f7e1-5bb8-4da0-8426-705f9b61f949","type":"WidgetBox"},{"attributes":{},"id":"5eb140d2-7fba-4116-bad6-46069b58c70f","type":"BasicTicker"},{"attributes":{"dimension":1,"plot":{"id":"164ea8f2-dbd3-4dee-9efd-2bd65db45936","subtype":"Figure","type":"Plot"},"ticker":{"id":"5eb140d2-7fba-4116-bad6-46069b58c70f","type":"BasicTicker"},"visible":false},"id":"23e496b0-7901-417a-9b49-5f63868b74e9","type":"Grid"},{"attributes":{"data_source":{"id":"0f412493-503e-4bb1-9869-5da760fb572f","type":"ColumnDataSource"},"glyph":{"id":"896c8ea3-5183-4912-be60-ac2278b69a63","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"7a83006a-af6e-4f3d-83b9-11d0eba6e662","type":"ImageURL"},"selection_glyph":null,"view":{"id":"86f752a4-b145-42f3-85e0-01ccfafc3144","type":"CDSView"}},"id":"ac61edb4-1deb-4e26-bef9-1501fb9685fb","type":"GlyphRenderer"}],"root_ids":["eaf42083-57c7-4199-a76a-8df2888a0727"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"a94e10a9-da3c-4166-ac68-4ad1415b815f","roots":{"eaf42083-57c7-4199-a76a-8df2888a0727":"6bb6fb25-7fa7-4b4f-b65c-aa0db9c943b4"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();