(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("91084ee4-a29d-4fa0-9ce8-e99dcc0fb703");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '91084ee4-a29d-4fa0-9ce8-e99dcc0fb703' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"8c37e9e0-6cc0-4867-b644-be9fbe360ad8":{"roots":{"references":[{"attributes":{"callback":null},"id":"2f517b3a-3e67-4e1a-a4a7-e5c44b3013e3","type":"Range1d"},{"attributes":{"source":{"id":"db46183e-f418-41aa-a2a3-6fb844902479","type":"ColumnDataSource"}},"id":"da4b5032-a452-4ab1-9904-c4fe5809cf0b","type":"CDSView"},{"attributes":{},"id":"0cdc0576-5147-4453-b2d7-a12824edf7a0","type":"UnionRenderers"},{"attributes":{},"id":"accfaca6-0c6e-42ed-af00-1283d3e3131e","type":"BasicTickFormatter"},{"attributes":{"callback":{"id":"6747479a-4278-4e3a-8d10-7c130a1c20f6","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"c508019f-8d96-4a75-bb00-91c2841fecf4","type":"Slider"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerTI8O16_000.png"],"url_num":["000"]},"selected":{"id":"8aeabd72-4d77-453b-8696-672073a7d9c6","type":"Selection"},"selection_policy":{"id":"0cdc0576-5147-4453-b2d7-a12824edf7a0","type":"UnionRenderers"}},"id":"db46183e-f418-41aa-a2a3-6fb844902479","type":"ColumnDataSource"},{"attributes":{"children":[{"id":"41cd37cf-eb1a-437b-8006-c71cf34c42d7","type":"WidgetBox"},{"id":"10021ad8-b208-428b-b828-bcb10f475811","subtype":"Figure","type":"Plot"}]},"id":"e0351075-23e7-4dfa-a965-f902e1ef55ab","type":"Column"},{"attributes":{},"id":"25e443d0-5ddd-443a-a88e-7ff30483252a","type":"LinearScale"},{"attributes":{"formatter":{"id":"77a54a49-d698-4904-8e24-903c987fece9","type":"BasicTickFormatter"},"plot":{"id":"10021ad8-b208-428b-b828-bcb10f475811","subtype":"Figure","type":"Plot"},"ticker":{"id":"84c18395-a5bc-44a7-943b-9b34284b1634","type":"BasicTicker"},"visible":false},"id":"a450ad1b-3508-4ef7-aaf6-1174f2628196","type":"LinearAxis"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"b906bc92-8aa8-483c-9038-4e9a8f158e36","type":"Toolbar"},{"attributes":{},"id":"77a54a49-d698-4904-8e24-903c987fece9","type":"BasicTickFormatter"},{"attributes":{"args":{"source":{"id":"db46183e-f418-41aa-a2a3-6fb844902479","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"6747479a-4278-4e3a-8d10-7c130a1c20f6","type":"CustomJS"},{"attributes":{},"id":"84c18395-a5bc-44a7-943b-9b34284b1634","type":"BasicTicker"},{"attributes":{"formatter":{"id":"accfaca6-0c6e-42ed-af00-1283d3e3131e","type":"BasicTickFormatter"},"plot":{"id":"10021ad8-b208-428b-b828-bcb10f475811","subtype":"Figure","type":"Plot"},"ticker":{"id":"111398c8-f764-4803-b784-0d5728e4ee15","type":"BasicTicker"},"visible":false},"id":"8ee05a37-559e-41eb-bba6-d5449a38d74b","type":"LinearAxis"},{"attributes":{"data_source":{"id":"db46183e-f418-41aa-a2a3-6fb844902479","type":"ColumnDataSource"},"glyph":{"id":"8c5e90cf-4818-48ea-8ec6-43b794425225","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"5f28a2bd-3f81-4eb3-b0f3-5ce6060d0bef","type":"ImageURL"},"selection_glyph":null,"view":{"id":"da4b5032-a452-4ab1-9904-c4fe5809cf0b","type":"CDSView"}},"id":"f54e66e6-7ebd-460e-96e1-2d1f7d234b1e","type":"GlyphRenderer"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"8c5e90cf-4818-48ea-8ec6-43b794425225","type":"ImageURL"},{"attributes":{"children":[{"id":"c508019f-8d96-4a75-bb00-91c2841fecf4","type":"Slider"}]},"id":"41cd37cf-eb1a-437b-8006-c71cf34c42d7","type":"WidgetBox"},{"attributes":{"dimension":1,"plot":{"id":"10021ad8-b208-428b-b828-bcb10f475811","subtype":"Figure","type":"Plot"},"ticker":{"id":"84c18395-a5bc-44a7-943b-9b34284b1634","type":"BasicTicker"},"visible":false},"id":"da3fabf7-d888-4c3f-a922-bd0e2ca472bd","type":"Grid"},{"attributes":{"plot":null,"text":""},"id":"7e6c5f97-e723-4dfc-b0bc-f7b211a63ee9","type":"Title"},{"attributes":{"below":[{"id":"8ee05a37-559e-41eb-bba6-d5449a38d74b","type":"LinearAxis"}],"left":[{"id":"a450ad1b-3508-4ef7-aaf6-1174f2628196","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"8ee05a37-559e-41eb-bba6-d5449a38d74b","type":"LinearAxis"},{"id":"ac975187-0140-4520-86e4-ee8a8c50f834","type":"Grid"},{"id":"a450ad1b-3508-4ef7-aaf6-1174f2628196","type":"LinearAxis"},{"id":"da3fabf7-d888-4c3f-a922-bd0e2ca472bd","type":"Grid"},{"id":"f54e66e6-7ebd-460e-96e1-2d1f7d234b1e","type":"GlyphRenderer"}],"title":{"id":"7e6c5f97-e723-4dfc-b0bc-f7b211a63ee9","type":"Title"},"toolbar":{"id":"b906bc92-8aa8-483c-9038-4e9a8f158e36","type":"Toolbar"},"x_range":{"id":"827d25cd-9b4b-43b5-979c-267dfd086696","type":"Range1d"},"x_scale":{"id":"25e443d0-5ddd-443a-a88e-7ff30483252a","type":"LinearScale"},"y_range":{"id":"2f517b3a-3e67-4e1a-a4a7-e5c44b3013e3","type":"Range1d"},"y_scale":{"id":"81033fce-149a-4bf8-8709-ba4126d9a034","type":"LinearScale"}},"id":"10021ad8-b208-428b-b828-bcb10f475811","subtype":"Figure","type":"Plot"},{"attributes":{"callback":null},"id":"827d25cd-9b4b-43b5-979c-267dfd086696","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"5f28a2bd-3f81-4eb3-b0f3-5ce6060d0bef","type":"ImageURL"},{"attributes":{},"id":"8aeabd72-4d77-453b-8696-672073a7d9c6","type":"Selection"},{"attributes":{},"id":"81033fce-149a-4bf8-8709-ba4126d9a034","type":"LinearScale"},{"attributes":{},"id":"111398c8-f764-4803-b784-0d5728e4ee15","type":"BasicTicker"},{"attributes":{"plot":{"id":"10021ad8-b208-428b-b828-bcb10f475811","subtype":"Figure","type":"Plot"},"ticker":{"id":"111398c8-f764-4803-b784-0d5728e4ee15","type":"BasicTicker"},"visible":false},"id":"ac975187-0140-4520-86e4-ee8a8c50f834","type":"Grid"}],"root_ids":["e0351075-23e7-4dfa-a965-f902e1ef55ab"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"8c37e9e0-6cc0-4867-b644-be9fbe360ad8","roots":{"e0351075-23e7-4dfa-a965-f902e1ef55ab":"91084ee4-a29d-4fa0-9ce8-e99dcc0fb703"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();