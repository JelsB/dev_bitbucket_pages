(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("26fcb7d8-4538-4ff0-8d9e-56a086a1fb5e");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '26fcb7d8-4538-4ff0-8d9e-56a086a1fb5e' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"1df7a452-7e95-45d8-944d-976333e43a84":{"roots":{"references":[{"attributes":{},"id":"2c2c434f-b3ad-4722-a01a-f94ef9e9a511","type":"UnionRenderers"},{"attributes":{"formatter":{"id":"afce8636-791b-49a4-917a-4e0c9c5f3cb3","type":"BasicTickFormatter"},"plot":{"id":"29ed2e36-7cd0-4a93-86d0-9fcaeab09d8d","subtype":"Figure","type":"Plot"},"ticker":{"id":"f097f594-5a42-4757-a863-25ac5d00126a","type":"BasicTicker"},"visible":false},"id":"638863e9-ced3-415f-bd0f-3a24d950515b","type":"LinearAxis"},{"attributes":{"dimension":1,"plot":{"id":"29ed2e36-7cd0-4a93-86d0-9fcaeab09d8d","subtype":"Figure","type":"Plot"},"ticker":{"id":"37a3eafb-da04-48e2-acda-a5083d76503d","type":"BasicTicker"},"visible":false},"id":"cb60eef1-7835-42fa-b035-07a1d1ebc1dd","type":"Grid"},{"attributes":{},"id":"afce8636-791b-49a4-917a-4e0c9c5f3cb3","type":"BasicTickFormatter"},{"attributes":{},"id":"2fbcf2ea-dbc7-4e87-a69d-665665aef0dd","type":"LinearScale"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"04366997-e0fc-4a88-99c5-a0f0495b186e","type":"ImageURL"},{"attributes":{},"id":"dcbdc70c-0485-4008-82c9-90a6a6fa016b","type":"BasicTickFormatter"},{"attributes":{"callback":null},"id":"69a29cec-7617-4359-b79f-7af747fba494","type":"Range1d"},{"attributes":{"formatter":{"id":"dcbdc70c-0485-4008-82c9-90a6a6fa016b","type":"BasicTickFormatter"},"plot":{"id":"29ed2e36-7cd0-4a93-86d0-9fcaeab09d8d","subtype":"Figure","type":"Plot"},"ticker":{"id":"37a3eafb-da04-48e2-acda-a5083d76503d","type":"BasicTicker"},"visible":false},"id":"56586051-b1fb-4423-aa35-13cf22d7e1d9","type":"LinearAxis"},{"attributes":{"plot":null,"text":""},"id":"24cfcc0b-6d92-425b-8b85-8ca044010107","type":"Title"},{"attributes":{"source":{"id":"6390fed1-4ced-4d66-a1bb-dd37ac485268","type":"ColumnDataSource"}},"id":"f81d52eb-8ced-44d1-88b4-e2404a4d219c","type":"CDSView"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"4df4d5a7-38e3-4c74-aea9-b698af9b3d4e","type":"Toolbar"},{"attributes":{"children":[{"id":"2991ce4a-1121-49db-87bb-81724eae7501","type":"Slider"}]},"id":"579e13ac-7289-4d55-87c7-bd73c28ca188","type":"WidgetBox"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/AL4O6_000.png"],"url_num":["000"]},"selected":{"id":"281dcff2-9d98-4b75-ab08-6bec12cfe1d5","type":"Selection"},"selection_policy":{"id":"2c2c434f-b3ad-4722-a01a-f94ef9e9a511","type":"UnionRenderers"}},"id":"6390fed1-4ced-4d66-a1bb-dd37ac485268","type":"ColumnDataSource"},{"attributes":{"args":{"source":{"id":"6390fed1-4ced-4d66-a1bb-dd37ac485268","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"f3883ac3-0ef4-4bd8-ab59-0534c30f5dbf","type":"CustomJS"},{"attributes":{},"id":"281dcff2-9d98-4b75-ab08-6bec12cfe1d5","type":"Selection"},{"attributes":{"callback":{"id":"f3883ac3-0ef4-4bd8-ab59-0534c30f5dbf","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"2991ce4a-1121-49db-87bb-81724eae7501","type":"Slider"},{"attributes":{"plot":{"id":"29ed2e36-7cd0-4a93-86d0-9fcaeab09d8d","subtype":"Figure","type":"Plot"},"ticker":{"id":"f097f594-5a42-4757-a863-25ac5d00126a","type":"BasicTicker"},"visible":false},"id":"344430f4-5a55-40e9-8f5e-6e6e32ef1cab","type":"Grid"},{"attributes":{},"id":"f097f594-5a42-4757-a863-25ac5d00126a","type":"BasicTicker"},{"attributes":{"children":[{"id":"579e13ac-7289-4d55-87c7-bd73c28ca188","type":"WidgetBox"},{"id":"29ed2e36-7cd0-4a93-86d0-9fcaeab09d8d","subtype":"Figure","type":"Plot"}]},"id":"2b85c756-4e86-405a-b234-6804987c1359","type":"Column"},{"attributes":{"below":[{"id":"638863e9-ced3-415f-bd0f-3a24d950515b","type":"LinearAxis"}],"left":[{"id":"56586051-b1fb-4423-aa35-13cf22d7e1d9","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"638863e9-ced3-415f-bd0f-3a24d950515b","type":"LinearAxis"},{"id":"344430f4-5a55-40e9-8f5e-6e6e32ef1cab","type":"Grid"},{"id":"56586051-b1fb-4423-aa35-13cf22d7e1d9","type":"LinearAxis"},{"id":"cb60eef1-7835-42fa-b035-07a1d1ebc1dd","type":"Grid"},{"id":"ada01515-02ac-433b-94ce-12dce7eb41fa","type":"GlyphRenderer"}],"title":{"id":"24cfcc0b-6d92-425b-8b85-8ca044010107","type":"Title"},"toolbar":{"id":"4df4d5a7-38e3-4c74-aea9-b698af9b3d4e","type":"Toolbar"},"x_range":{"id":"69a29cec-7617-4359-b79f-7af747fba494","type":"Range1d"},"x_scale":{"id":"9d01117e-a758-42f2-a6ea-3010b538c3f0","type":"LinearScale"},"y_range":{"id":"b743662e-8be9-4949-8ef0-3f7f416bbf86","type":"Range1d"},"y_scale":{"id":"2fbcf2ea-dbc7-4e87-a69d-665665aef0dd","type":"LinearScale"}},"id":"29ed2e36-7cd0-4a93-86d0-9fcaeab09d8d","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"37a3eafb-da04-48e2-acda-a5083d76503d","type":"BasicTicker"},{"attributes":{"data_source":{"id":"6390fed1-4ced-4d66-a1bb-dd37ac485268","type":"ColumnDataSource"},"glyph":{"id":"0506a3d7-9c39-4647-9e15-b946237e5252","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"04366997-e0fc-4a88-99c5-a0f0495b186e","type":"ImageURL"},"selection_glyph":null,"view":{"id":"f81d52eb-8ced-44d1-88b4-e2404a4d219c","type":"CDSView"}},"id":"ada01515-02ac-433b-94ce-12dce7eb41fa","type":"GlyphRenderer"},{"attributes":{},"id":"9d01117e-a758-42f2-a6ea-3010b538c3f0","type":"LinearScale"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"0506a3d7-9c39-4647-9e15-b946237e5252","type":"ImageURL"},{"attributes":{"callback":null},"id":"b743662e-8be9-4949-8ef0-3f7f416bbf86","type":"Range1d"}],"root_ids":["2b85c756-4e86-405a-b234-6804987c1359"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"1df7a452-7e95-45d8-944d-976333e43a84","roots":{"2b85c756-4e86-405a-b234-6804987c1359":"26fcb7d8-4538-4ff0-8d9e-56a086a1fb5e"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();