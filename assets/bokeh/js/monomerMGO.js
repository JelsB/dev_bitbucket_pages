(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("c0a435c6-1ae9-4a7c-b4af-f7c5f762b1a6");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'c0a435c6-1ae9-4a7c-b4af-f7c5f762b1a6' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"78d1d92c-0504-42f5-bb89-254e793bcfc8":{"roots":{"references":[{"attributes":{"source":{"id":"b72f256a-9dc3-4687-ae18-0f37a3da114c","type":"ColumnDataSource"}},"id":"274df3f5-fee9-4b48-8a8a-4e6bf4b41e45","type":"CDSView"},{"attributes":{},"id":"c1d468a1-6a50-468e-9742-06a3450a7bf1","type":"UnionRenderers"},{"attributes":{},"id":"5226e69b-d988-43e3-b610-585c2c2ca871","type":"BasicTicker"},{"attributes":{},"id":"f38e22a9-880d-4a93-991a-44b8d5ba8c16","type":"BasicTickFormatter"},{"attributes":{"callback":{"id":"e9a82723-c7f6-441d-9032-abc5c971235d","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"b6d09bd5-b649-440e-88ed-71bc17a5a06c","type":"Slider"},{"attributes":{"plot":null,"text":""},"id":"de27c9af-d45f-4e2d-b110-fa549d1e7c26","type":"Title"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"bc834a45-163e-4471-be36-2230b4d93580","type":"Toolbar"},{"attributes":{},"id":"3505a083-de2d-4c11-b4b8-8ca907865e10","type":"BasicTickFormatter"},{"attributes":{},"id":"472db585-0a2e-4f8d-be6c-5ebc184bc060","type":"Selection"},{"attributes":{"data_source":{"id":"b72f256a-9dc3-4687-ae18-0f37a3da114c","type":"ColumnDataSource"},"glyph":{"id":"3933e2ba-3a09-4ee7-b351-4b5bcd6800ba","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"1e61f1ae-09f5-49be-8000-c3982314bb25","type":"ImageURL"},"selection_glyph":null,"view":{"id":"274df3f5-fee9-4b48-8a8a-4e6bf4b41e45","type":"CDSView"}},"id":"aae58c97-2931-44b3-a5cd-adaad7bc3ff8","type":"GlyphRenderer"},{"attributes":{"children":[{"id":"b6d09bd5-b649-440e-88ed-71bc17a5a06c","type":"Slider"}]},"id":"bf5f7417-1da3-4746-a864-5cc363a73a07","type":"WidgetBox"},{"attributes":{"callback":null},"id":"4dcb7775-dd2b-491e-9d4a-6e2c3f73cae1","type":"Range1d"},{"attributes":{"dimension":1,"plot":{"id":"989769be-b7d2-496f-8a45-2a6685e52cd5","subtype":"Figure","type":"Plot"},"ticker":{"id":"5226e69b-d988-43e3-b610-585c2c2ca871","type":"BasicTicker"},"visible":false},"id":"d2ea28e0-36a7-4e00-a6de-91c03a496585","type":"Grid"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerMGO_000.png"],"url_num":["000"]},"selected":{"id":"472db585-0a2e-4f8d-be6c-5ebc184bc060","type":"Selection"},"selection_policy":{"id":"c1d468a1-6a50-468e-9742-06a3450a7bf1","type":"UnionRenderers"}},"id":"b72f256a-9dc3-4687-ae18-0f37a3da114c","type":"ColumnDataSource"},{"attributes":{"callback":null},"id":"e95d43dc-1384-40ce-a707-5fce983ebda9","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"1e61f1ae-09f5-49be-8000-c3982314bb25","type":"ImageURL"},{"attributes":{"args":{"source":{"id":"b72f256a-9dc3-4687-ae18-0f37a3da114c","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"e9a82723-c7f6-441d-9032-abc5c971235d","type":"CustomJS"},{"attributes":{},"id":"a28124d3-aef1-40ca-9776-2b078573a816","type":"LinearScale"},{"attributes":{"below":[{"id":"799c0463-331c-4a29-897f-4cb7aaf9f98b","type":"LinearAxis"}],"left":[{"id":"b97b2546-eb43-40c2-aa85-958193cfd0ad","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"799c0463-331c-4a29-897f-4cb7aaf9f98b","type":"LinearAxis"},{"id":"e0b44e68-e821-4eb9-b147-987987b938c0","type":"Grid"},{"id":"b97b2546-eb43-40c2-aa85-958193cfd0ad","type":"LinearAxis"},{"id":"d2ea28e0-36a7-4e00-a6de-91c03a496585","type":"Grid"},{"id":"aae58c97-2931-44b3-a5cd-adaad7bc3ff8","type":"GlyphRenderer"}],"title":{"id":"de27c9af-d45f-4e2d-b110-fa549d1e7c26","type":"Title"},"toolbar":{"id":"bc834a45-163e-4471-be36-2230b4d93580","type":"Toolbar"},"x_range":{"id":"4dcb7775-dd2b-491e-9d4a-6e2c3f73cae1","type":"Range1d"},"x_scale":{"id":"a28124d3-aef1-40ca-9776-2b078573a816","type":"LinearScale"},"y_range":{"id":"e95d43dc-1384-40ce-a707-5fce983ebda9","type":"Range1d"},"y_scale":{"id":"13c0c892-ba72-4d28-8f26-e940dd1a3dc7","type":"LinearScale"}},"id":"989769be-b7d2-496f-8a45-2a6685e52cd5","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"c0158152-7697-423c-96e4-2658773aabc5","type":"BasicTicker"},{"attributes":{"children":[{"id":"bf5f7417-1da3-4746-a864-5cc363a73a07","type":"WidgetBox"},{"id":"989769be-b7d2-496f-8a45-2a6685e52cd5","subtype":"Figure","type":"Plot"}]},"id":"e7553a2a-fbb1-4e15-9527-fe613db1ed96","type":"Column"},{"attributes":{"formatter":{"id":"3505a083-de2d-4c11-b4b8-8ca907865e10","type":"BasicTickFormatter"},"plot":{"id":"989769be-b7d2-496f-8a45-2a6685e52cd5","subtype":"Figure","type":"Plot"},"ticker":{"id":"c0158152-7697-423c-96e4-2658773aabc5","type":"BasicTicker"},"visible":false},"id":"799c0463-331c-4a29-897f-4cb7aaf9f98b","type":"LinearAxis"},{"attributes":{"formatter":{"id":"f38e22a9-880d-4a93-991a-44b8d5ba8c16","type":"BasicTickFormatter"},"plot":{"id":"989769be-b7d2-496f-8a45-2a6685e52cd5","subtype":"Figure","type":"Plot"},"ticker":{"id":"5226e69b-d988-43e3-b610-585c2c2ca871","type":"BasicTicker"},"visible":false},"id":"b97b2546-eb43-40c2-aa85-958193cfd0ad","type":"LinearAxis"},{"attributes":{"plot":{"id":"989769be-b7d2-496f-8a45-2a6685e52cd5","subtype":"Figure","type":"Plot"},"ticker":{"id":"c0158152-7697-423c-96e4-2658773aabc5","type":"BasicTicker"},"visible":false},"id":"e0b44e68-e821-4eb9-b147-987987b938c0","type":"Grid"},{"attributes":{},"id":"13c0c892-ba72-4d28-8f26-e940dd1a3dc7","type":"LinearScale"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"3933e2ba-3a09-4ee7-b351-4b5bcd6800ba","type":"ImageURL"}],"root_ids":["e7553a2a-fbb1-4e15-9527-fe613db1ed96"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"78d1d92c-0504-42f5-bb89-254e793bcfc8","roots":{"e7553a2a-fbb1-4e15-9527-fe613db1ed96":"c0a435c6-1ae9-4a7c-b4af-f7c5f762b1a6"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();