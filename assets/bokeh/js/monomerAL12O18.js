(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("aeb9f17c-c7f1-4615-9653-dd48b4cf7192");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'aeb9f17c-c7f1-4615-9653-dd48b4cf7192' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"1a04d3e0-cd9c-4855-80bd-a13f1ab0187f":{"roots":{"references":[{"attributes":{"callback":{"id":"aed1a1f3-0303-4b27-9ac1-b771ad1bd4d8","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"5ff54174-dbe5-4d3d-86af-24128654ec17","type":"Slider"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"a05aedb3-0c28-41f6-8355-ec71e1ed00b7","type":"Toolbar"},{"attributes":{},"id":"8c7e9155-d932-4169-bc25-5c81654185ff","type":"BasicTicker"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerAL12O18_000.png"],"url_num":["000"]},"selected":{"id":"0d78776e-9459-42db-bbda-f9d7808875ee","type":"Selection"},"selection_policy":{"id":"76fe2624-ce34-4b99-a92a-7ed79d6fffab","type":"UnionRenderers"}},"id":"b19c12f5-2959-4df8-98a6-48c5e3e3d09d","type":"ColumnDataSource"},{"attributes":{"below":[{"id":"3b0972c7-b3c6-4822-a2d8-e1b25deb29f5","type":"LinearAxis"}],"left":[{"id":"4622ba4e-4507-48ee-8f48-e9910d4b30b1","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"3b0972c7-b3c6-4822-a2d8-e1b25deb29f5","type":"LinearAxis"},{"id":"8ec7f9c2-c4e1-455b-8efd-61c2f0ceba84","type":"Grid"},{"id":"4622ba4e-4507-48ee-8f48-e9910d4b30b1","type":"LinearAxis"},{"id":"162caf79-b949-408e-8566-19755c6b1f0f","type":"Grid"},{"id":"3a4e49a5-265e-4aad-b307-cd97c6935b10","type":"GlyphRenderer"}],"title":{"id":"7c941d5e-b2bc-4fc5-afb7-d86af192bfb9","type":"Title"},"toolbar":{"id":"a05aedb3-0c28-41f6-8355-ec71e1ed00b7","type":"Toolbar"},"x_range":{"id":"60b7320e-01b5-4d77-9211-4ddf51a544e3","type":"Range1d"},"x_scale":{"id":"0910487f-0ac8-42b6-8233-8f23cab843c2","type":"LinearScale"},"y_range":{"id":"b5afb5e3-e561-4ac3-94e7-a07fcc20278f","type":"Range1d"},"y_scale":{"id":"110a99b4-cf0f-4766-aecd-b49f04f14be0","type":"LinearScale"}},"id":"9a04302a-c147-440b-9063-882ad38b5fdb","subtype":"Figure","type":"Plot"},{"attributes":{"callback":null},"id":"60b7320e-01b5-4d77-9211-4ddf51a544e3","type":"Range1d"},{"attributes":{},"id":"76fe2624-ce34-4b99-a92a-7ed79d6fffab","type":"UnionRenderers"},{"attributes":{},"id":"110a99b4-cf0f-4766-aecd-b49f04f14be0","type":"LinearScale"},{"attributes":{},"id":"d51ce9e7-c04b-4e17-b06c-982da7075317","type":"BasicTicker"},{"attributes":{},"id":"3f02be48-e566-43f3-ae6a-c112cb6c7c00","type":"BasicTickFormatter"},{"attributes":{},"id":"0910487f-0ac8-42b6-8233-8f23cab843c2","type":"LinearScale"},{"attributes":{"source":{"id":"b19c12f5-2959-4df8-98a6-48c5e3e3d09d","type":"ColumnDataSource"}},"id":"ce88a6bc-0dbf-48fc-9a49-505e3643e1ce","type":"CDSView"},{"attributes":{"formatter":{"id":"ba786abf-4cb3-4972-baa1-aa8ec49d5403","type":"BasicTickFormatter"},"plot":{"id":"9a04302a-c147-440b-9063-882ad38b5fdb","subtype":"Figure","type":"Plot"},"ticker":{"id":"8c7e9155-d932-4169-bc25-5c81654185ff","type":"BasicTicker"},"visible":false},"id":"4622ba4e-4507-48ee-8f48-e9910d4b30b1","type":"LinearAxis"},{"attributes":{"callback":null},"id":"b5afb5e3-e561-4ac3-94e7-a07fcc20278f","type":"Range1d"},{"attributes":{"plot":{"id":"9a04302a-c147-440b-9063-882ad38b5fdb","subtype":"Figure","type":"Plot"},"ticker":{"id":"d51ce9e7-c04b-4e17-b06c-982da7075317","type":"BasicTicker"},"visible":false},"id":"8ec7f9c2-c4e1-455b-8efd-61c2f0ceba84","type":"Grid"},{"attributes":{},"id":"0d78776e-9459-42db-bbda-f9d7808875ee","type":"Selection"},{"attributes":{"children":[{"id":"5ff54174-dbe5-4d3d-86af-24128654ec17","type":"Slider"}]},"id":"69a6ff53-e6df-45d2-a9ba-db9aa3956674","type":"WidgetBox"},{"attributes":{},"id":"ba786abf-4cb3-4972-baa1-aa8ec49d5403","type":"BasicTickFormatter"},{"attributes":{"data_source":{"id":"b19c12f5-2959-4df8-98a6-48c5e3e3d09d","type":"ColumnDataSource"},"glyph":{"id":"22943fa2-c488-4b14-89a2-76ef72e9244c","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"6a8e89b6-12d9-4eb6-806b-c48072d3c48e","type":"ImageURL"},"selection_glyph":null,"view":{"id":"ce88a6bc-0dbf-48fc-9a49-505e3643e1ce","type":"CDSView"}},"id":"3a4e49a5-265e-4aad-b307-cd97c6935b10","type":"GlyphRenderer"},{"attributes":{"dimension":1,"plot":{"id":"9a04302a-c147-440b-9063-882ad38b5fdb","subtype":"Figure","type":"Plot"},"ticker":{"id":"8c7e9155-d932-4169-bc25-5c81654185ff","type":"BasicTicker"},"visible":false},"id":"162caf79-b949-408e-8566-19755c6b1f0f","type":"Grid"},{"attributes":{"args":{"source":{"id":"b19c12f5-2959-4df8-98a6-48c5e3e3d09d","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"aed1a1f3-0303-4b27-9ac1-b771ad1bd4d8","type":"CustomJS"},{"attributes":{"plot":null,"text":""},"id":"7c941d5e-b2bc-4fc5-afb7-d86af192bfb9","type":"Title"},{"attributes":{"children":[{"id":"69a6ff53-e6df-45d2-a9ba-db9aa3956674","type":"WidgetBox"},{"id":"9a04302a-c147-440b-9063-882ad38b5fdb","subtype":"Figure","type":"Plot"}]},"id":"a4f4e0f2-2da2-434f-9a86-8e0af03326cf","type":"Column"},{"attributes":{"formatter":{"id":"3f02be48-e566-43f3-ae6a-c112cb6c7c00","type":"BasicTickFormatter"},"plot":{"id":"9a04302a-c147-440b-9063-882ad38b5fdb","subtype":"Figure","type":"Plot"},"ticker":{"id":"d51ce9e7-c04b-4e17-b06c-982da7075317","type":"BasicTicker"},"visible":false},"id":"3b0972c7-b3c6-4822-a2d8-e1b25deb29f5","type":"LinearAxis"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"22943fa2-c488-4b14-89a2-76ef72e9244c","type":"ImageURL"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"6a8e89b6-12d9-4eb6-806b-c48072d3c48e","type":"ImageURL"}],"root_ids":["a4f4e0f2-2da2-434f-9a86-8e0af03326cf"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"1a04d3e0-cd9c-4855-80bd-a13f1ab0187f","roots":{"a4f4e0f2-2da2-434f-9a86-8e0af03326cf":"aeb9f17c-c7f1-4615-9653-dd48b4cf7192"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();