(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("5786318a-8010-493d-9b37-f1af5e08c398");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '5786318a-8010-493d-9b37-f1af5e08c398' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"7be293af-215a-40c2-9632-c3dc7d000dc6":{"roots":{"references":[{"attributes":{"dimension":1,"plot":{"id":"9eeac3fb-0060-40c0-ae27-c9d1703e8f8b","subtype":"Figure","type":"Plot"},"ticker":{"id":"4d68b528-b1b6-4d64-a195-7ccf8b16da09","type":"BasicTicker"},"visible":false},"id":"182fb134-0f67-4c9c-9f4d-891548642ecb","type":"Grid"},{"attributes":{},"id":"4d68b528-b1b6-4d64-a195-7ccf8b16da09","type":"BasicTicker"},{"attributes":{},"id":"911eb517-5a1f-473c-a577-64cabeba6746","type":"BasicTickFormatter"},{"attributes":{"children":[{"id":"9e6ed1d6-15fe-4b13-b7f4-c2932d0455fe","type":"Slider"}]},"id":"c6db66ec-e59d-43fc-bc47-60c1baa25928","type":"WidgetBox"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/MG9O9_000.png"],"url_num":["000"]},"selected":{"id":"1a6184ad-b265-47f0-bfb4-e311767ecb5c","type":"Selection"},"selection_policy":{"id":"59fd1375-6e08-45e5-847a-05aff578e743","type":"UnionRenderers"}},"id":"13d1ad34-9b7f-4d2a-aec7-96ff8622b19d","type":"ColumnDataSource"},{"attributes":{},"id":"1a6184ad-b265-47f0-bfb4-e311767ecb5c","type":"Selection"},{"attributes":{"formatter":{"id":"0ff9792f-5072-42e3-b11a-632e2c240d3e","type":"BasicTickFormatter"},"plot":{"id":"9eeac3fb-0060-40c0-ae27-c9d1703e8f8b","subtype":"Figure","type":"Plot"},"ticker":{"id":"4d68b528-b1b6-4d64-a195-7ccf8b16da09","type":"BasicTicker"},"visible":false},"id":"5cd21672-30c4-4078-ac3a-28b675c28a3a","type":"LinearAxis"},{"attributes":{"args":{"source":{"id":"13d1ad34-9b7f-4d2a-aec7-96ff8622b19d","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"615158bb-4fdf-4787-bb22-e624a08033fb","type":"CustomJS"},{"attributes":{"source":{"id":"13d1ad34-9b7f-4d2a-aec7-96ff8622b19d","type":"ColumnDataSource"}},"id":"14feed1d-117a-49d1-a1a9-aef3368e23d3","type":"CDSView"},{"attributes":{},"id":"0ff9792f-5072-42e3-b11a-632e2c240d3e","type":"BasicTickFormatter"},{"attributes":{"data_source":{"id":"13d1ad34-9b7f-4d2a-aec7-96ff8622b19d","type":"ColumnDataSource"},"glyph":{"id":"2c7bf89a-52cc-4d54-95d8-c3403329c054","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"786ec7b7-b3cf-4533-9e65-a0feff9c48db","type":"ImageURL"},"selection_glyph":null,"view":{"id":"14feed1d-117a-49d1-a1a9-aef3368e23d3","type":"CDSView"}},"id":"f7b5c400-e710-421d-99f3-e56a419e2109","type":"GlyphRenderer"},{"attributes":{"callback":null},"id":"0fa42c9d-7a92-4ce7-a5fd-f14203cd749d","type":"Range1d"},{"attributes":{"children":[{"id":"c6db66ec-e59d-43fc-bc47-60c1baa25928","type":"WidgetBox"},{"id":"9eeac3fb-0060-40c0-ae27-c9d1703e8f8b","subtype":"Figure","type":"Plot"}]},"id":"e486426d-7b08-4530-91cc-9258e8550ffb","type":"Column"},{"attributes":{},"id":"08ac4dcb-cf32-40ca-9f50-1524752c6985","type":"LinearScale"},{"attributes":{},"id":"ccc2e3e8-9f0a-4179-97fc-4664d508f020","type":"LinearScale"},{"attributes":{"below":[{"id":"809b399e-4ddb-40a2-84d6-38902f20e005","type":"LinearAxis"}],"left":[{"id":"5cd21672-30c4-4078-ac3a-28b675c28a3a","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"809b399e-4ddb-40a2-84d6-38902f20e005","type":"LinearAxis"},{"id":"0eae8c8c-cf9e-4b4a-bb4a-df76693e2eb9","type":"Grid"},{"id":"5cd21672-30c4-4078-ac3a-28b675c28a3a","type":"LinearAxis"},{"id":"182fb134-0f67-4c9c-9f4d-891548642ecb","type":"Grid"},{"id":"f7b5c400-e710-421d-99f3-e56a419e2109","type":"GlyphRenderer"}],"title":{"id":"e194b2fc-ae9d-4a9d-8e00-bea31adabb3a","type":"Title"},"toolbar":{"id":"2fbefe4c-fb0c-4438-9fdf-6fe9647573c1","type":"Toolbar"},"x_range":{"id":"0fa42c9d-7a92-4ce7-a5fd-f14203cd749d","type":"Range1d"},"x_scale":{"id":"ccc2e3e8-9f0a-4179-97fc-4664d508f020","type":"LinearScale"},"y_range":{"id":"4e0a60d7-6249-49d8-8f14-4925cf0568fb","type":"Range1d"},"y_scale":{"id":"08ac4dcb-cf32-40ca-9f50-1524752c6985","type":"LinearScale"}},"id":"9eeac3fb-0060-40c0-ae27-c9d1703e8f8b","subtype":"Figure","type":"Plot"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"2c7bf89a-52cc-4d54-95d8-c3403329c054","type":"ImageURL"},{"attributes":{"callback":null},"id":"4e0a60d7-6249-49d8-8f14-4925cf0568fb","type":"Range1d"},{"attributes":{},"id":"59fd1375-6e08-45e5-847a-05aff578e743","type":"UnionRenderers"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"2fbefe4c-fb0c-4438-9fdf-6fe9647573c1","type":"Toolbar"},{"attributes":{"formatter":{"id":"911eb517-5a1f-473c-a577-64cabeba6746","type":"BasicTickFormatter"},"plot":{"id":"9eeac3fb-0060-40c0-ae27-c9d1703e8f8b","subtype":"Figure","type":"Plot"},"ticker":{"id":"72139c9f-6364-4462-83fc-43a3cb44bc52","type":"BasicTicker"},"visible":false},"id":"809b399e-4ddb-40a2-84d6-38902f20e005","type":"LinearAxis"},{"attributes":{"plot":null,"text":""},"id":"e194b2fc-ae9d-4a9d-8e00-bea31adabb3a","type":"Title"},{"attributes":{"callback":{"id":"615158bb-4fdf-4787-bb22-e624a08033fb","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"9e6ed1d6-15fe-4b13-b7f4-c2932d0455fe","type":"Slider"},{"attributes":{},"id":"72139c9f-6364-4462-83fc-43a3cb44bc52","type":"BasicTicker"},{"attributes":{"plot":{"id":"9eeac3fb-0060-40c0-ae27-c9d1703e8f8b","subtype":"Figure","type":"Plot"},"ticker":{"id":"72139c9f-6364-4462-83fc-43a3cb44bc52","type":"BasicTicker"},"visible":false},"id":"0eae8c8c-cf9e-4b4a-bb4a-df76693e2eb9","type":"Grid"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"786ec7b7-b3cf-4533-9e65-a0feff9c48db","type":"ImageURL"}],"root_ids":["e486426d-7b08-4530-91cc-9258e8550ffb"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"7be293af-215a-40c2-9632-c3dc7d000dc6","roots":{"e486426d-7b08-4530-91cc-9258e8550ffb":"5786318a-8010-493d-9b37-f1af5e08c398"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();