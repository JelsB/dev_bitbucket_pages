(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("590106f6-becc-4f7b-855a-7a51a27fd2f9");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '590106f6-becc-4f7b-855a-7a51a27fd2f9' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"234759e0-6989-45d0-bb19-fc7d4b20670c":{"roots":{"references":[{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"c993f850-180c-4ffa-a729-48c7ccac37e4","type":"Toolbar"},{"attributes":{"args":{"source":{"id":"77cd156b-6924-4772-90a5-347768aa2506","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"357743ee-f189-449d-87ac-11dd01465b09","type":"CustomJS"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerSI8O8_000.png"],"url_num":["000"]},"selected":{"id":"6b60f408-71fd-4bb8-8166-deb378080e3e","type":"Selection"},"selection_policy":{"id":"15e5d367-7472-4c73-9dba-a58bf21a9bb9","type":"UnionRenderers"}},"id":"77cd156b-6924-4772-90a5-347768aa2506","type":"ColumnDataSource"},{"attributes":{},"id":"3ad51c10-f2f1-414d-a0f9-c8e09b2a32e5","type":"BasicTicker"},{"attributes":{"formatter":{"id":"b812bef3-84a4-46a5-a095-d98247961270","type":"BasicTickFormatter"},"plot":{"id":"19a99d4c-7f86-45f1-b1f7-d1ef3b5b74fb","subtype":"Figure","type":"Plot"},"ticker":{"id":"e6130de3-c81d-449b-9008-8e2bbd92328d","type":"BasicTicker"},"visible":false},"id":"a7830d0f-3a2f-47cc-9c04-461a1a7f4ece","type":"LinearAxis"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"946a0bf3-1b41-413a-844d-29a4cd145879","type":"ImageURL"},{"attributes":{},"id":"284592e9-10d0-41dc-a623-ff4b74f02294","type":"BasicTickFormatter"},{"attributes":{"dimension":1,"plot":{"id":"19a99d4c-7f86-45f1-b1f7-d1ef3b5b74fb","subtype":"Figure","type":"Plot"},"ticker":{"id":"3ad51c10-f2f1-414d-a0f9-c8e09b2a32e5","type":"BasicTicker"},"visible":false},"id":"65e843c2-80d7-4477-a547-0aff7cb4a6c9","type":"Grid"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"8ffd44b3-257a-4ef4-ae24-0131eecd4a99","type":"ImageURL"},{"attributes":{"data_source":{"id":"77cd156b-6924-4772-90a5-347768aa2506","type":"ColumnDataSource"},"glyph":{"id":"946a0bf3-1b41-413a-844d-29a4cd145879","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"8ffd44b3-257a-4ef4-ae24-0131eecd4a99","type":"ImageURL"},"selection_glyph":null,"view":{"id":"1da4dfb4-f017-4089-93e6-61cee07821ec","type":"CDSView"}},"id":"3e8f3a8a-af92-4e26-b8df-e969ec27df27","type":"GlyphRenderer"},{"attributes":{},"id":"6b60f408-71fd-4bb8-8166-deb378080e3e","type":"Selection"},{"attributes":{},"id":"afc3cb43-8ff2-4f3f-918f-7e98510aa4b8","type":"LinearScale"},{"attributes":{"callback":null},"id":"686d8f2e-b4ca-40cb-9643-0dd5d0ef34ca","type":"Range1d"},{"attributes":{"below":[{"id":"a7830d0f-3a2f-47cc-9c04-461a1a7f4ece","type":"LinearAxis"}],"left":[{"id":"efbed85a-f0d0-4094-b378-81692d44cee7","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"a7830d0f-3a2f-47cc-9c04-461a1a7f4ece","type":"LinearAxis"},{"id":"a5bd6612-9174-48aa-83df-fbf3d05a4f81","type":"Grid"},{"id":"efbed85a-f0d0-4094-b378-81692d44cee7","type":"LinearAxis"},{"id":"65e843c2-80d7-4477-a547-0aff7cb4a6c9","type":"Grid"},{"id":"3e8f3a8a-af92-4e26-b8df-e969ec27df27","type":"GlyphRenderer"}],"title":{"id":"281cfdcf-3f0c-4beb-9cfd-57455203c66c","type":"Title"},"toolbar":{"id":"c993f850-180c-4ffa-a729-48c7ccac37e4","type":"Toolbar"},"x_range":{"id":"686d8f2e-b4ca-40cb-9643-0dd5d0ef34ca","type":"Range1d"},"x_scale":{"id":"dbb97c44-9b88-4c85-8fd1-89e7f2b19aee","type":"LinearScale"},"y_range":{"id":"1870f74e-06b1-4cd0-ab3a-3d888526aadf","type":"Range1d"},"y_scale":{"id":"afc3cb43-8ff2-4f3f-918f-7e98510aa4b8","type":"LinearScale"}},"id":"19a99d4c-7f86-45f1-b1f7-d1ef3b5b74fb","subtype":"Figure","type":"Plot"},{"attributes":{"children":[{"id":"a83d6246-6d97-4a22-bb81-31f5e6c0c413","type":"WidgetBox"},{"id":"19a99d4c-7f86-45f1-b1f7-d1ef3b5b74fb","subtype":"Figure","type":"Plot"}]},"id":"8b28fdee-240d-40d4-b249-39c97e803b84","type":"Column"},{"attributes":{"formatter":{"id":"284592e9-10d0-41dc-a623-ff4b74f02294","type":"BasicTickFormatter"},"plot":{"id":"19a99d4c-7f86-45f1-b1f7-d1ef3b5b74fb","subtype":"Figure","type":"Plot"},"ticker":{"id":"3ad51c10-f2f1-414d-a0f9-c8e09b2a32e5","type":"BasicTicker"},"visible":false},"id":"efbed85a-f0d0-4094-b378-81692d44cee7","type":"LinearAxis"},{"attributes":{},"id":"e6130de3-c81d-449b-9008-8e2bbd92328d","type":"BasicTicker"},{"attributes":{"plot":null,"text":""},"id":"281cfdcf-3f0c-4beb-9cfd-57455203c66c","type":"Title"},{"attributes":{"plot":{"id":"19a99d4c-7f86-45f1-b1f7-d1ef3b5b74fb","subtype":"Figure","type":"Plot"},"ticker":{"id":"e6130de3-c81d-449b-9008-8e2bbd92328d","type":"BasicTicker"},"visible":false},"id":"a5bd6612-9174-48aa-83df-fbf3d05a4f81","type":"Grid"},{"attributes":{},"id":"dbb97c44-9b88-4c85-8fd1-89e7f2b19aee","type":"LinearScale"},{"attributes":{},"id":"b812bef3-84a4-46a5-a095-d98247961270","type":"BasicTickFormatter"},{"attributes":{"children":[{"id":"5f57481e-4433-4440-86f4-844248c69ee8","type":"Slider"}]},"id":"a83d6246-6d97-4a22-bb81-31f5e6c0c413","type":"WidgetBox"},{"attributes":{"source":{"id":"77cd156b-6924-4772-90a5-347768aa2506","type":"ColumnDataSource"}},"id":"1da4dfb4-f017-4089-93e6-61cee07821ec","type":"CDSView"},{"attributes":{"callback":null},"id":"1870f74e-06b1-4cd0-ab3a-3d888526aadf","type":"Range1d"},{"attributes":{"callback":{"id":"357743ee-f189-449d-87ac-11dd01465b09","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"5f57481e-4433-4440-86f4-844248c69ee8","type":"Slider"},{"attributes":{},"id":"15e5d367-7472-4c73-9dba-a58bf21a9bb9","type":"UnionRenderers"}],"root_ids":["8b28fdee-240d-40d4-b249-39c97e803b84"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"234759e0-6989-45d0-bb19-fc7d4b20670c","roots":{"8b28fdee-240d-40d4-b249-39c97e803b84":"590106f6-becc-4f7b-855a-7a51a27fd2f9"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();