(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("85f007b0-3fa3-4fc0-bf03-060869887eda");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '85f007b0-3fa3-4fc0-bf03-060869887eda' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"71d6aa00-584b-45e6-aa70-1be1d1e0d623":{"roots":{"references":[{"attributes":{},"id":"b41b064a-c512-4494-8760-15edd86b1f94","type":"LinearScale"},{"attributes":{"below":[{"id":"cc566a29-d132-4ecc-99f8-34b463626b37","type":"LinearAxis"}],"left":[{"id":"066acf7c-be12-4548-8936-4eec86396929","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"cc566a29-d132-4ecc-99f8-34b463626b37","type":"LinearAxis"},{"id":"a786750f-2bd5-4b52-8dd7-a341af069c03","type":"Grid"},{"id":"066acf7c-be12-4548-8936-4eec86396929","type":"LinearAxis"},{"id":"35bbca14-c1af-4867-874c-72824a37a020","type":"Grid"},{"id":"3d6a7aee-caa2-4b05-8476-31b9378a3662","type":"GlyphRenderer"}],"title":{"id":"1ba34e4b-573b-4f54-bfa5-7fb9f060ae32","type":"Title"},"toolbar":{"id":"9827e48c-4935-4b0d-a241-b9eab0ecc1cb","type":"Toolbar"},"x_range":{"id":"9cf81248-e606-4012-9b64-1b2dc58f3630","type":"Range1d"},"x_scale":{"id":"b41b064a-c512-4494-8760-15edd86b1f94","type":"LinearScale"},"y_range":{"id":"5d2fa6a8-3000-4e76-bc37-910863f3cd8a","type":"Range1d"},"y_scale":{"id":"574025ce-2a3a-46f0-8f21-31e7c9fd8174","type":"LinearScale"}},"id":"0093d645-450f-49e8-9e13-06edd1353065","subtype":"Figure","type":"Plot"},{"attributes":{"children":[{"id":"b75c3c00-1021-4e1a-9902-248caa498fe4","type":"WidgetBox"},{"id":"0093d645-450f-49e8-9e13-06edd1353065","subtype":"Figure","type":"Plot"}]},"id":"afa6fa83-82c9-4312-a703-bb3bf15d7689","type":"Column"},{"attributes":{"plot":{"id":"0093d645-450f-49e8-9e13-06edd1353065","subtype":"Figure","type":"Plot"},"ticker":{"id":"6fe7b71e-0e87-448b-a892-462cc5ff1d2e","type":"BasicTicker"},"visible":false},"id":"a786750f-2bd5-4b52-8dd7-a341af069c03","type":"Grid"},{"attributes":{"plot":null,"text":""},"id":"1ba34e4b-573b-4f54-bfa5-7fb9f060ae32","type":"Title"},{"attributes":{},"id":"574025ce-2a3a-46f0-8f21-31e7c9fd8174","type":"LinearScale"},{"attributes":{"dimension":1,"plot":{"id":"0093d645-450f-49e8-9e13-06edd1353065","subtype":"Figure","type":"Plot"},"ticker":{"id":"28b934d9-477b-49e2-89fa-774839472760","type":"BasicTicker"},"visible":false},"id":"35bbca14-c1af-4867-874c-72824a37a020","type":"Grid"},{"attributes":{},"id":"38d90904-a6cc-410d-8e8d-565d6423fbba","type":"BasicTickFormatter"},{"attributes":{"children":[{"id":"5df5e531-d638-40a3-a174-5b1ef3fe5206","type":"Slider"}]},"id":"b75c3c00-1021-4e1a-9902-248caa498fe4","type":"WidgetBox"},{"attributes":{"formatter":{"id":"667eccec-903e-4035-877e-8ba06bbee1d1","type":"BasicTickFormatter"},"plot":{"id":"0093d645-450f-49e8-9e13-06edd1353065","subtype":"Figure","type":"Plot"},"ticker":{"id":"28b934d9-477b-49e2-89fa-774839472760","type":"BasicTicker"},"visible":false},"id":"066acf7c-be12-4548-8936-4eec86396929","type":"LinearAxis"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"074e28d2-71f5-40d7-9063-d0fe48915519","type":"ImageURL"},{"attributes":{"args":{"source":{"id":"fabd7f73-48f9-4396-bb1c-bea3e487fb30","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"05ba0bb9-a7a7-45a1-837f-27a1cf7332dd","type":"CustomJS"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerMG8O8_000.png"],"url_num":["000"]},"selected":{"id":"cd8f1196-bb97-40d4-b268-4419bc615c4c","type":"Selection"},"selection_policy":{"id":"65650824-c178-4b24-ba7c-f58678ac81af","type":"UnionRenderers"}},"id":"fabd7f73-48f9-4396-bb1c-bea3e487fb30","type":"ColumnDataSource"},{"attributes":{"formatter":{"id":"38d90904-a6cc-410d-8e8d-565d6423fbba","type":"BasicTickFormatter"},"plot":{"id":"0093d645-450f-49e8-9e13-06edd1353065","subtype":"Figure","type":"Plot"},"ticker":{"id":"6fe7b71e-0e87-448b-a892-462cc5ff1d2e","type":"BasicTicker"},"visible":false},"id":"cc566a29-d132-4ecc-99f8-34b463626b37","type":"LinearAxis"},{"attributes":{},"id":"65650824-c178-4b24-ba7c-f58678ac81af","type":"UnionRenderers"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"9827e48c-4935-4b0d-a241-b9eab0ecc1cb","type":"Toolbar"},{"attributes":{},"id":"667eccec-903e-4035-877e-8ba06bbee1d1","type":"BasicTickFormatter"},{"attributes":{},"id":"28b934d9-477b-49e2-89fa-774839472760","type":"BasicTicker"},{"attributes":{},"id":"cd8f1196-bb97-40d4-b268-4419bc615c4c","type":"Selection"},{"attributes":{"callback":{"id":"05ba0bb9-a7a7-45a1-837f-27a1cf7332dd","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"5df5e531-d638-40a3-a174-5b1ef3fe5206","type":"Slider"},{"attributes":{"data_source":{"id":"fabd7f73-48f9-4396-bb1c-bea3e487fb30","type":"ColumnDataSource"},"glyph":{"id":"824bb1ba-9a46-4a4a-a7e1-0b741bbcbbb0","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"074e28d2-71f5-40d7-9063-d0fe48915519","type":"ImageURL"},"selection_glyph":null,"view":{"id":"342ded39-dc25-4daa-9752-a7a57474ef4c","type":"CDSView"}},"id":"3d6a7aee-caa2-4b05-8476-31b9378a3662","type":"GlyphRenderer"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"824bb1ba-9a46-4a4a-a7e1-0b741bbcbbb0","type":"ImageURL"},{"attributes":{"callback":null},"id":"5d2fa6a8-3000-4e76-bc37-910863f3cd8a","type":"Range1d"},{"attributes":{"source":{"id":"fabd7f73-48f9-4396-bb1c-bea3e487fb30","type":"ColumnDataSource"}},"id":"342ded39-dc25-4daa-9752-a7a57474ef4c","type":"CDSView"},{"attributes":{"callback":null},"id":"9cf81248-e606-4012-9b64-1b2dc58f3630","type":"Range1d"},{"attributes":{},"id":"6fe7b71e-0e87-448b-a892-462cc5ff1d2e","type":"BasicTicker"}],"root_ids":["afa6fa83-82c9-4312-a703-bb3bf15d7689"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"71d6aa00-584b-45e6-aa70-1be1d1e0d623","roots":{"afa6fa83-82c9-4312-a703-bb3bf15d7689":"85f007b0-3fa3-4fc0-bf03-060869887eda"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();