(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("e41e9a7b-60e3-4fc1-bcb1-de653d91c05a");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'e41e9a7b-60e3-4fc1-bcb1-de653d91c05a' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"a0c176ae-313c-48a7-95c3-2bbad2b88882":{"roots":{"references":[{"attributes":{},"id":"13666043-2e51-429f-865e-73a1a858ff8d","type":"LinearScale"},{"attributes":{"data_source":{"id":"51e8896e-db4b-4c57-9892-968c3e7b2179","type":"ColumnDataSource"},"glyph":{"id":"a881d311-4bdc-4e69-9f24-80a5f1686b80","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"6d221fb3-3433-44cd-a2d6-53d52e30b7a1","type":"ImageURL"},"selection_glyph":null,"view":{"id":"ed4dfedd-31ca-491f-b7c5-39939a505afa","type":"CDSView"}},"id":"d34c794d-2967-4127-9172-73f72349948f","type":"GlyphRenderer"},{"attributes":{"dimension":1,"plot":{"id":"4e0ebf7a-3e94-45a0-9003-395664fee9bc","subtype":"Figure","type":"Plot"},"ticker":{"id":"299275e6-8b6c-4bf5-a4b7-6df01bb1068c","type":"BasicTicker"},"visible":false},"id":"5f9fcaeb-d154-4014-b0fc-ade065a7ef67","type":"Grid"},{"attributes":{},"id":"0b40e985-996d-4912-8148-757353697224","type":"LinearScale"},{"attributes":{"children":[{"id":"60f8de63-b9b7-4130-8519-827b7ca3ddd5","type":"Slider"}]},"id":"9117750c-6e29-4e22-8217-0e57fefa47ac","type":"WidgetBox"},{"attributes":{"callback":{"id":"86bf6cc2-1159-41c0-9b7f-06514174c30f","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"60f8de63-b9b7-4130-8519-827b7ca3ddd5","type":"Slider"},{"attributes":{},"id":"f268e7c4-0bf0-4165-b314-8ed7c31ea12f","type":"BasicTickFormatter"},{"attributes":{},"id":"299275e6-8b6c-4bf5-a4b7-6df01bb1068c","type":"BasicTicker"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"a881d311-4bdc-4e69-9f24-80a5f1686b80","type":"ImageURL"},{"attributes":{},"id":"45a957b9-4da9-462a-807d-4bb563665d7f","type":"BasicTicker"},{"attributes":{},"id":"dd65931b-f016-49f5-88f3-794638e34791","type":"Selection"},{"attributes":{"callback":null},"id":"5b38de70-964d-425f-aa05-b554ad3f0ef7","type":"Range1d"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerSI3O3_000.png"],"url_num":["000"]},"selected":{"id":"dd65931b-f016-49f5-88f3-794638e34791","type":"Selection"},"selection_policy":{"id":"c55ab605-0d38-4994-88cd-7470f073b4c0","type":"UnionRenderers"}},"id":"51e8896e-db4b-4c57-9892-968c3e7b2179","type":"ColumnDataSource"},{"attributes":{},"id":"dec0f5cc-bc7e-4288-90a7-8c4bb857fd55","type":"BasicTickFormatter"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"6d221fb3-3433-44cd-a2d6-53d52e30b7a1","type":"ImageURL"},{"attributes":{"plot":{"id":"4e0ebf7a-3e94-45a0-9003-395664fee9bc","subtype":"Figure","type":"Plot"},"ticker":{"id":"45a957b9-4da9-462a-807d-4bb563665d7f","type":"BasicTicker"},"visible":false},"id":"292ddae3-9d4b-419f-80f0-259ee8ec91c2","type":"Grid"},{"attributes":{"args":{"source":{"id":"51e8896e-db4b-4c57-9892-968c3e7b2179","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"86bf6cc2-1159-41c0-9b7f-06514174c30f","type":"CustomJS"},{"attributes":{},"id":"c55ab605-0d38-4994-88cd-7470f073b4c0","type":"UnionRenderers"},{"attributes":{"formatter":{"id":"dec0f5cc-bc7e-4288-90a7-8c4bb857fd55","type":"BasicTickFormatter"},"plot":{"id":"4e0ebf7a-3e94-45a0-9003-395664fee9bc","subtype":"Figure","type":"Plot"},"ticker":{"id":"299275e6-8b6c-4bf5-a4b7-6df01bb1068c","type":"BasicTicker"},"visible":false},"id":"02859ddb-b019-4871-b470-804c7aeca2d8","type":"LinearAxis"},{"attributes":{"plot":null,"text":""},"id":"ac7e53df-49b6-4357-ade9-44cb49a934ca","type":"Title"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"7f05392c-3fa0-4145-82bf-2ff7729c0ab7","type":"Toolbar"},{"attributes":{"below":[{"id":"71e9d892-57db-4220-a6a2-1910e9662631","type":"LinearAxis"}],"left":[{"id":"02859ddb-b019-4871-b470-804c7aeca2d8","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"71e9d892-57db-4220-a6a2-1910e9662631","type":"LinearAxis"},{"id":"292ddae3-9d4b-419f-80f0-259ee8ec91c2","type":"Grid"},{"id":"02859ddb-b019-4871-b470-804c7aeca2d8","type":"LinearAxis"},{"id":"5f9fcaeb-d154-4014-b0fc-ade065a7ef67","type":"Grid"},{"id":"d34c794d-2967-4127-9172-73f72349948f","type":"GlyphRenderer"}],"title":{"id":"ac7e53df-49b6-4357-ade9-44cb49a934ca","type":"Title"},"toolbar":{"id":"7f05392c-3fa0-4145-82bf-2ff7729c0ab7","type":"Toolbar"},"x_range":{"id":"2f8d0d7d-1c9f-4482-9fb0-80cc27716331","type":"Range1d"},"x_scale":{"id":"0b40e985-996d-4912-8148-757353697224","type":"LinearScale"},"y_range":{"id":"5b38de70-964d-425f-aa05-b554ad3f0ef7","type":"Range1d"},"y_scale":{"id":"13666043-2e51-429f-865e-73a1a858ff8d","type":"LinearScale"}},"id":"4e0ebf7a-3e94-45a0-9003-395664fee9bc","subtype":"Figure","type":"Plot"},{"attributes":{"children":[{"id":"9117750c-6e29-4e22-8217-0e57fefa47ac","type":"WidgetBox"},{"id":"4e0ebf7a-3e94-45a0-9003-395664fee9bc","subtype":"Figure","type":"Plot"}]},"id":"dbd77d19-3eaf-4ff0-b5b1-bd8d200dcadd","type":"Column"},{"attributes":{"callback":null},"id":"2f8d0d7d-1c9f-4482-9fb0-80cc27716331","type":"Range1d"},{"attributes":{"source":{"id":"51e8896e-db4b-4c57-9892-968c3e7b2179","type":"ColumnDataSource"}},"id":"ed4dfedd-31ca-491f-b7c5-39939a505afa","type":"CDSView"},{"attributes":{"formatter":{"id":"f268e7c4-0bf0-4165-b314-8ed7c31ea12f","type":"BasicTickFormatter"},"plot":{"id":"4e0ebf7a-3e94-45a0-9003-395664fee9bc","subtype":"Figure","type":"Plot"},"ticker":{"id":"45a957b9-4da9-462a-807d-4bb563665d7f","type":"BasicTicker"},"visible":false},"id":"71e9d892-57db-4220-a6a2-1910e9662631","type":"LinearAxis"}],"root_ids":["dbd77d19-3eaf-4ff0-b5b1-bd8d200dcadd"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"a0c176ae-313c-48a7-95c3-2bbad2b88882","roots":{"dbd77d19-3eaf-4ff0-b5b1-bd8d200dcadd":"e41e9a7b-60e3-4fc1-bcb1-de653d91c05a"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();