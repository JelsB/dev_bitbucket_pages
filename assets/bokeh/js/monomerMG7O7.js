(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("629bc667-2fec-4f34-b6b2-7b4d0c21f73c");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '629bc667-2fec-4f34-b6b2-7b4d0c21f73c' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"2def77ca-184f-467a-af00-3657ab25bd1d":{"roots":{"references":[{"attributes":{"callback":null},"id":"5a190843-8c61-41b6-a223-70788b921373","type":"Range1d"},{"attributes":{"source":{"id":"f6c4df28-083c-486e-948a-5a43fca9dc8d","type":"ColumnDataSource"}},"id":"97bb6b56-85b4-4e0b-afe8-a457e46b78f7","type":"CDSView"},{"attributes":{"dimension":1,"plot":{"id":"a946e9f7-86ff-435d-a549-0e3cd6705815","subtype":"Figure","type":"Plot"},"ticker":{"id":"1b227aae-1a6f-4a2f-89b4-dae164317016","type":"BasicTicker"},"visible":false},"id":"f34eaad9-67f2-4cf4-8897-dd2f46bbc44b","type":"Grid"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerMG7O7_000.png"],"url_num":["000"]},"selected":{"id":"72214104-99dd-4eeb-b82e-3758d17bc43e","type":"Selection"},"selection_policy":{"id":"8a5442a9-55ea-466d-b77d-74885c878186","type":"UnionRenderers"}},"id":"f6c4df28-083c-486e-948a-5a43fca9dc8d","type":"ColumnDataSource"},{"attributes":{"below":[{"id":"7fe2cb49-cdfb-4e44-8912-de1a9c43842a","type":"LinearAxis"}],"left":[{"id":"e1030354-6205-4f36-aec2-3cb21f1e6a9c","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"7fe2cb49-cdfb-4e44-8912-de1a9c43842a","type":"LinearAxis"},{"id":"855c127a-aba0-4fe8-a819-01fc4031f01a","type":"Grid"},{"id":"e1030354-6205-4f36-aec2-3cb21f1e6a9c","type":"LinearAxis"},{"id":"f34eaad9-67f2-4cf4-8897-dd2f46bbc44b","type":"Grid"},{"id":"67b4055d-a639-4066-aba7-554b28d63f90","type":"GlyphRenderer"}],"title":{"id":"db4e4540-cd20-4fca-a6e5-2c9870ea6a03","type":"Title"},"toolbar":{"id":"d9cae907-aa36-4eb7-ac60-803c70d6a727","type":"Toolbar"},"x_range":{"id":"5a190843-8c61-41b6-a223-70788b921373","type":"Range1d"},"x_scale":{"id":"53f5ae6e-727a-454d-804b-e51b7281073a","type":"LinearScale"},"y_range":{"id":"409294a6-3d01-402e-b116-77d669ea58b7","type":"Range1d"},"y_scale":{"id":"6d6b186e-9acd-4bb2-8aea-b43debd1ac47","type":"LinearScale"}},"id":"a946e9f7-86ff-435d-a549-0e3cd6705815","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"1b227aae-1a6f-4a2f-89b4-dae164317016","type":"BasicTicker"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"7b793f59-bfc3-4874-9b23-1873f05b0260","type":"ImageURL"},{"attributes":{},"id":"72214104-99dd-4eeb-b82e-3758d17bc43e","type":"Selection"},{"attributes":{"data_source":{"id":"f6c4df28-083c-486e-948a-5a43fca9dc8d","type":"ColumnDataSource"},"glyph":{"id":"7b793f59-bfc3-4874-9b23-1873f05b0260","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"4fd5f38d-5eb1-43e2-b9b6-a8f4d57abe18","type":"ImageURL"},"selection_glyph":null,"view":{"id":"97bb6b56-85b4-4e0b-afe8-a457e46b78f7","type":"CDSView"}},"id":"67b4055d-a639-4066-aba7-554b28d63f90","type":"GlyphRenderer"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"d9cae907-aa36-4eb7-ac60-803c70d6a727","type":"Toolbar"},{"attributes":{"args":{"source":{"id":"f6c4df28-083c-486e-948a-5a43fca9dc8d","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"32d6a7ab-8d4e-4046-a7b3-aeba84040029","type":"CustomJS"},{"attributes":{},"id":"6d6b186e-9acd-4bb2-8aea-b43debd1ac47","type":"LinearScale"},{"attributes":{},"id":"132fdcd5-d0b0-45a0-9587-47e512f6e952","type":"BasicTickFormatter"},{"attributes":{"children":[{"id":"b66af401-d24e-401b-8c95-c3ab379e4a67","type":"Slider"}]},"id":"cf1a9ed3-2ca2-4cde-96c4-20e3a581c858","type":"WidgetBox"},{"attributes":{"formatter":{"id":"132fdcd5-d0b0-45a0-9587-47e512f6e952","type":"BasicTickFormatter"},"plot":{"id":"a946e9f7-86ff-435d-a549-0e3cd6705815","subtype":"Figure","type":"Plot"},"ticker":{"id":"ea84b8ce-c148-47b7-aed6-6114ece1061e","type":"BasicTicker"},"visible":false},"id":"7fe2cb49-cdfb-4e44-8912-de1a9c43842a","type":"LinearAxis"},{"attributes":{"formatter":{"id":"8e29b3d0-0832-49dc-8952-209dccf90d37","type":"BasicTickFormatter"},"plot":{"id":"a946e9f7-86ff-435d-a549-0e3cd6705815","subtype":"Figure","type":"Plot"},"ticker":{"id":"1b227aae-1a6f-4a2f-89b4-dae164317016","type":"BasicTicker"},"visible":false},"id":"e1030354-6205-4f36-aec2-3cb21f1e6a9c","type":"LinearAxis"},{"attributes":{},"id":"53f5ae6e-727a-454d-804b-e51b7281073a","type":"LinearScale"},{"attributes":{},"id":"ea84b8ce-c148-47b7-aed6-6114ece1061e","type":"BasicTicker"},{"attributes":{"plot":null,"text":""},"id":"db4e4540-cd20-4fca-a6e5-2c9870ea6a03","type":"Title"},{"attributes":{"plot":{"id":"a946e9f7-86ff-435d-a549-0e3cd6705815","subtype":"Figure","type":"Plot"},"ticker":{"id":"ea84b8ce-c148-47b7-aed6-6114ece1061e","type":"BasicTicker"},"visible":false},"id":"855c127a-aba0-4fe8-a819-01fc4031f01a","type":"Grid"},{"attributes":{},"id":"8e29b3d0-0832-49dc-8952-209dccf90d37","type":"BasicTickFormatter"},{"attributes":{"callback":{"id":"32d6a7ab-8d4e-4046-a7b3-aeba84040029","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"b66af401-d24e-401b-8c95-c3ab379e4a67","type":"Slider"},{"attributes":{},"id":"8a5442a9-55ea-466d-b77d-74885c878186","type":"UnionRenderers"},{"attributes":{"callback":null},"id":"409294a6-3d01-402e-b116-77d669ea58b7","type":"Range1d"},{"attributes":{"children":[{"id":"cf1a9ed3-2ca2-4cde-96c4-20e3a581c858","type":"WidgetBox"},{"id":"a946e9f7-86ff-435d-a549-0e3cd6705815","subtype":"Figure","type":"Plot"}]},"id":"2836281b-2c93-42c9-b4f6-336c15a7afb9","type":"Column"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"4fd5f38d-5eb1-43e2-b9b6-a8f4d57abe18","type":"ImageURL"}],"root_ids":["2836281b-2c93-42c9-b4f6-336c15a7afb9"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"2def77ca-184f-467a-af00-3657ab25bd1d","roots":{"2836281b-2c93-42c9-b4f6-336c15a7afb9":"629bc667-2fec-4f34-b6b2-7b4d0c21f73c"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();