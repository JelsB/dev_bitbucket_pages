(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("e2d6e188-d96e-417e-ac07-503d9a6dfeb5");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'e2d6e188-d96e-417e-ac07-503d9a6dfeb5' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"f270c445-a828-4d92-9a1a-bbc64e2cd341":{"roots":{"references":[{"attributes":{},"id":"0d082244-4089-4ae3-898f-4b2b49872254","type":"BasicTickFormatter"},{"attributes":{},"id":"d18bef2f-5a48-4053-ac26-2e97064f0e79","type":"BasicTicker"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"ca2f2f94-1070-465b-b6c7-009b4514b7c5","type":"ImageURL"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"3cf5608b-bb13-4fa1-a99f-5cd1f5fc4e5d","type":"Toolbar"},{"attributes":{},"id":"a400cd45-7731-431b-8ad2-afbc3bb2c873","type":"Selection"},{"attributes":{},"id":"24667f2a-c2f2-4543-8942-41bac9459441","type":"LinearScale"},{"attributes":{"callback":null},"id":"4b0fbca9-1401-43b8-b0d0-ee8287a3179c","type":"Range1d"},{"attributes":{"callback":{"id":"d9ad14a2-e5d7-4415-a4b6-f87cfb42dfa7","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"5d40e33d-d40b-49ca-bba9-8455beaec6a2","type":"Slider"},{"attributes":{},"id":"fa85b080-777f-42e4-a3b9-458890b7a0be","type":"BasicTickFormatter"},{"attributes":{},"id":"f9f73c2d-e561-4536-aeca-3442a33aee08","type":"LinearScale"},{"attributes":{"plot":{"id":"8b6c2486-1f40-4a82-a88d-4cd5e964b120","subtype":"Figure","type":"Plot"},"ticker":{"id":"d18bef2f-5a48-4053-ac26-2e97064f0e79","type":"BasicTicker"},"visible":false},"id":"eed76a6b-c0cf-4e62-abcf-6dd56940a810","type":"Grid"},{"attributes":{"formatter":{"id":"0d082244-4089-4ae3-898f-4b2b49872254","type":"BasicTickFormatter"},"plot":{"id":"8b6c2486-1f40-4a82-a88d-4cd5e964b120","subtype":"Figure","type":"Plot"},"ticker":{"id":"68e24110-8006-4366-977a-ad84a864d342","type":"BasicTicker"},"visible":false},"id":"f017daab-5389-473a-9f3b-18d3b823a4a3","type":"LinearAxis"},{"attributes":{},"id":"68e24110-8006-4366-977a-ad84a864d342","type":"BasicTicker"},{"attributes":{"children":[{"id":"5d40e33d-d40b-49ca-bba9-8455beaec6a2","type":"Slider"}]},"id":"e1b73d6f-2cd9-4c6e-af1a-cbefe19aebbc","type":"WidgetBox"},{"attributes":{"data_source":{"id":"ab7d3f14-59fd-49d4-90bb-ff7730edfb89","type":"ColumnDataSource"},"glyph":{"id":"ca2f2f94-1070-465b-b6c7-009b4514b7c5","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"c5b8fff7-0f4a-4d0b-b07d-8132436e4173","type":"ImageURL"},"selection_glyph":null,"view":{"id":"c260fa78-852f-4417-a4ed-6734ff858895","type":"CDSView"}},"id":"94d879a1-1f46-42c1-8518-9a391c28db09","type":"GlyphRenderer"},{"attributes":{"source":{"id":"ab7d3f14-59fd-49d4-90bb-ff7730edfb89","type":"ColumnDataSource"}},"id":"c260fa78-852f-4417-a4ed-6734ff858895","type":"CDSView"},{"attributes":{"formatter":{"id":"fa85b080-777f-42e4-a3b9-458890b7a0be","type":"BasicTickFormatter"},"plot":{"id":"8b6c2486-1f40-4a82-a88d-4cd5e964b120","subtype":"Figure","type":"Plot"},"ticker":{"id":"d18bef2f-5a48-4053-ac26-2e97064f0e79","type":"BasicTicker"},"visible":false},"id":"67498068-2520-4901-bcf3-f26838e77ae4","type":"LinearAxis"},{"attributes":{"callback":null},"id":"11ca0b84-7893-4c42-9829-f1f5986d715d","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"c5b8fff7-0f4a-4d0b-b07d-8132436e4173","type":"ImageURL"},{"attributes":{"below":[{"id":"67498068-2520-4901-bcf3-f26838e77ae4","type":"LinearAxis"}],"left":[{"id":"f017daab-5389-473a-9f3b-18d3b823a4a3","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"67498068-2520-4901-bcf3-f26838e77ae4","type":"LinearAxis"},{"id":"eed76a6b-c0cf-4e62-abcf-6dd56940a810","type":"Grid"},{"id":"f017daab-5389-473a-9f3b-18d3b823a4a3","type":"LinearAxis"},{"id":"f207690e-ccb2-4c08-bf22-37b4bb2e699d","type":"Grid"},{"id":"94d879a1-1f46-42c1-8518-9a391c28db09","type":"GlyphRenderer"}],"title":{"id":"19f16a5b-ba60-460c-94e4-6939a01d6405","type":"Title"},"toolbar":{"id":"3cf5608b-bb13-4fa1-a99f-5cd1f5fc4e5d","type":"Toolbar"},"x_range":{"id":"11ca0b84-7893-4c42-9829-f1f5986d715d","type":"Range1d"},"x_scale":{"id":"f9f73c2d-e561-4536-aeca-3442a33aee08","type":"LinearScale"},"y_range":{"id":"4b0fbca9-1401-43b8-b0d0-ee8287a3179c","type":"Range1d"},"y_scale":{"id":"24667f2a-c2f2-4543-8942-41bac9459441","type":"LinearScale"}},"id":"8b6c2486-1f40-4a82-a88d-4cd5e964b120","subtype":"Figure","type":"Plot"},{"attributes":{"args":{"source":{"id":"ab7d3f14-59fd-49d4-90bb-ff7730edfb89","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"d9ad14a2-e5d7-4415-a4b6-f87cfb42dfa7","type":"CustomJS"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerMG4O4_000.png"],"url_num":["000"]},"selected":{"id":"a400cd45-7731-431b-8ad2-afbc3bb2c873","type":"Selection"},"selection_policy":{"id":"4f1c3e24-4010-4b83-83a5-5cb0d77cfa9b","type":"UnionRenderers"}},"id":"ab7d3f14-59fd-49d4-90bb-ff7730edfb89","type":"ColumnDataSource"},{"attributes":{},"id":"4f1c3e24-4010-4b83-83a5-5cb0d77cfa9b","type":"UnionRenderers"},{"attributes":{"plot":null,"text":""},"id":"19f16a5b-ba60-460c-94e4-6939a01d6405","type":"Title"},{"attributes":{"dimension":1,"plot":{"id":"8b6c2486-1f40-4a82-a88d-4cd5e964b120","subtype":"Figure","type":"Plot"},"ticker":{"id":"68e24110-8006-4366-977a-ad84a864d342","type":"BasicTicker"},"visible":false},"id":"f207690e-ccb2-4c08-bf22-37b4bb2e699d","type":"Grid"},{"attributes":{"children":[{"id":"e1b73d6f-2cd9-4c6e-af1a-cbefe19aebbc","type":"WidgetBox"},{"id":"8b6c2486-1f40-4a82-a88d-4cd5e964b120","subtype":"Figure","type":"Plot"}]},"id":"ccaddf26-f51d-41ee-8456-70aea61fdc12","type":"Column"}],"root_ids":["ccaddf26-f51d-41ee-8456-70aea61fdc12"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"f270c445-a828-4d92-9a1a-bbc64e2cd341","roots":{"ccaddf26-f51d-41ee-8456-70aea61fdc12":"e2d6e188-d96e-417e-ac07-503d9a6dfeb5"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();