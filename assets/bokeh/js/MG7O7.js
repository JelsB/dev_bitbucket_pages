(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("2b386424-0926-433c-a188-91689cd080a6");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '2b386424-0926-433c-a188-91689cd080a6' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"61417f08-8a90-48d3-9a8f-a63f55dbab45":{"roots":{"references":[{"attributes":{},"id":"a469a127-b956-423e-8802-00be06d33d15","type":"UnionRenderers"},{"attributes":{"plot":{"id":"d70bc045-bf8a-48cc-b3ed-06cdf52fccfb","subtype":"Figure","type":"Plot"},"ticker":{"id":"f485e8b7-ed2d-47ca-a544-4c25d37c615d","type":"BasicTicker"},"visible":false},"id":"07b1b388-0356-4f2d-a774-2c43a38486b6","type":"Grid"},{"attributes":{},"id":"428250ed-15d9-4442-8e88-8737c57c1200","type":"Selection"},{"attributes":{"formatter":{"id":"5b47574a-5595-429e-b35d-64406a2d47e9","type":"BasicTickFormatter"},"plot":{"id":"d70bc045-bf8a-48cc-b3ed-06cdf52fccfb","subtype":"Figure","type":"Plot"},"ticker":{"id":"f485e8b7-ed2d-47ca-a544-4c25d37c615d","type":"BasicTicker"},"visible":false},"id":"6c2967f1-a6b1-4d9a-ba47-2dfedc5e2fc7","type":"LinearAxis"},{"attributes":{"formatter":{"id":"0f4c47ed-c558-4b3e-971f-f8d8375164e5","type":"BasicTickFormatter"},"plot":{"id":"d70bc045-bf8a-48cc-b3ed-06cdf52fccfb","subtype":"Figure","type":"Plot"},"ticker":{"id":"9c9a3d5e-8866-4fe9-90ab-46ec527672f8","type":"BasicTicker"},"visible":false},"id":"ffc9e69b-0e0c-4105-936f-34c411ded937","type":"LinearAxis"},{"attributes":{"children":[{"id":"b26a5c91-849d-409a-b126-0d4757cb0d70","type":"WidgetBox"},{"id":"d70bc045-bf8a-48cc-b3ed-06cdf52fccfb","subtype":"Figure","type":"Plot"}]},"id":"d990d397-b66d-4c37-9da0-ad968509a6aa","type":"Column"},{"attributes":{},"id":"d7b95d6a-f3af-4814-b01e-2d15ad0bd4c6","type":"LinearScale"},{"attributes":{},"id":"5b47574a-5595-429e-b35d-64406a2d47e9","type":"BasicTickFormatter"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"5a3ccf05-6b02-4878-8b03-4e540ed95c92","type":"Toolbar"},{"attributes":{},"id":"0f4c47ed-c558-4b3e-971f-f8d8375164e5","type":"BasicTickFormatter"},{"attributes":{},"id":"1768a1a5-7485-49a3-becf-4b459058a24f","type":"LinearScale"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/MG7O7_000.png"],"url_num":["000"]},"selected":{"id":"428250ed-15d9-4442-8e88-8737c57c1200","type":"Selection"},"selection_policy":{"id":"a469a127-b956-423e-8802-00be06d33d15","type":"UnionRenderers"}},"id":"442672e7-16f5-4e20-b201-0eeb0e30e1f7","type":"ColumnDataSource"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"36ed4e26-0501-4fef-8bdb-1faf6f21bc6d","type":"ImageURL"},{"attributes":{"data_source":{"id":"442672e7-16f5-4e20-b201-0eeb0e30e1f7","type":"ColumnDataSource"},"glyph":{"id":"fc47bf68-6148-4e0f-ac97-ef0e05a62a62","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"36ed4e26-0501-4fef-8bdb-1faf6f21bc6d","type":"ImageURL"},"selection_glyph":null,"view":{"id":"6853ae4d-c311-4b33-8f9c-4da8282e1dba","type":"CDSView"}},"id":"bc52def2-595c-4653-bcd0-9d47605882a6","type":"GlyphRenderer"},{"attributes":{"dimension":1,"plot":{"id":"d70bc045-bf8a-48cc-b3ed-06cdf52fccfb","subtype":"Figure","type":"Plot"},"ticker":{"id":"9c9a3d5e-8866-4fe9-90ab-46ec527672f8","type":"BasicTicker"},"visible":false},"id":"0e6e7473-c3b4-4a4c-a810-2ec28eee7b50","type":"Grid"},{"attributes":{"source":{"id":"442672e7-16f5-4e20-b201-0eeb0e30e1f7","type":"ColumnDataSource"}},"id":"6853ae4d-c311-4b33-8f9c-4da8282e1dba","type":"CDSView"},{"attributes":{},"id":"9c9a3d5e-8866-4fe9-90ab-46ec527672f8","type":"BasicTicker"},{"attributes":{},"id":"f485e8b7-ed2d-47ca-a544-4c25d37c615d","type":"BasicTicker"},{"attributes":{"below":[{"id":"6c2967f1-a6b1-4d9a-ba47-2dfedc5e2fc7","type":"LinearAxis"}],"left":[{"id":"ffc9e69b-0e0c-4105-936f-34c411ded937","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"6c2967f1-a6b1-4d9a-ba47-2dfedc5e2fc7","type":"LinearAxis"},{"id":"07b1b388-0356-4f2d-a774-2c43a38486b6","type":"Grid"},{"id":"ffc9e69b-0e0c-4105-936f-34c411ded937","type":"LinearAxis"},{"id":"0e6e7473-c3b4-4a4c-a810-2ec28eee7b50","type":"Grid"},{"id":"bc52def2-595c-4653-bcd0-9d47605882a6","type":"GlyphRenderer"}],"title":{"id":"ecd5f57e-a0b1-468f-8ee4-a609bfa513e3","type":"Title"},"toolbar":{"id":"5a3ccf05-6b02-4878-8b03-4e540ed95c92","type":"Toolbar"},"x_range":{"id":"ae756f22-4ddf-4d6c-a7c6-416117483ff4","type":"Range1d"},"x_scale":{"id":"d7b95d6a-f3af-4814-b01e-2d15ad0bd4c6","type":"LinearScale"},"y_range":{"id":"559bccce-7bbe-43d3-ad82-c28cb31727bf","type":"Range1d"},"y_scale":{"id":"1768a1a5-7485-49a3-becf-4b459058a24f","type":"LinearScale"}},"id":"d70bc045-bf8a-48cc-b3ed-06cdf52fccfb","subtype":"Figure","type":"Plot"},{"attributes":{"args":{"source":{"id":"442672e7-16f5-4e20-b201-0eeb0e30e1f7","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"6d4c147a-6a04-4e2b-8f6f-3662a2d093e0","type":"CustomJS"},{"attributes":{"plot":null,"text":""},"id":"ecd5f57e-a0b1-468f-8ee4-a609bfa513e3","type":"Title"},{"attributes":{"callback":null},"id":"559bccce-7bbe-43d3-ad82-c28cb31727bf","type":"Range1d"},{"attributes":{"callback":null},"id":"ae756f22-4ddf-4d6c-a7c6-416117483ff4","type":"Range1d"},{"attributes":{"children":[{"id":"3ba9f718-3091-4eba-bba5-8644e46dfeab","type":"Slider"}]},"id":"b26a5c91-849d-409a-b126-0d4757cb0d70","type":"WidgetBox"},{"attributes":{"callback":{"id":"6d4c147a-6a04-4e2b-8f6f-3662a2d093e0","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"3ba9f718-3091-4eba-bba5-8644e46dfeab","type":"Slider"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"fc47bf68-6148-4e0f-ac97-ef0e05a62a62","type":"ImageURL"}],"root_ids":["d990d397-b66d-4c37-9da0-ad968509a6aa"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"61417f08-8a90-48d3-9a8f-a63f55dbab45","roots":{"d990d397-b66d-4c37-9da0-ad968509a6aa":"2b386424-0926-433c-a188-91689cd080a6"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();