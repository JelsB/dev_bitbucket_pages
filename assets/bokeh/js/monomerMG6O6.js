(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("bd7d5f4b-7227-4edc-9c84-62fc11bed0e4");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'bd7d5f4b-7227-4edc-9c84-62fc11bed0e4' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"e92d440c-8771-46b7-9b0e-b7f08118852d":{"roots":{"references":[{"attributes":{"children":[{"id":"31a70b11-8df9-4266-af9c-0ed9205b3c71","type":"Slider"}]},"id":"3a76d8f0-6206-4e9e-b8e5-ae2cee3509f1","type":"WidgetBox"},{"attributes":{"children":[{"id":"3a76d8f0-6206-4e9e-b8e5-ae2cee3509f1","type":"WidgetBox"},{"id":"3a89662a-4b9a-4177-8cd5-07da5e3d34f5","subtype":"Figure","type":"Plot"}]},"id":"6c51a391-5e4d-4b9e-8000-cf1ff4243953","type":"Column"},{"attributes":{"callback":{"id":"42d3e714-42a3-4ade-a7cb-990400a70f66","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"31a70b11-8df9-4266-af9c-0ed9205b3c71","type":"Slider"},{"attributes":{"dimension":1,"plot":{"id":"3a89662a-4b9a-4177-8cd5-07da5e3d34f5","subtype":"Figure","type":"Plot"},"ticker":{"id":"9675b5d0-85ad-44ef-b0c6-c0c48c99b959","type":"BasicTicker"},"visible":false},"id":"d7ac417e-63d3-40e8-97a0-15067908081f","type":"Grid"},{"attributes":{"source":{"id":"2203002a-a134-42be-aeec-99e6f8272da5","type":"ColumnDataSource"}},"id":"c340c8cf-8604-4d93-9df7-8df766202bb2","type":"CDSView"},{"attributes":{"formatter":{"id":"e6c9e64c-bc91-48e8-86f5-d5b85f9555be","type":"BasicTickFormatter"},"plot":{"id":"3a89662a-4b9a-4177-8cd5-07da5e3d34f5","subtype":"Figure","type":"Plot"},"ticker":{"id":"1f5dcd00-1f98-4368-957a-1234d13f1da5","type":"BasicTicker"},"visible":false},"id":"18c566ec-6056-4701-9636-fba4166520cc","type":"LinearAxis"},{"attributes":{},"id":"1f5dcd00-1f98-4368-957a-1234d13f1da5","type":"BasicTicker"},{"attributes":{},"id":"e6c9e64c-bc91-48e8-86f5-d5b85f9555be","type":"BasicTickFormatter"},{"attributes":{"formatter":{"id":"25b80835-533c-4671-86fb-914b147adb3f","type":"BasicTickFormatter"},"plot":{"id":"3a89662a-4b9a-4177-8cd5-07da5e3d34f5","subtype":"Figure","type":"Plot"},"ticker":{"id":"9675b5d0-85ad-44ef-b0c6-c0c48c99b959","type":"BasicTicker"},"visible":false},"id":"1295ca33-f041-46fe-b2dc-a1e584b6d526","type":"LinearAxis"},{"attributes":{"below":[{"id":"18c566ec-6056-4701-9636-fba4166520cc","type":"LinearAxis"}],"left":[{"id":"1295ca33-f041-46fe-b2dc-a1e584b6d526","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"18c566ec-6056-4701-9636-fba4166520cc","type":"LinearAxis"},{"id":"d0b9c625-1c9c-4eab-a516-a8b7a2d00935","type":"Grid"},{"id":"1295ca33-f041-46fe-b2dc-a1e584b6d526","type":"LinearAxis"},{"id":"d7ac417e-63d3-40e8-97a0-15067908081f","type":"Grid"},{"id":"6c2b4649-9ffd-40ca-8b1d-45c04f0ba684","type":"GlyphRenderer"}],"title":{"id":"65dd1b18-be7b-4153-9fc6-2e2a5ac76a21","type":"Title"},"toolbar":{"id":"1e69f333-9d7e-4d6c-922b-3f837d9d7c30","type":"Toolbar"},"x_range":{"id":"42dbfd8c-a300-4ee0-be5c-a6fa8c1d21e4","type":"Range1d"},"x_scale":{"id":"98055afb-5b13-46fe-b427-08b95cfb30fd","type":"LinearScale"},"y_range":{"id":"d80f6876-3db3-48df-bea8-f03252d8f078","type":"Range1d"},"y_scale":{"id":"fd27102a-ba56-4fa1-8a3c-154053417be3","type":"LinearScale"}},"id":"3a89662a-4b9a-4177-8cd5-07da5e3d34f5","subtype":"Figure","type":"Plot"},{"attributes":{"data_source":{"id":"2203002a-a134-42be-aeec-99e6f8272da5","type":"ColumnDataSource"},"glyph":{"id":"afa768d7-1785-47bc-8e3e-059d856fed41","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"303567fc-d931-486d-8d47-58896059543a","type":"ImageURL"},"selection_glyph":null,"view":{"id":"c340c8cf-8604-4d93-9df7-8df766202bb2","type":"CDSView"}},"id":"6c2b4649-9ffd-40ca-8b1d-45c04f0ba684","type":"GlyphRenderer"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"1e69f333-9d7e-4d6c-922b-3f837d9d7c30","type":"Toolbar"},{"attributes":{"callback":null},"id":"42dbfd8c-a300-4ee0-be5c-a6fa8c1d21e4","type":"Range1d"},{"attributes":{"callback":null},"id":"d80f6876-3db3-48df-bea8-f03252d8f078","type":"Range1d"},{"attributes":{},"id":"fd27102a-ba56-4fa1-8a3c-154053417be3","type":"LinearScale"},{"attributes":{},"id":"98055afb-5b13-46fe-b427-08b95cfb30fd","type":"LinearScale"},{"attributes":{},"id":"9675b5d0-85ad-44ef-b0c6-c0c48c99b959","type":"BasicTicker"},{"attributes":{"plot":{"id":"3a89662a-4b9a-4177-8cd5-07da5e3d34f5","subtype":"Figure","type":"Plot"},"ticker":{"id":"1f5dcd00-1f98-4368-957a-1234d13f1da5","type":"BasicTicker"},"visible":false},"id":"d0b9c625-1c9c-4eab-a516-a8b7a2d00935","type":"Grid"},{"attributes":{},"id":"5b852827-a965-4a69-aacc-7712865121bc","type":"UnionRenderers"},{"attributes":{"args":{"source":{"id":"2203002a-a134-42be-aeec-99e6f8272da5","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"42d3e714-42a3-4ade-a7cb-990400a70f66","type":"CustomJS"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"303567fc-d931-486d-8d47-58896059543a","type":"ImageURL"},{"attributes":{},"id":"25b80835-533c-4671-86fb-914b147adb3f","type":"BasicTickFormatter"},{"attributes":{},"id":"dbb2894a-eeda-415e-8aa2-01768211e552","type":"Selection"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerMG6O6_000.png"],"url_num":["000"]},"selected":{"id":"dbb2894a-eeda-415e-8aa2-01768211e552","type":"Selection"},"selection_policy":{"id":"5b852827-a965-4a69-aacc-7712865121bc","type":"UnionRenderers"}},"id":"2203002a-a134-42be-aeec-99e6f8272da5","type":"ColumnDataSource"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"afa768d7-1785-47bc-8e3e-059d856fed41","type":"ImageURL"},{"attributes":{"plot":null,"text":""},"id":"65dd1b18-be7b-4153-9fc6-2e2a5ac76a21","type":"Title"}],"root_ids":["6c51a391-5e4d-4b9e-8000-cf1ff4243953"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"e92d440c-8771-46b7-9b0e-b7f08118852d","roots":{"6c51a391-5e4d-4b9e-8000-cf1ff4243953":"bd7d5f4b-7227-4edc-9c84-62fc11bed0e4"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();