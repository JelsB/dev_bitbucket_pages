(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("9be9c643-3c5d-4ad8-8045-82e0f922496e");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '9be9c643-3c5d-4ad8-8045-82e0f922496e' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"5697e318-be67-4030-98cc-2d0bbed5741e":{"roots":{"references":[{"attributes":{},"id":"50d1ce3c-2e4a-46bd-9408-2e99e070ace7","type":"Selection"},{"attributes":{},"id":"905723ee-289f-42fb-9880-1c318266b7ba","type":"BasicTickFormatter"},{"attributes":{"formatter":{"id":"905723ee-289f-42fb-9880-1c318266b7ba","type":"BasicTickFormatter"},"plot":{"id":"0d628014-d093-4bfb-9d9d-8510845e5c73","subtype":"Figure","type":"Plot"},"ticker":{"id":"341ce2db-112d-4736-ae6e-0afc952cf0ca","type":"BasicTicker"},"visible":false},"id":"e076f715-2416-408e-9e42-a93636bb424d","type":"LinearAxis"},{"attributes":{"plot":{"id":"0d628014-d093-4bfb-9d9d-8510845e5c73","subtype":"Figure","type":"Plot"},"ticker":{"id":"341ce2db-112d-4736-ae6e-0afc952cf0ca","type":"BasicTicker"},"visible":false},"id":"08073f8f-c7d7-427f-b7dc-10f3f1376757","type":"Grid"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerSI5O5_000.png"],"url_num":["000"]},"selected":{"id":"50d1ce3c-2e4a-46bd-9408-2e99e070ace7","type":"Selection"},"selection_policy":{"id":"ecc41bb4-1e14-4114-8292-ed9803c5bf64","type":"UnionRenderers"}},"id":"6d26024b-39d9-42e5-8c59-2125cfe4ad48","type":"ColumnDataSource"},{"attributes":{},"id":"229ab969-a2a1-400a-bcf2-b7e0df9db74a","type":"LinearScale"},{"attributes":{"below":[{"id":"e076f715-2416-408e-9e42-a93636bb424d","type":"LinearAxis"}],"left":[{"id":"ff8aae0f-bbb8-489e-baf7-d57b43c932fc","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"e076f715-2416-408e-9e42-a93636bb424d","type":"LinearAxis"},{"id":"08073f8f-c7d7-427f-b7dc-10f3f1376757","type":"Grid"},{"id":"ff8aae0f-bbb8-489e-baf7-d57b43c932fc","type":"LinearAxis"},{"id":"6a576aae-d0ff-46f5-b7a2-f21bc882e856","type":"Grid"},{"id":"7bb53040-5339-4df9-a5b9-54bb2848a2c2","type":"GlyphRenderer"}],"title":{"id":"98ec5221-00a1-4a2a-aa4b-9355fa04ce39","type":"Title"},"toolbar":{"id":"e665477b-d4d0-4e5a-b235-112837ff5da7","type":"Toolbar"},"x_range":{"id":"eff559a7-3cc0-41da-8072-a1c6606ee92c","type":"Range1d"},"x_scale":{"id":"229ab969-a2a1-400a-bcf2-b7e0df9db74a","type":"LinearScale"},"y_range":{"id":"027a444c-4a23-4dd6-b869-fb9f892fe541","type":"Range1d"},"y_scale":{"id":"309a15ab-74c3-4ea3-8764-211d36ee90c2","type":"LinearScale"}},"id":"0d628014-d093-4bfb-9d9d-8510845e5c73","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"ecc41bb4-1e14-4114-8292-ed9803c5bf64","type":"UnionRenderers"},{"attributes":{},"id":"309a15ab-74c3-4ea3-8764-211d36ee90c2","type":"LinearScale"},{"attributes":{"plot":null,"text":""},"id":"98ec5221-00a1-4a2a-aa4b-9355fa04ce39","type":"Title"},{"attributes":{},"id":"6e885e9f-d05a-4022-95e9-71b34dc921b9","type":"BasicTickFormatter"},{"attributes":{},"id":"341ce2db-112d-4736-ae6e-0afc952cf0ca","type":"BasicTicker"},{"attributes":{"callback":{"id":"b21058b6-2a3d-4d92-ba81-017f6bdddc11","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"d3d52350-d6ff-4b55-9797-340e70257252","type":"Slider"},{"attributes":{"children":[{"id":"74480c23-0a44-4481-b03c-4689cb10ca97","type":"WidgetBox"},{"id":"0d628014-d093-4bfb-9d9d-8510845e5c73","subtype":"Figure","type":"Plot"}]},"id":"e19cbcef-5a21-4db2-ab0d-81b4203d5ddf","type":"Column"},{"attributes":{"source":{"id":"6d26024b-39d9-42e5-8c59-2125cfe4ad48","type":"ColumnDataSource"}},"id":"c58908a5-8d3b-4f03-a598-9d0601ea1964","type":"CDSView"},{"attributes":{"children":[{"id":"d3d52350-d6ff-4b55-9797-340e70257252","type":"Slider"}]},"id":"74480c23-0a44-4481-b03c-4689cb10ca97","type":"WidgetBox"},{"attributes":{"callback":null},"id":"eff559a7-3cc0-41da-8072-a1c6606ee92c","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"a0d0f2d3-e92b-40ff-82f2-8e37c7e6d7a7","type":"ImageURL"},{"attributes":{"data_source":{"id":"6d26024b-39d9-42e5-8c59-2125cfe4ad48","type":"ColumnDataSource"},"glyph":{"id":"db930da5-3a8a-4278-86e2-4ae597fa4b3f","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"a0d0f2d3-e92b-40ff-82f2-8e37c7e6d7a7","type":"ImageURL"},"selection_glyph":null,"view":{"id":"c58908a5-8d3b-4f03-a598-9d0601ea1964","type":"CDSView"}},"id":"7bb53040-5339-4df9-a5b9-54bb2848a2c2","type":"GlyphRenderer"},{"attributes":{},"id":"30259e6c-084b-4831-9870-2c7abcabf0a0","type":"BasicTicker"},{"attributes":{"formatter":{"id":"6e885e9f-d05a-4022-95e9-71b34dc921b9","type":"BasicTickFormatter"},"plot":{"id":"0d628014-d093-4bfb-9d9d-8510845e5c73","subtype":"Figure","type":"Plot"},"ticker":{"id":"30259e6c-084b-4831-9870-2c7abcabf0a0","type":"BasicTicker"},"visible":false},"id":"ff8aae0f-bbb8-489e-baf7-d57b43c932fc","type":"LinearAxis"},{"attributes":{"dimension":1,"plot":{"id":"0d628014-d093-4bfb-9d9d-8510845e5c73","subtype":"Figure","type":"Plot"},"ticker":{"id":"30259e6c-084b-4831-9870-2c7abcabf0a0","type":"BasicTicker"},"visible":false},"id":"6a576aae-d0ff-46f5-b7a2-f21bc882e856","type":"Grid"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"e665477b-d4d0-4e5a-b235-112837ff5da7","type":"Toolbar"},{"attributes":{"args":{"source":{"id":"6d26024b-39d9-42e5-8c59-2125cfe4ad48","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"b21058b6-2a3d-4d92-ba81-017f6bdddc11","type":"CustomJS"},{"attributes":{"callback":null},"id":"027a444c-4a23-4dd6-b869-fb9f892fe541","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"db930da5-3a8a-4278-86e2-4ae597fa4b3f","type":"ImageURL"}],"root_ids":["e19cbcef-5a21-4db2-ab0d-81b4203d5ddf"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"5697e318-be67-4030-98cc-2d0bbed5741e","roots":{"e19cbcef-5a21-4db2-ab0d-81b4203d5ddf":"9be9c643-3c5d-4ad8-8045-82e0f922496e"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();