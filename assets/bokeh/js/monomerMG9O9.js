(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("5b5ed60b-9a49-4928-8ee4-4acdcfc6b20e");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '5b5ed60b-9a49-4928-8ee4-4acdcfc6b20e' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"e7089766-e973-434d-a354-afd703f2a836":{"roots":{"references":[{"attributes":{"dimension":1,"plot":{"id":"dc815490-fd70-46f4-ab5d-17cdd5c57c03","subtype":"Figure","type":"Plot"},"ticker":{"id":"a84451c2-d812-424e-9afb-d51a44cda9f9","type":"BasicTicker"},"visible":false},"id":"6e7a7f60-940b-42a7-94a3-e306b82f8c75","type":"Grid"},{"attributes":{},"id":"1895413b-b521-42f6-bfc9-a726c25dc693","type":"LinearScale"},{"attributes":{},"id":"a84451c2-d812-424e-9afb-d51a44cda9f9","type":"BasicTicker"},{"attributes":{"formatter":{"id":"80a1756c-903f-4bef-a438-cca2ee2f4f7c","type":"BasicTickFormatter"},"plot":{"id":"dc815490-fd70-46f4-ab5d-17cdd5c57c03","subtype":"Figure","type":"Plot"},"ticker":{"id":"a84451c2-d812-424e-9afb-d51a44cda9f9","type":"BasicTicker"},"visible":false},"id":"ff0d7865-328b-4cc7-8f64-b51c7fbbf834","type":"LinearAxis"},{"attributes":{"children":[{"id":"b666afd1-7a13-47fb-9240-cc735e211411","type":"Slider"}]},"id":"9c6df7a5-da6a-4cad-80ef-de66fad580a9","type":"WidgetBox"},{"attributes":{"source":{"id":"98ab7414-5823-4dd2-8cf8-fe5761255471","type":"ColumnDataSource"}},"id":"bdfac19f-469b-47e0-9723-f161921b4511","type":"CDSView"},{"attributes":{"formatter":{"id":"877d8a3b-6423-4cf3-ba2c-09b8cb11dedc","type":"BasicTickFormatter"},"plot":{"id":"dc815490-fd70-46f4-ab5d-17cdd5c57c03","subtype":"Figure","type":"Plot"},"ticker":{"id":"9351de0e-7592-4e54-9ee9-bc2d83779488","type":"BasicTicker"},"visible":false},"id":"5c5468d4-4f2a-4549-848d-b6c93c2c1686","type":"LinearAxis"},{"attributes":{"callback":null},"id":"318e4866-3ae7-4c01-a6d0-d313cc217c1e","type":"Range1d"},{"attributes":{},"id":"9f45c9a2-d229-45ec-b23f-476e15ad2aef","type":"LinearScale"},{"attributes":{},"id":"9a6f85bf-deb7-4ff4-b701-f916045a5639","type":"Selection"},{"attributes":{},"id":"28e0a598-281e-4b0a-b6a5-e568c0e26bfd","type":"UnionRenderers"},{"attributes":{"args":{"source":{"id":"98ab7414-5823-4dd2-8cf8-fe5761255471","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"8df47ee6-c458-476e-908d-a9637dbac761","type":"CustomJS"},{"attributes":{"data_source":{"id":"98ab7414-5823-4dd2-8cf8-fe5761255471","type":"ColumnDataSource"},"glyph":{"id":"8838ff33-2612-4169-9589-7d27b2c9fe24","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"f61dffc7-27d1-4ca6-9b4e-6d5f1ecc39be","type":"ImageURL"},"selection_glyph":null,"view":{"id":"bdfac19f-469b-47e0-9723-f161921b4511","type":"CDSView"}},"id":"cbfa1cc5-b85f-44ef-9ef3-7af66ef5170a","type":"GlyphRenderer"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"8838ff33-2612-4169-9589-7d27b2c9fe24","type":"ImageURL"},{"attributes":{"callback":{"id":"8df47ee6-c458-476e-908d-a9637dbac761","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"b666afd1-7a13-47fb-9240-cc735e211411","type":"Slider"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"f61dffc7-27d1-4ca6-9b4e-6d5f1ecc39be","type":"ImageURL"},{"attributes":{"children":[{"id":"9c6df7a5-da6a-4cad-80ef-de66fad580a9","type":"WidgetBox"},{"id":"dc815490-fd70-46f4-ab5d-17cdd5c57c03","subtype":"Figure","type":"Plot"}]},"id":"ff94ba7c-f9b7-44d6-8459-2a4e1d460a2f","type":"Column"},{"attributes":{"callback":null},"id":"fcc56d19-15b5-4716-a090-4b4d29420bd2","type":"Range1d"},{"attributes":{},"id":"9351de0e-7592-4e54-9ee9-bc2d83779488","type":"BasicTicker"},{"attributes":{"plot":null,"text":""},"id":"1338cbb8-de72-441a-8a31-f41e3f53aa7c","type":"Title"},{"attributes":{},"id":"80a1756c-903f-4bef-a438-cca2ee2f4f7c","type":"BasicTickFormatter"},{"attributes":{"plot":{"id":"dc815490-fd70-46f4-ab5d-17cdd5c57c03","subtype":"Figure","type":"Plot"},"ticker":{"id":"9351de0e-7592-4e54-9ee9-bc2d83779488","type":"BasicTicker"},"visible":false},"id":"40001aa9-bf85-423e-85e8-1b5d1a53e6a1","type":"Grid"},{"attributes":{},"id":"877d8a3b-6423-4cf3-ba2c-09b8cb11dedc","type":"BasicTickFormatter"},{"attributes":{"below":[{"id":"5c5468d4-4f2a-4549-848d-b6c93c2c1686","type":"LinearAxis"}],"left":[{"id":"ff0d7865-328b-4cc7-8f64-b51c7fbbf834","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"5c5468d4-4f2a-4549-848d-b6c93c2c1686","type":"LinearAxis"},{"id":"40001aa9-bf85-423e-85e8-1b5d1a53e6a1","type":"Grid"},{"id":"ff0d7865-328b-4cc7-8f64-b51c7fbbf834","type":"LinearAxis"},{"id":"6e7a7f60-940b-42a7-94a3-e306b82f8c75","type":"Grid"},{"id":"cbfa1cc5-b85f-44ef-9ef3-7af66ef5170a","type":"GlyphRenderer"}],"title":{"id":"1338cbb8-de72-441a-8a31-f41e3f53aa7c","type":"Title"},"toolbar":{"id":"9d8b8b67-3e15-45b1-a735-d6df4bc2dd65","type":"Toolbar"},"x_range":{"id":"318e4866-3ae7-4c01-a6d0-d313cc217c1e","type":"Range1d"},"x_scale":{"id":"9f45c9a2-d229-45ec-b23f-476e15ad2aef","type":"LinearScale"},"y_range":{"id":"fcc56d19-15b5-4716-a090-4b4d29420bd2","type":"Range1d"},"y_scale":{"id":"1895413b-b521-42f6-bfc9-a726c25dc693","type":"LinearScale"}},"id":"dc815490-fd70-46f4-ab5d-17cdd5c57c03","subtype":"Figure","type":"Plot"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"9d8b8b67-3e15-45b1-a735-d6df4bc2dd65","type":"Toolbar"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerMG9O9_000.png"],"url_num":["000"]},"selected":{"id":"9a6f85bf-deb7-4ff4-b701-f916045a5639","type":"Selection"},"selection_policy":{"id":"28e0a598-281e-4b0a-b6a5-e568c0e26bfd","type":"UnionRenderers"}},"id":"98ab7414-5823-4dd2-8cf8-fe5761255471","type":"ColumnDataSource"}],"root_ids":["ff94ba7c-f9b7-44d6-8459-2a4e1d460a2f"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"e7089766-e973-434d-a354-afd703f2a836","roots":{"ff94ba7c-f9b7-44d6-8459-2a4e1d460a2f":"5b5ed60b-9a49-4928-8ee4-4acdcfc6b20e"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();