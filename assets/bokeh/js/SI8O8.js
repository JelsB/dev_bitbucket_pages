(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("94902a42-329c-4d42-986d-b43c7c53b38a");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '94902a42-329c-4d42-986d-b43c7c53b38a' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"23d9feca-b79f-4bee-b581-a939752ea2c4":{"roots":{"references":[{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"74f321d9-283a-4181-ac19-6ae61ec922ec","type":"ImageURL"},{"attributes":{},"id":"6e6a9716-6ad1-48e6-8610-9cf025b4c622","type":"Selection"},{"attributes":{"children":[{"id":"a3ac0f1b-a0c0-405b-9358-8d28a1635fa1","type":"WidgetBox"},{"id":"ce4e0c73-3e1c-4aa9-a420-5a67112f7dec","subtype":"Figure","type":"Plot"}]},"id":"589e0bc5-360a-440e-a0ee-eba645192e23","type":"Column"},{"attributes":{},"id":"7f0eb3cd-50d5-41fe-a8cb-72581e097084","type":"BasicTicker"},{"attributes":{"source":{"id":"bb856c9d-4659-49af-968a-0f729168a599","type":"ColumnDataSource"}},"id":"20f0be45-a349-465a-8ef9-8004b6dcd198","type":"CDSView"},{"attributes":{"dimension":1,"plot":{"id":"ce4e0c73-3e1c-4aa9-a420-5a67112f7dec","subtype":"Figure","type":"Plot"},"ticker":{"id":"7f0eb3cd-50d5-41fe-a8cb-72581e097084","type":"BasicTicker"},"visible":false},"id":"e1132ccf-f628-4601-9be7-a0f96df672b7","type":"Grid"},{"attributes":{},"id":"a1e299c6-e4cc-4514-bae9-674cb456fab8","type":"BasicTickFormatter"},{"attributes":{},"id":"1e9a8e8b-31d5-41d8-b27a-feefe332a6e6","type":"LinearScale"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/SI8O8_000.png"],"url_num":["000"]},"selected":{"id":"6e6a9716-6ad1-48e6-8610-9cf025b4c622","type":"Selection"},"selection_policy":{"id":"e1154b86-f7f4-481a-810b-d2cfaf6f0f31","type":"UnionRenderers"}},"id":"bb856c9d-4659-49af-968a-0f729168a599","type":"ColumnDataSource"},{"attributes":{},"id":"4e466fbc-b313-4ba7-a5fe-3740242e2774","type":"BasicTickFormatter"},{"attributes":{"callback":{"id":"60cc8035-1c3a-43c1-b1d7-2ca3d98018ff","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"8641eb0d-7c6b-4338-a474-7057a02d5180","type":"Slider"},{"attributes":{"plot":null,"text":""},"id":"49d5021f-e5f3-4044-816b-c10790382edb","type":"Title"},{"attributes":{"args":{"source":{"id":"bb856c9d-4659-49af-968a-0f729168a599","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"60cc8035-1c3a-43c1-b1d7-2ca3d98018ff","type":"CustomJS"},{"attributes":{"callback":null},"id":"773215ed-e8f7-4510-8b05-f9370635b686","type":"Range1d"},{"attributes":{"children":[{"id":"8641eb0d-7c6b-4338-a474-7057a02d5180","type":"Slider"}]},"id":"a3ac0f1b-a0c0-405b-9358-8d28a1635fa1","type":"WidgetBox"},{"attributes":{"formatter":{"id":"4e466fbc-b313-4ba7-a5fe-3740242e2774","type":"BasicTickFormatter"},"plot":{"id":"ce4e0c73-3e1c-4aa9-a420-5a67112f7dec","subtype":"Figure","type":"Plot"},"ticker":{"id":"3f1b91bc-78d1-4290-9f86-de6f1d75b4f3","type":"BasicTicker"},"visible":false},"id":"e2bdf62c-f575-47ef-a66e-fab335dc1015","type":"LinearAxis"},{"attributes":{"plot":{"id":"ce4e0c73-3e1c-4aa9-a420-5a67112f7dec","subtype":"Figure","type":"Plot"},"ticker":{"id":"3f1b91bc-78d1-4290-9f86-de6f1d75b4f3","type":"BasicTicker"},"visible":false},"id":"4fb8becf-4d87-4082-82a5-1602fb948e7f","type":"Grid"},{"attributes":{},"id":"e1154b86-f7f4-481a-810b-d2cfaf6f0f31","type":"UnionRenderers"},{"attributes":{"below":[{"id":"e2bdf62c-f575-47ef-a66e-fab335dc1015","type":"LinearAxis"}],"left":[{"id":"fe204fae-d588-4c36-be38-86d93c711eb2","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"e2bdf62c-f575-47ef-a66e-fab335dc1015","type":"LinearAxis"},{"id":"4fb8becf-4d87-4082-82a5-1602fb948e7f","type":"Grid"},{"id":"fe204fae-d588-4c36-be38-86d93c711eb2","type":"LinearAxis"},{"id":"e1132ccf-f628-4601-9be7-a0f96df672b7","type":"Grid"},{"id":"5edee06e-264c-4213-8a86-0e183990e04a","type":"GlyphRenderer"}],"title":{"id":"49d5021f-e5f3-4044-816b-c10790382edb","type":"Title"},"toolbar":{"id":"8ec0ca20-737f-4bf8-b4b2-8a6a8f7569a3","type":"Toolbar"},"x_range":{"id":"773215ed-e8f7-4510-8b05-f9370635b686","type":"Range1d"},"x_scale":{"id":"49cbda54-641a-4e2f-aecf-de153e55d9f2","type":"LinearScale"},"y_range":{"id":"25edb16f-6476-447a-a2f6-4a70fe1a2793","type":"Range1d"},"y_scale":{"id":"1e9a8e8b-31d5-41d8-b27a-feefe332a6e6","type":"LinearScale"}},"id":"ce4e0c73-3e1c-4aa9-a420-5a67112f7dec","subtype":"Figure","type":"Plot"},{"attributes":{"formatter":{"id":"a1e299c6-e4cc-4514-bae9-674cb456fab8","type":"BasicTickFormatter"},"plot":{"id":"ce4e0c73-3e1c-4aa9-a420-5a67112f7dec","subtype":"Figure","type":"Plot"},"ticker":{"id":"7f0eb3cd-50d5-41fe-a8cb-72581e097084","type":"BasicTicker"},"visible":false},"id":"fe204fae-d588-4c36-be38-86d93c711eb2","type":"LinearAxis"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"8ec0ca20-737f-4bf8-b4b2-8a6a8f7569a3","type":"Toolbar"},{"attributes":{},"id":"49cbda54-641a-4e2f-aecf-de153e55d9f2","type":"LinearScale"},{"attributes":{"callback":null},"id":"25edb16f-6476-447a-a2f6-4a70fe1a2793","type":"Range1d"},{"attributes":{"data_source":{"id":"bb856c9d-4659-49af-968a-0f729168a599","type":"ColumnDataSource"},"glyph":{"id":"74f321d9-283a-4181-ac19-6ae61ec922ec","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"8ee212ee-7699-4658-b963-3f1b7de78db8","type":"ImageURL"},"selection_glyph":null,"view":{"id":"20f0be45-a349-465a-8ef9-8004b6dcd198","type":"CDSView"}},"id":"5edee06e-264c-4213-8a86-0e183990e04a","type":"GlyphRenderer"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"8ee212ee-7699-4658-b963-3f1b7de78db8","type":"ImageURL"},{"attributes":{},"id":"3f1b91bc-78d1-4290-9f86-de6f1d75b4f3","type":"BasicTicker"}],"root_ids":["589e0bc5-360a-440e-a0ee-eba645192e23"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"23d9feca-b79f-4bee-b581-a939752ea2c4","roots":{"589e0bc5-360a-440e-a0ee-eba645192e23":"94902a42-329c-4d42-986d-b43c7c53b38a"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();