(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("a4415b38-0d5c-4322-bca7-c53758895695");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'a4415b38-0d5c-4322-bca7-c53758895695' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"f3121198-5eef-4922-a026-43a803718bc0":{"roots":{"references":[{"attributes":{"children":[{"id":"f2404d38-eead-4e91-bfc1-c12843a16649","type":"WidgetBox"},{"id":"3eeca323-bd71-4599-96a5-74e7b721e41e","subtype":"Figure","type":"Plot"}]},"id":"bf4dc818-359d-429f-ba0c-4bab1823be9c","type":"Column"},{"attributes":{},"id":"1fdb3618-8c71-4685-9c27-3ca2ec06cd3a","type":"BasicTickFormatter"},{"attributes":{},"id":"6ce6aa4e-42b1-43f2-9c86-74a1ecfd96b6","type":"BasicTickFormatter"},{"attributes":{"plot":{"id":"3eeca323-bd71-4599-96a5-74e7b721e41e","subtype":"Figure","type":"Plot"},"ticker":{"id":"a9223b55-2ee5-42b6-84c1-3b3eaed321a2","type":"BasicTicker"},"visible":false},"id":"a12b89bb-8bb3-4f37-8514-3b7051e8869a","type":"Grid"},{"attributes":{"callback":null},"id":"a6b3abb1-029a-4129-a3cb-7fa06943c36d","type":"Range1d"},{"attributes":{},"id":"a9223b55-2ee5-42b6-84c1-3b3eaed321a2","type":"BasicTicker"},{"attributes":{"source":{"id":"3a5b861d-8562-438e-b4e3-aa86e784e17d","type":"ColumnDataSource"}},"id":"6fa8aacd-db29-4065-94fe-5178d3c77d56","type":"CDSView"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"6f738276-c0a8-4867-a892-7a1fa4fede0a","type":"Toolbar"},{"attributes":{"data_source":{"id":"3a5b861d-8562-438e-b4e3-aa86e784e17d","type":"ColumnDataSource"},"glyph":{"id":"7683f38d-0d7b-4347-9ba4-74733100effb","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"92c636eb-3028-4edd-8c55-592daa088fc9","type":"ImageURL"},"selection_glyph":null,"view":{"id":"6fa8aacd-db29-4065-94fe-5178d3c77d56","type":"CDSView"}},"id":"4bad2fea-1ba9-4e42-93bb-de503ca043e3","type":"GlyphRenderer"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/MG5O5_000.png"],"url_num":["000"]},"selected":{"id":"1183663f-a780-4ff6-bfb1-a9cae721506b","type":"Selection"},"selection_policy":{"id":"fa277571-a0a3-4d3a-94cd-5e1f4c5f35ed","type":"UnionRenderers"}},"id":"3a5b861d-8562-438e-b4e3-aa86e784e17d","type":"ColumnDataSource"},{"attributes":{"callback":null},"id":"455143ef-0034-41cf-b9d5-c454ff4e691f","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"92c636eb-3028-4edd-8c55-592daa088fc9","type":"ImageURL"},{"attributes":{"dimension":1,"plot":{"id":"3eeca323-bd71-4599-96a5-74e7b721e41e","subtype":"Figure","type":"Plot"},"ticker":{"id":"9924d28c-279b-41fe-9804-c21b36c30df6","type":"BasicTicker"},"visible":false},"id":"68d7fb53-97a5-422c-8f9c-e5c765cd6c17","type":"Grid"},{"attributes":{},"id":"fa277571-a0a3-4d3a-94cd-5e1f4c5f35ed","type":"UnionRenderers"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"7683f38d-0d7b-4347-9ba4-74733100effb","type":"ImageURL"},{"attributes":{"args":{"source":{"id":"3a5b861d-8562-438e-b4e3-aa86e784e17d","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"8d53aa54-1404-4402-b6e7-2f3c30b64a8e","type":"CustomJS"},{"attributes":{},"id":"5429285e-1c09-4915-92af-3c219cfc4a23","type":"LinearScale"},{"attributes":{"formatter":{"id":"1fdb3618-8c71-4685-9c27-3ca2ec06cd3a","type":"BasicTickFormatter"},"plot":{"id":"3eeca323-bd71-4599-96a5-74e7b721e41e","subtype":"Figure","type":"Plot"},"ticker":{"id":"9924d28c-279b-41fe-9804-c21b36c30df6","type":"BasicTicker"},"visible":false},"id":"c20e19e2-f519-4cc2-8afa-a7e9100ac1e7","type":"LinearAxis"},{"attributes":{"plot":null,"text":""},"id":"9ac557af-06dd-45c2-9f23-2e29cfd3e6d9","type":"Title"},{"attributes":{"formatter":{"id":"6ce6aa4e-42b1-43f2-9c86-74a1ecfd96b6","type":"BasicTickFormatter"},"plot":{"id":"3eeca323-bd71-4599-96a5-74e7b721e41e","subtype":"Figure","type":"Plot"},"ticker":{"id":"a9223b55-2ee5-42b6-84c1-3b3eaed321a2","type":"BasicTicker"},"visible":false},"id":"a344ea51-511b-4d99-bcd0-f1c90d9560a1","type":"LinearAxis"},{"attributes":{},"id":"9924d28c-279b-41fe-9804-c21b36c30df6","type":"BasicTicker"},{"attributes":{},"id":"b2173417-9352-4caa-a1af-673c2783d687","type":"LinearScale"},{"attributes":{"below":[{"id":"a344ea51-511b-4d99-bcd0-f1c90d9560a1","type":"LinearAxis"}],"left":[{"id":"c20e19e2-f519-4cc2-8afa-a7e9100ac1e7","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"a344ea51-511b-4d99-bcd0-f1c90d9560a1","type":"LinearAxis"},{"id":"a12b89bb-8bb3-4f37-8514-3b7051e8869a","type":"Grid"},{"id":"c20e19e2-f519-4cc2-8afa-a7e9100ac1e7","type":"LinearAxis"},{"id":"68d7fb53-97a5-422c-8f9c-e5c765cd6c17","type":"Grid"},{"id":"4bad2fea-1ba9-4e42-93bb-de503ca043e3","type":"GlyphRenderer"}],"title":{"id":"9ac557af-06dd-45c2-9f23-2e29cfd3e6d9","type":"Title"},"toolbar":{"id":"6f738276-c0a8-4867-a892-7a1fa4fede0a","type":"Toolbar"},"x_range":{"id":"455143ef-0034-41cf-b9d5-c454ff4e691f","type":"Range1d"},"x_scale":{"id":"b2173417-9352-4caa-a1af-673c2783d687","type":"LinearScale"},"y_range":{"id":"a6b3abb1-029a-4129-a3cb-7fa06943c36d","type":"Range1d"},"y_scale":{"id":"5429285e-1c09-4915-92af-3c219cfc4a23","type":"LinearScale"}},"id":"3eeca323-bd71-4599-96a5-74e7b721e41e","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"1183663f-a780-4ff6-bfb1-a9cae721506b","type":"Selection"},{"attributes":{"callback":{"id":"8d53aa54-1404-4402-b6e7-2f3c30b64a8e","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"0fe74e4a-a3df-4166-a8ca-89844b7c4cb3","type":"Slider"},{"attributes":{"children":[{"id":"0fe74e4a-a3df-4166-a8ca-89844b7c4cb3","type":"Slider"}]},"id":"f2404d38-eead-4e91-bfc1-c12843a16649","type":"WidgetBox"}],"root_ids":["bf4dc818-359d-429f-ba0c-4bab1823be9c"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"f3121198-5eef-4922-a026-43a803718bc0","roots":{"bf4dc818-359d-429f-ba0c-4bab1823be9c":"a4415b38-0d5c-4322-bca7-c53758895695"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();