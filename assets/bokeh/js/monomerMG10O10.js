(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("c9c5402c-c146-4fed-86d1-90a3527e6a31");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'c9c5402c-c146-4fed-86d1-90a3527e6a31' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"d042b5af-614a-403a-9015-36d24f304107":{"roots":{"references":[{"attributes":{"children":[{"id":"8f088713-f86e-456c-83f0-2dc57ed93aa4","type":"Slider"}]},"id":"1a09e5c1-a70c-4614-a47a-ccdaf52321fe","type":"WidgetBox"},{"attributes":{"formatter":{"id":"30e83e4d-ee51-4c03-979e-8cd481ecd117","type":"BasicTickFormatter"},"plot":{"id":"ca8cfcdb-6749-4060-b077-8feea6c0aaec","subtype":"Figure","type":"Plot"},"ticker":{"id":"20a7ca77-877f-438e-9b9a-2a4f514059c4","type":"BasicTicker"},"visible":false},"id":"54cfdfea-8638-4fa9-a615-291dee46dff0","type":"LinearAxis"},{"attributes":{},"id":"46330c3f-844f-41bb-8db2-c37ec234521c","type":"BasicTickFormatter"},{"attributes":{},"id":"9c462d92-017e-48e6-85ad-b333332bc818","type":"Selection"},{"attributes":{"callback":null},"id":"ebddd8e0-a1b6-4aaf-b6ba-2153fbc47936","type":"Range1d"},{"attributes":{"plot":{"id":"ca8cfcdb-6749-4060-b077-8feea6c0aaec","subtype":"Figure","type":"Plot"},"ticker":{"id":"20a7ca77-877f-438e-9b9a-2a4f514059c4","type":"BasicTicker"},"visible":false},"id":"5c08f2d6-d41f-4c8b-91ff-fa68f25a258c","type":"Grid"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"3369fce1-f910-4f09-8e73-a597d5bcaf41","type":"ImageURL"},{"attributes":{"callback":null},"id":"a2cf9f61-b1d9-43b7-a569-b6c98a8d12ec","type":"Range1d"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"fd18c490-1591-4c20-b3a2-a125108c213f","type":"Toolbar"},{"attributes":{"data_source":{"id":"e910f81f-3c68-41bc-bb3a-e621ebfa5ba0","type":"ColumnDataSource"},"glyph":{"id":"2b9c2565-c3fe-4f20-ba9c-4582e250e57b","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"3369fce1-f910-4f09-8e73-a597d5bcaf41","type":"ImageURL"},"selection_glyph":null,"view":{"id":"801f88ed-55cb-4317-8c50-fedede450d30","type":"CDSView"}},"id":"5df91be7-a97e-4f19-83a2-3f8d12a5497c","type":"GlyphRenderer"},{"attributes":{"plot":null,"text":""},"id":"989ab723-2276-4c09-b81e-29fd91000003","type":"Title"},{"attributes":{},"id":"20a7ca77-877f-438e-9b9a-2a4f514059c4","type":"BasicTicker"},{"attributes":{"dimension":1,"plot":{"id":"ca8cfcdb-6749-4060-b077-8feea6c0aaec","subtype":"Figure","type":"Plot"},"ticker":{"id":"9f4e5e13-c045-44e3-8d84-02933f719b7b","type":"BasicTicker"},"visible":false},"id":"50fb0d00-bb2d-4ef9-a810-08a7522e0088","type":"Grid"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"2b9c2565-c3fe-4f20-ba9c-4582e250e57b","type":"ImageURL"},{"attributes":{"formatter":{"id":"46330c3f-844f-41bb-8db2-c37ec234521c","type":"BasicTickFormatter"},"plot":{"id":"ca8cfcdb-6749-4060-b077-8feea6c0aaec","subtype":"Figure","type":"Plot"},"ticker":{"id":"9f4e5e13-c045-44e3-8d84-02933f719b7b","type":"BasicTicker"},"visible":false},"id":"937b7609-53b2-48c1-8194-095944f12e0a","type":"LinearAxis"},{"attributes":{},"id":"30e83e4d-ee51-4c03-979e-8cd481ecd117","type":"BasicTickFormatter"},{"attributes":{},"id":"f6602066-fe7e-4a39-9995-de5df3ed4aba","type":"LinearScale"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerMG10O10_000.png"],"url_num":["000"]},"selected":{"id":"9c462d92-017e-48e6-85ad-b333332bc818","type":"Selection"},"selection_policy":{"id":"76993e8b-60e5-4a7b-aef8-38c14ecd6682","type":"UnionRenderers"}},"id":"e910f81f-3c68-41bc-bb3a-e621ebfa5ba0","type":"ColumnDataSource"},{"attributes":{},"id":"3f83ac41-e3ae-4cb4-b296-2a64cc4faf24","type":"LinearScale"},{"attributes":{"args":{"source":{"id":"e910f81f-3c68-41bc-bb3a-e621ebfa5ba0","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"cf950eff-3e28-4967-bc8e-1c17c2b3be7c","type":"CustomJS"},{"attributes":{"below":[{"id":"54cfdfea-8638-4fa9-a615-291dee46dff0","type":"LinearAxis"}],"left":[{"id":"937b7609-53b2-48c1-8194-095944f12e0a","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"54cfdfea-8638-4fa9-a615-291dee46dff0","type":"LinearAxis"},{"id":"5c08f2d6-d41f-4c8b-91ff-fa68f25a258c","type":"Grid"},{"id":"937b7609-53b2-48c1-8194-095944f12e0a","type":"LinearAxis"},{"id":"50fb0d00-bb2d-4ef9-a810-08a7522e0088","type":"Grid"},{"id":"5df91be7-a97e-4f19-83a2-3f8d12a5497c","type":"GlyphRenderer"}],"title":{"id":"989ab723-2276-4c09-b81e-29fd91000003","type":"Title"},"toolbar":{"id":"fd18c490-1591-4c20-b3a2-a125108c213f","type":"Toolbar"},"x_range":{"id":"ebddd8e0-a1b6-4aaf-b6ba-2153fbc47936","type":"Range1d"},"x_scale":{"id":"f6602066-fe7e-4a39-9995-de5df3ed4aba","type":"LinearScale"},"y_range":{"id":"a2cf9f61-b1d9-43b7-a569-b6c98a8d12ec","type":"Range1d"},"y_scale":{"id":"3f83ac41-e3ae-4cb4-b296-2a64cc4faf24","type":"LinearScale"}},"id":"ca8cfcdb-6749-4060-b077-8feea6c0aaec","subtype":"Figure","type":"Plot"},{"attributes":{"source":{"id":"e910f81f-3c68-41bc-bb3a-e621ebfa5ba0","type":"ColumnDataSource"}},"id":"801f88ed-55cb-4317-8c50-fedede450d30","type":"CDSView"},{"attributes":{},"id":"9f4e5e13-c045-44e3-8d84-02933f719b7b","type":"BasicTicker"},{"attributes":{"children":[{"id":"1a09e5c1-a70c-4614-a47a-ccdaf52321fe","type":"WidgetBox"},{"id":"ca8cfcdb-6749-4060-b077-8feea6c0aaec","subtype":"Figure","type":"Plot"}]},"id":"ff6f3db8-a17f-48c2-b50c-146f3f895f35","type":"Column"},{"attributes":{"callback":{"id":"cf950eff-3e28-4967-bc8e-1c17c2b3be7c","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"8f088713-f86e-456c-83f0-2dc57ed93aa4","type":"Slider"},{"attributes":{},"id":"76993e8b-60e5-4a7b-aef8-38c14ecd6682","type":"UnionRenderers"}],"root_ids":["ff6f3db8-a17f-48c2-b50c-146f3f895f35"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"d042b5af-614a-403a-9015-36d24f304107","roots":{"ff6f3db8-a17f-48c2-b50c-146f3f895f35":"c9c5402c-c146-4fed-86d1-90a3527e6a31"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();