(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("f338bf33-53f2-49c5-bb03-fc45f14236fa");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'f338bf33-53f2-49c5-bb03-fc45f14236fa' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"cefb0d75-1c92-45fc-b08d-652b7a3d6378":{"roots":{"references":[{"attributes":{},"id":"76c5b96c-80b0-4b8c-8514-0e4e2a29892e","type":"BasicTicker"},{"attributes":{"source":{"id":"8fb14a7d-e79c-410c-8c4c-ef9fd67b542f","type":"ColumnDataSource"}},"id":"aad372f9-0eee-48d4-aeb4-37ca72751fec","type":"CDSView"},{"attributes":{"callback":null},"id":"4ee5aa45-0cac-4fa1-bb28-e8a0eb52b76b","type":"Range1d"},{"attributes":{},"id":"d455c6aa-059e-4bea-9e42-e1b6e289d799","type":"BasicTickFormatter"},{"attributes":{"plot":null,"text":""},"id":"97881692-e5ac-4391-9879-9d06be47283d","type":"Title"},{"attributes":{"callback":{"id":"9e7cf846-5b3a-42b3-8d10-f7fe31b56c9e","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"1de6fc81-e039-4de8-8cb7-be4ea2de346f","type":"Slider"},{"attributes":{"callback":null},"id":"e26a03e2-d9fb-42b6-ac46-fda5bb8ecf85","type":"Range1d"},{"attributes":{},"id":"4ce5b45e-e47d-4bd5-800c-b26d52130d3e","type":"LinearScale"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"bdbbf4e7-6f5a-4e93-89ed-6b31d5564412","type":"ImageURL"},{"attributes":{},"id":"8d8ddc51-5d59-468a-aacb-7a25766418fe","type":"BasicTicker"},{"attributes":{},"id":"fa6de2cf-c016-44a1-aebd-ec58fad69371","type":"Selection"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/AL14O21_000.png"],"url_num":["000"]},"selected":{"id":"fa6de2cf-c016-44a1-aebd-ec58fad69371","type":"Selection"},"selection_policy":{"id":"89a79647-83d5-4273-9f33-f4940ea94423","type":"UnionRenderers"}},"id":"8fb14a7d-e79c-410c-8c4c-ef9fd67b542f","type":"ColumnDataSource"},{"attributes":{},"id":"321a4bb9-59a9-460a-8bc0-2fc09096183a","type":"BasicTickFormatter"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"86ac3d41-679e-401b-b50f-0ddfedc140fc","type":"ImageURL"},{"attributes":{"plot":{"id":"f1fd3763-b385-4415-9d85-bbd4a2148b45","subtype":"Figure","type":"Plot"},"ticker":{"id":"76c5b96c-80b0-4b8c-8514-0e4e2a29892e","type":"BasicTicker"},"visible":false},"id":"f21e2286-9bef-48af-8c07-a1f12aa92c31","type":"Grid"},{"attributes":{"children":[{"id":"1de6fc81-e039-4de8-8cb7-be4ea2de346f","type":"Slider"}]},"id":"89232c77-f178-4d00-a407-b0e7113e250e","type":"WidgetBox"},{"attributes":{"data_source":{"id":"8fb14a7d-e79c-410c-8c4c-ef9fd67b542f","type":"ColumnDataSource"},"glyph":{"id":"86ac3d41-679e-401b-b50f-0ddfedc140fc","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"bdbbf4e7-6f5a-4e93-89ed-6b31d5564412","type":"ImageURL"},"selection_glyph":null,"view":{"id":"aad372f9-0eee-48d4-aeb4-37ca72751fec","type":"CDSView"}},"id":"11fd6932-8a15-47ec-98ab-4b453d17a4e3","type":"GlyphRenderer"},{"attributes":{"below":[{"id":"87787b27-a858-49a5-bfdd-7d4572572cc0","type":"LinearAxis"}],"left":[{"id":"a9d56357-aa6e-4df3-b07e-2e6352203f9f","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"87787b27-a858-49a5-bfdd-7d4572572cc0","type":"LinearAxis"},{"id":"f21e2286-9bef-48af-8c07-a1f12aa92c31","type":"Grid"},{"id":"a9d56357-aa6e-4df3-b07e-2e6352203f9f","type":"LinearAxis"},{"id":"b4c9371a-24e2-4acb-958c-7e131f0ae865","type":"Grid"},{"id":"11fd6932-8a15-47ec-98ab-4b453d17a4e3","type":"GlyphRenderer"}],"title":{"id":"97881692-e5ac-4391-9879-9d06be47283d","type":"Title"},"toolbar":{"id":"1f2c1e4e-3b81-4a6f-847e-2d4ac60f5081","type":"Toolbar"},"x_range":{"id":"e26a03e2-d9fb-42b6-ac46-fda5bb8ecf85","type":"Range1d"},"x_scale":{"id":"c24cce8e-1ea0-44e7-92df-d98a2e2ab54b","type":"LinearScale"},"y_range":{"id":"4ee5aa45-0cac-4fa1-bb28-e8a0eb52b76b","type":"Range1d"},"y_scale":{"id":"4ce5b45e-e47d-4bd5-800c-b26d52130d3e","type":"LinearScale"}},"id":"f1fd3763-b385-4415-9d85-bbd4a2148b45","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"89a79647-83d5-4273-9f33-f4940ea94423","type":"UnionRenderers"},{"attributes":{"formatter":{"id":"321a4bb9-59a9-460a-8bc0-2fc09096183a","type":"BasicTickFormatter"},"plot":{"id":"f1fd3763-b385-4415-9d85-bbd4a2148b45","subtype":"Figure","type":"Plot"},"ticker":{"id":"8d8ddc51-5d59-468a-aacb-7a25766418fe","type":"BasicTicker"},"visible":false},"id":"a9d56357-aa6e-4df3-b07e-2e6352203f9f","type":"LinearAxis"},{"attributes":{"children":[{"id":"89232c77-f178-4d00-a407-b0e7113e250e","type":"WidgetBox"},{"id":"f1fd3763-b385-4415-9d85-bbd4a2148b45","subtype":"Figure","type":"Plot"}]},"id":"377e0ae0-d1b1-4a57-b5c7-3124fa5344d2","type":"Column"},{"attributes":{},"id":"c24cce8e-1ea0-44e7-92df-d98a2e2ab54b","type":"LinearScale"},{"attributes":{"formatter":{"id":"d455c6aa-059e-4bea-9e42-e1b6e289d799","type":"BasicTickFormatter"},"plot":{"id":"f1fd3763-b385-4415-9d85-bbd4a2148b45","subtype":"Figure","type":"Plot"},"ticker":{"id":"76c5b96c-80b0-4b8c-8514-0e4e2a29892e","type":"BasicTicker"},"visible":false},"id":"87787b27-a858-49a5-bfdd-7d4572572cc0","type":"LinearAxis"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"1f2c1e4e-3b81-4a6f-847e-2d4ac60f5081","type":"Toolbar"},{"attributes":{"args":{"source":{"id":"8fb14a7d-e79c-410c-8c4c-ef9fd67b542f","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"9e7cf846-5b3a-42b3-8d10-f7fe31b56c9e","type":"CustomJS"},{"attributes":{"dimension":1,"plot":{"id":"f1fd3763-b385-4415-9d85-bbd4a2148b45","subtype":"Figure","type":"Plot"},"ticker":{"id":"8d8ddc51-5d59-468a-aacb-7a25766418fe","type":"BasicTicker"},"visible":false},"id":"b4c9371a-24e2-4acb-958c-7e131f0ae865","type":"Grid"}],"root_ids":["377e0ae0-d1b1-4a57-b5c7-3124fa5344d2"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"cefb0d75-1c92-45fc-b08d-652b7a3d6378","roots":{"377e0ae0-d1b1-4a57-b5c7-3124fa5344d2":"f338bf33-53f2-49c5-bb03-fc45f14236fa"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();