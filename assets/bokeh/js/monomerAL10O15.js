(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("b8399249-2cfe-4e13-84ed-614c06d30475");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'b8399249-2cfe-4e13-84ed-614c06d30475' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"73487b1c-21f7-4bac-8dca-2229f154e5ba":{"roots":{"references":[{"attributes":{"plot":null,"text":""},"id":"309fe080-6821-48da-9b34-64fda18bedcf","type":"Title"},{"attributes":{"callback":null},"id":"363c3192-b065-4176-978f-965f436c0c98","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"aa3d0735-ed80-421d-9354-524c6c70e2b7","type":"ImageURL"},{"attributes":{"below":[{"id":"de4ea051-63a7-4f28-84bf-3bf49e90a8f2","type":"LinearAxis"}],"left":[{"id":"80e9eb11-603b-4b09-b22e-fcef040feb41","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"de4ea051-63a7-4f28-84bf-3bf49e90a8f2","type":"LinearAxis"},{"id":"8e5812d2-b2e9-44cd-a5ae-744e8d1632ab","type":"Grid"},{"id":"80e9eb11-603b-4b09-b22e-fcef040feb41","type":"LinearAxis"},{"id":"a1484bfe-48d8-40f7-969f-21832bdd7cf9","type":"Grid"},{"id":"2b377fa1-edab-4ff5-8eef-31699de51546","type":"GlyphRenderer"}],"title":{"id":"309fe080-6821-48da-9b34-64fda18bedcf","type":"Title"},"toolbar":{"id":"9797fdf1-bfa8-4fdb-be1d-161ae864925b","type":"Toolbar"},"x_range":{"id":"c7d651c2-0af0-4741-956f-cd47fc40cf2d","type":"Range1d"},"x_scale":{"id":"3a204636-a853-439f-82c8-6aba313366d0","type":"LinearScale"},"y_range":{"id":"363c3192-b065-4176-978f-965f436c0c98","type":"Range1d"},"y_scale":{"id":"08a8f577-8bbd-4fb9-8ab8-72028228d2dd","type":"LinearScale"}},"id":"704778a2-cf25-4c84-baf7-e794227bfd5d","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"c6316bf6-206b-40a6-9ffa-6612de5804ae","type":"UnionRenderers"},{"attributes":{"formatter":{"id":"e94bf7b7-fb83-4f15-9c34-a572e738a8d5","type":"BasicTickFormatter"},"plot":{"id":"704778a2-cf25-4c84-baf7-e794227bfd5d","subtype":"Figure","type":"Plot"},"ticker":{"id":"3a8bd007-46a6-42bb-abdc-4e70c43a80df","type":"BasicTicker"},"visible":false},"id":"80e9eb11-603b-4b09-b22e-fcef040feb41","type":"LinearAxis"},{"attributes":{"callback":null},"id":"c7d651c2-0af0-4741-956f-cd47fc40cf2d","type":"Range1d"},{"attributes":{"data_source":{"id":"9d8c412e-1cda-4cb3-8014-ba2e8f4dd307","type":"ColumnDataSource"},"glyph":{"id":"c2f9ffef-950a-41fc-8a1c-e762f3d917ab","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"aa3d0735-ed80-421d-9354-524c6c70e2b7","type":"ImageURL"},"selection_glyph":null,"view":{"id":"813432f1-7d1c-453e-b650-b3465c2c010a","type":"CDSView"}},"id":"2b377fa1-edab-4ff5-8eef-31699de51546","type":"GlyphRenderer"},{"attributes":{"children":[{"id":"bda21374-658b-4ab7-b9b7-eaa222020ef5","type":"Slider"}]},"id":"7062f15a-7508-4625-a2ba-873678b753ad","type":"WidgetBox"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerAL10O15_000.png"],"url_num":["000"]},"selected":{"id":"e94f787c-a3e5-4e48-bb59-88264569ead2","type":"Selection"},"selection_policy":{"id":"c6316bf6-206b-40a6-9ffa-6612de5804ae","type":"UnionRenderers"}},"id":"9d8c412e-1cda-4cb3-8014-ba2e8f4dd307","type":"ColumnDataSource"},{"attributes":{"dimension":1,"plot":{"id":"704778a2-cf25-4c84-baf7-e794227bfd5d","subtype":"Figure","type":"Plot"},"ticker":{"id":"3a8bd007-46a6-42bb-abdc-4e70c43a80df","type":"BasicTicker"},"visible":false},"id":"a1484bfe-48d8-40f7-969f-21832bdd7cf9","type":"Grid"},{"attributes":{},"id":"3a8bd007-46a6-42bb-abdc-4e70c43a80df","type":"BasicTicker"},{"attributes":{},"id":"677ecbf9-cb15-4f6f-a0d2-8fae4b047bba","type":"BasicTicker"},{"attributes":{"formatter":{"id":"9d38f4e1-7149-48fa-a603-85ea82006d17","type":"BasicTickFormatter"},"plot":{"id":"704778a2-cf25-4c84-baf7-e794227bfd5d","subtype":"Figure","type":"Plot"},"ticker":{"id":"677ecbf9-cb15-4f6f-a0d2-8fae4b047bba","type":"BasicTicker"},"visible":false},"id":"de4ea051-63a7-4f28-84bf-3bf49e90a8f2","type":"LinearAxis"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"c2f9ffef-950a-41fc-8a1c-e762f3d917ab","type":"ImageURL"},{"attributes":{},"id":"3a204636-a853-439f-82c8-6aba313366d0","type":"LinearScale"},{"attributes":{"plot":{"id":"704778a2-cf25-4c84-baf7-e794227bfd5d","subtype":"Figure","type":"Plot"},"ticker":{"id":"677ecbf9-cb15-4f6f-a0d2-8fae4b047bba","type":"BasicTicker"},"visible":false},"id":"8e5812d2-b2e9-44cd-a5ae-744e8d1632ab","type":"Grid"},{"attributes":{"source":{"id":"9d8c412e-1cda-4cb3-8014-ba2e8f4dd307","type":"ColumnDataSource"}},"id":"813432f1-7d1c-453e-b650-b3465c2c010a","type":"CDSView"},{"attributes":{"children":[{"id":"7062f15a-7508-4625-a2ba-873678b753ad","type":"WidgetBox"},{"id":"704778a2-cf25-4c84-baf7-e794227bfd5d","subtype":"Figure","type":"Plot"}]},"id":"c0ea08a0-f94f-4733-b80a-18a5acdcf290","type":"Column"},{"attributes":{"callback":{"id":"6fef58ac-1256-4cb5-b556-cde5e5a36428","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"bda21374-658b-4ab7-b9b7-eaa222020ef5","type":"Slider"},{"attributes":{},"id":"e94bf7b7-fb83-4f15-9c34-a572e738a8d5","type":"BasicTickFormatter"},{"attributes":{},"id":"9d38f4e1-7149-48fa-a603-85ea82006d17","type":"BasicTickFormatter"},{"attributes":{"args":{"source":{"id":"9d8c412e-1cda-4cb3-8014-ba2e8f4dd307","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"6fef58ac-1256-4cb5-b556-cde5e5a36428","type":"CustomJS"},{"attributes":{},"id":"08a8f577-8bbd-4fb9-8ab8-72028228d2dd","type":"LinearScale"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"9797fdf1-bfa8-4fdb-be1d-161ae864925b","type":"Toolbar"},{"attributes":{},"id":"e94f787c-a3e5-4e48-bb59-88264569ead2","type":"Selection"}],"root_ids":["c0ea08a0-f94f-4733-b80a-18a5acdcf290"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"73487b1c-21f7-4bac-8dca-2229f154e5ba","roots":{"c0ea08a0-f94f-4733-b80a-18a5acdcf290":"b8399249-2cfe-4e13-84ed-614c06d30475"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();