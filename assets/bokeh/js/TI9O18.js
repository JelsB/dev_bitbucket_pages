(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("778ddc6c-0b29-4ad9-9001-d30778dea3ba");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '778ddc6c-0b29-4ad9-9001-d30778dea3ba' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"44621e29-477b-420f-929f-2e206e087104":{"roots":{"references":[{"attributes":{"plot":{"id":"a73fe1ca-b36f-4b85-854c-164cb72a27dc","subtype":"Figure","type":"Plot"},"ticker":{"id":"de5db013-0b97-44f5-a780-a3c6921e85a0","type":"BasicTicker"},"visible":false},"id":"575abf14-d028-468c-8916-adc899045810","type":"Grid"},{"attributes":{"dimension":1,"plot":{"id":"a73fe1ca-b36f-4b85-854c-164cb72a27dc","subtype":"Figure","type":"Plot"},"ticker":{"id":"f9defee8-758f-4850-96da-def53b81bd02","type":"BasicTicker"},"visible":false},"id":"d9ebac38-b32d-481d-91b2-c74d6ba9b5e9","type":"Grid"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/TI9O18_000.png"],"url_num":["000"]},"selected":{"id":"d2ffad92-14be-4e4c-b9b0-d08945723936","type":"Selection"},"selection_policy":{"id":"a7442d89-345a-495b-8945-ee2d36768163","type":"UnionRenderers"}},"id":"ffa0fa2e-1365-4217-96df-9a5e9b8dd87c","type":"ColumnDataSource"},{"attributes":{},"id":"f9defee8-758f-4850-96da-def53b81bd02","type":"BasicTicker"},{"attributes":{},"id":"c43352d8-f8f7-4e30-a2c4-0fdd779a07d2","type":"BasicTickFormatter"},{"attributes":{"data_source":{"id":"ffa0fa2e-1365-4217-96df-9a5e9b8dd87c","type":"ColumnDataSource"},"glyph":{"id":"9f5c6fd9-6ba1-40f8-8162-279f689f2f32","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"c3adee6a-3ad6-4891-82d7-17c3e4021352","type":"ImageURL"},"selection_glyph":null,"view":{"id":"2ef80a4e-89fb-4584-b4e5-0c06b3bd276c","type":"CDSView"}},"id":"581c7af9-be64-46cd-8c16-b7cb5bfa0099","type":"GlyphRenderer"},{"attributes":{"args":{"source":{"id":"ffa0fa2e-1365-4217-96df-9a5e9b8dd87c","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"30e05079-506d-49d6-8be6-00d89f54993e","type":"CustomJS"},{"attributes":{},"id":"d2ffad92-14be-4e4c-b9b0-d08945723936","type":"Selection"},{"attributes":{},"id":"b7dc8ff7-735b-449d-b1bc-bc6856b486ad","type":"LinearScale"},{"attributes":{"formatter":{"id":"c43352d8-f8f7-4e30-a2c4-0fdd779a07d2","type":"BasicTickFormatter"},"plot":{"id":"a73fe1ca-b36f-4b85-854c-164cb72a27dc","subtype":"Figure","type":"Plot"},"ticker":{"id":"f9defee8-758f-4850-96da-def53b81bd02","type":"BasicTicker"},"visible":false},"id":"4689a2f1-c648-49cf-8a8d-13b0bbc9e552","type":"LinearAxis"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"9f5c6fd9-6ba1-40f8-8162-279f689f2f32","type":"ImageURL"},{"attributes":{"callback":{"id":"30e05079-506d-49d6-8be6-00d89f54993e","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"50b27c75-9a9c-47e6-818b-7fb605e0ec38","type":"Slider"},{"attributes":{"source":{"id":"ffa0fa2e-1365-4217-96df-9a5e9b8dd87c","type":"ColumnDataSource"}},"id":"2ef80a4e-89fb-4584-b4e5-0c06b3bd276c","type":"CDSView"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"104e4acc-d79e-47ae-9516-c3e27d734f3a","type":"Toolbar"},{"attributes":{},"id":"2ec64b6d-abfd-4e25-9230-515a0c266db2","type":"BasicTickFormatter"},{"attributes":{"callback":null},"id":"7eaa657a-c285-4269-b0b0-dea01f5d9c66","type":"Range1d"},{"attributes":{"callback":null},"id":"1fc37193-17ac-4d30-8e28-7e81fe4ff84e","type":"Range1d"},{"attributes":{},"id":"4ac6eeef-1914-4f73-b47a-e4e9b549616b","type":"LinearScale"},{"attributes":{"formatter":{"id":"2ec64b6d-abfd-4e25-9230-515a0c266db2","type":"BasicTickFormatter"},"plot":{"id":"a73fe1ca-b36f-4b85-854c-164cb72a27dc","subtype":"Figure","type":"Plot"},"ticker":{"id":"de5db013-0b97-44f5-a780-a3c6921e85a0","type":"BasicTicker"},"visible":false},"id":"744d030c-ea54-4e40-b392-fe9ca61f388b","type":"LinearAxis"},{"attributes":{"plot":null,"text":""},"id":"8459fcbe-4e01-4273-a4d7-f72f61b0fc05","type":"Title"},{"attributes":{"children":[{"id":"50b27c75-9a9c-47e6-818b-7fb605e0ec38","type":"Slider"}]},"id":"84d856a7-b56a-4285-9abe-6dad0b7943dd","type":"WidgetBox"},{"attributes":{"children":[{"id":"84d856a7-b56a-4285-9abe-6dad0b7943dd","type":"WidgetBox"},{"id":"a73fe1ca-b36f-4b85-854c-164cb72a27dc","subtype":"Figure","type":"Plot"}]},"id":"013dab48-f8a7-45f6-beea-3888718d35bc","type":"Column"},{"attributes":{"below":[{"id":"744d030c-ea54-4e40-b392-fe9ca61f388b","type":"LinearAxis"}],"left":[{"id":"4689a2f1-c648-49cf-8a8d-13b0bbc9e552","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"744d030c-ea54-4e40-b392-fe9ca61f388b","type":"LinearAxis"},{"id":"575abf14-d028-468c-8916-adc899045810","type":"Grid"},{"id":"4689a2f1-c648-49cf-8a8d-13b0bbc9e552","type":"LinearAxis"},{"id":"d9ebac38-b32d-481d-91b2-c74d6ba9b5e9","type":"Grid"},{"id":"581c7af9-be64-46cd-8c16-b7cb5bfa0099","type":"GlyphRenderer"}],"title":{"id":"8459fcbe-4e01-4273-a4d7-f72f61b0fc05","type":"Title"},"toolbar":{"id":"104e4acc-d79e-47ae-9516-c3e27d734f3a","type":"Toolbar"},"x_range":{"id":"7eaa657a-c285-4269-b0b0-dea01f5d9c66","type":"Range1d"},"x_scale":{"id":"b7dc8ff7-735b-449d-b1bc-bc6856b486ad","type":"LinearScale"},"y_range":{"id":"1fc37193-17ac-4d30-8e28-7e81fe4ff84e","type":"Range1d"},"y_scale":{"id":"4ac6eeef-1914-4f73-b47a-e4e9b549616b","type":"LinearScale"}},"id":"a73fe1ca-b36f-4b85-854c-164cb72a27dc","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"de5db013-0b97-44f5-a780-a3c6921e85a0","type":"BasicTicker"},{"attributes":{},"id":"a7442d89-345a-495b-8945-ee2d36768163","type":"UnionRenderers"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"c3adee6a-3ad6-4891-82d7-17c3e4021352","type":"ImageURL"}],"root_ids":["013dab48-f8a7-45f6-beea-3888718d35bc"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"44621e29-477b-420f-929f-2e206e087104","roots":{"013dab48-f8a7-45f6-beea-3888718d35bc":"778ddc6c-0b29-4ad9-9001-d30778dea3ba"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();