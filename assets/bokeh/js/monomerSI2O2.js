(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("13aaab80-fd72-4dc5-8ead-9bff8d3767b5");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '13aaab80-fd72-4dc5-8ead-9bff8d3767b5' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"d80a9e70-064a-487d-896a-e6f17fb2af2e":{"roots":{"references":[{"attributes":{},"id":"9009781f-802c-4710-8b79-cca7dc61bb30","type":"UnionRenderers"},{"attributes":{"children":[{"id":"c1d13745-2e56-4202-8b97-5a4a7c931f33","type":"WidgetBox"},{"id":"9648eb15-cae2-4fdc-b6af-aecbb48cf1fc","subtype":"Figure","type":"Plot"}]},"id":"fd48f724-ff26-45b4-8373-fb9ca2edc731","type":"Column"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"1a91d95d-f47a-4ad5-909a-094cb3a34261","type":"ImageURL"},{"attributes":{"plot":{"id":"9648eb15-cae2-4fdc-b6af-aecbb48cf1fc","subtype":"Figure","type":"Plot"},"ticker":{"id":"87a8f25e-5b35-4e8b-85aa-695c4f2748c0","type":"BasicTicker"},"visible":false},"id":"fd140e8f-fa6f-4824-8417-62e29057a824","type":"Grid"},{"attributes":{"data_source":{"id":"31bc2473-c9a9-4454-a0a5-e451b8266163","type":"ColumnDataSource"},"glyph":{"id":"1a91d95d-f47a-4ad5-909a-094cb3a34261","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"67ceba91-cdc8-4209-8d6d-ec51adca48cb","type":"ImageURL"},"selection_glyph":null,"view":{"id":"d6917024-e854-4976-abdc-afe833975bb2","type":"CDSView"}},"id":"91334449-7d95-4835-bd4c-f7764f7c3db4","type":"GlyphRenderer"},{"attributes":{},"id":"fd60cb72-6ed1-41bd-9c08-5ebef6ca47eb","type":"BasicTickFormatter"},{"attributes":{},"id":"aab0e029-35af-49c6-ba10-3123df80ddd1","type":"BasicTickFormatter"},{"attributes":{},"id":"4ff095d6-a5fd-465c-909d-1f7137f03cce","type":"BasicTicker"},{"attributes":{"plot":null,"text":""},"id":"66e79a77-bcfa-4dc3-ac3e-b1e3824c82c0","type":"Title"},{"attributes":{"formatter":{"id":"aab0e029-35af-49c6-ba10-3123df80ddd1","type":"BasicTickFormatter"},"plot":{"id":"9648eb15-cae2-4fdc-b6af-aecbb48cf1fc","subtype":"Figure","type":"Plot"},"ticker":{"id":"4ff095d6-a5fd-465c-909d-1f7137f03cce","type":"BasicTicker"},"visible":false},"id":"526c2668-41ad-4347-a9df-5be1bdc0b553","type":"LinearAxis"},{"attributes":{"dimension":1,"plot":{"id":"9648eb15-cae2-4fdc-b6af-aecbb48cf1fc","subtype":"Figure","type":"Plot"},"ticker":{"id":"4ff095d6-a5fd-465c-909d-1f7137f03cce","type":"BasicTicker"},"visible":false},"id":"9b9ea300-9aca-47b3-8fc6-781f92055025","type":"Grid"},{"attributes":{"callback":null},"id":"8b9ae591-8a09-4972-9114-921b062aa221","type":"Range1d"},{"attributes":{"below":[{"id":"1ca3ab60-19ab-456b-8e56-994110a5e26e","type":"LinearAxis"}],"left":[{"id":"526c2668-41ad-4347-a9df-5be1bdc0b553","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"1ca3ab60-19ab-456b-8e56-994110a5e26e","type":"LinearAxis"},{"id":"fd140e8f-fa6f-4824-8417-62e29057a824","type":"Grid"},{"id":"526c2668-41ad-4347-a9df-5be1bdc0b553","type":"LinearAxis"},{"id":"9b9ea300-9aca-47b3-8fc6-781f92055025","type":"Grid"},{"id":"91334449-7d95-4835-bd4c-f7764f7c3db4","type":"GlyphRenderer"}],"title":{"id":"66e79a77-bcfa-4dc3-ac3e-b1e3824c82c0","type":"Title"},"toolbar":{"id":"4882b0f3-7bad-498c-9d53-584839b31e76","type":"Toolbar"},"x_range":{"id":"8b9ae591-8a09-4972-9114-921b062aa221","type":"Range1d"},"x_scale":{"id":"889816a7-e4e7-45ae-b664-3c7a8157db3e","type":"LinearScale"},"y_range":{"id":"329ff96d-fe0c-4686-bbdb-82eefe6d459c","type":"Range1d"},"y_scale":{"id":"9d1c9266-a98a-4b18-aa43-981c845a35c0","type":"LinearScale"}},"id":"9648eb15-cae2-4fdc-b6af-aecbb48cf1fc","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"87a8f25e-5b35-4e8b-85aa-695c4f2748c0","type":"BasicTicker"},{"attributes":{"source":{"id":"31bc2473-c9a9-4454-a0a5-e451b8266163","type":"ColumnDataSource"}},"id":"d6917024-e854-4976-abdc-afe833975bb2","type":"CDSView"},{"attributes":{"children":[{"id":"848ad8e2-a96b-463b-b036-b6c65a50d4b5","type":"Slider"}]},"id":"c1d13745-2e56-4202-8b97-5a4a7c931f33","type":"WidgetBox"},{"attributes":{},"id":"889816a7-e4e7-45ae-b664-3c7a8157db3e","type":"LinearScale"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerSI2O2_000.png"],"url_num":["000"]},"selected":{"id":"0d751337-2cc2-4fc3-9613-79112a0b38c3","type":"Selection"},"selection_policy":{"id":"9009781f-802c-4710-8b79-cca7dc61bb30","type":"UnionRenderers"}},"id":"31bc2473-c9a9-4454-a0a5-e451b8266163","type":"ColumnDataSource"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"4882b0f3-7bad-498c-9d53-584839b31e76","type":"Toolbar"},{"attributes":{},"id":"9d1c9266-a98a-4b18-aa43-981c845a35c0","type":"LinearScale"},{"attributes":{"args":{"source":{"id":"31bc2473-c9a9-4454-a0a5-e451b8266163","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"8442d017-ab51-4dcd-8e3a-cc887f552457","type":"CustomJS"},{"attributes":{"callback":{"id":"8442d017-ab51-4dcd-8e3a-cc887f552457","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"848ad8e2-a96b-463b-b036-b6c65a50d4b5","type":"Slider"},{"attributes":{"formatter":{"id":"fd60cb72-6ed1-41bd-9c08-5ebef6ca47eb","type":"BasicTickFormatter"},"plot":{"id":"9648eb15-cae2-4fdc-b6af-aecbb48cf1fc","subtype":"Figure","type":"Plot"},"ticker":{"id":"87a8f25e-5b35-4e8b-85aa-695c4f2748c0","type":"BasicTicker"},"visible":false},"id":"1ca3ab60-19ab-456b-8e56-994110a5e26e","type":"LinearAxis"},{"attributes":{},"id":"0d751337-2cc2-4fc3-9613-79112a0b38c3","type":"Selection"},{"attributes":{"callback":null},"id":"329ff96d-fe0c-4686-bbdb-82eefe6d459c","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"67ceba91-cdc8-4209-8d6d-ec51adca48cb","type":"ImageURL"}],"root_ids":["fd48f724-ff26-45b4-8373-fb9ca2edc731"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"d80a9e70-064a-487d-896a-e6f17fb2af2e","roots":{"fd48f724-ff26-45b4-8373-fb9ca2edc731":"13aaab80-fd72-4dc5-8ead-9bff8d3767b5"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();