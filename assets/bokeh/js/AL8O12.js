(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("9f0df5e7-ae9e-45ba-bee3-0b11fcab6635");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '9f0df5e7-ae9e-45ba-bee3-0b11fcab6635' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"f4122cb1-5ba2-46ea-8b92-078d29cba5a6":{"roots":{"references":[{"attributes":{},"id":"700675bb-548e-4bbb-8988-9275de48a25d","type":"LinearScale"},{"attributes":{},"id":"23a38fa7-d1b7-4360-b73a-c4987a0a3fd6","type":"UnionRenderers"},{"attributes":{"args":{"source":{"id":"0501457e-dc95-4b71-9807-344f953007e2","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"766e3ca4-298a-4f76-99a5-27c641f0b3fb","type":"CustomJS"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"573373d2-1968-47f8-b492-127968b210f8","type":"Toolbar"},{"attributes":{},"id":"5574ab17-d5f1-438e-a6ad-6c0bb584dec0","type":"Selection"},{"attributes":{"plot":{"id":"4ef210da-a4b1-4a78-9335-687406d58382","subtype":"Figure","type":"Plot"},"ticker":{"id":"6f5570d3-2c06-4b8f-93b5-a54fc099f232","type":"BasicTicker"},"visible":false},"id":"7ab55bba-2b85-4d35-b312-ca8cd9355233","type":"Grid"},{"attributes":{"children":[{"id":"1a90cf4f-0a2d-4440-adb1-82466e319fcb","type":"WidgetBox"},{"id":"4ef210da-a4b1-4a78-9335-687406d58382","subtype":"Figure","type":"Plot"}]},"id":"9aab8b51-3555-40a2-b8a9-23274ec0ef66","type":"Column"},{"attributes":{"children":[{"id":"153f868a-fcf4-44ee-b499-f92887189176","type":"Slider"}]},"id":"1a90cf4f-0a2d-4440-adb1-82466e319fcb","type":"WidgetBox"},{"attributes":{},"id":"d9457f70-0d95-41c6-adee-b83e78cc0c6f","type":"BasicTickFormatter"},{"attributes":{"callback":null},"id":"3a17212d-9016-44b2-a2e8-1af762517b3d","type":"Range1d"},{"attributes":{},"id":"6f5570d3-2c06-4b8f-93b5-a54fc099f232","type":"BasicTicker"},{"attributes":{"formatter":{"id":"820c4dba-ad07-461e-8452-2d9bfc4c0a8f","type":"BasicTickFormatter"},"plot":{"id":"4ef210da-a4b1-4a78-9335-687406d58382","subtype":"Figure","type":"Plot"},"ticker":{"id":"440b6839-dcbd-448d-80fe-78106c55aae1","type":"BasicTicker"},"visible":false},"id":"44a69f9a-9034-4ca4-a5f9-ddeb72279088","type":"LinearAxis"},{"attributes":{"callback":{"id":"766e3ca4-298a-4f76-99a5-27c641f0b3fb","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"153f868a-fcf4-44ee-b499-f92887189176","type":"Slider"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"78c7dd36-c14c-44ac-9ea7-5ef44458549a","type":"ImageURL"},{"attributes":{"below":[{"id":"d490f686-4d99-435e-a094-0bd5faa94eb6","type":"LinearAxis"}],"left":[{"id":"44a69f9a-9034-4ca4-a5f9-ddeb72279088","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"d490f686-4d99-435e-a094-0bd5faa94eb6","type":"LinearAxis"},{"id":"7ab55bba-2b85-4d35-b312-ca8cd9355233","type":"Grid"},{"id":"44a69f9a-9034-4ca4-a5f9-ddeb72279088","type":"LinearAxis"},{"id":"a1d775f9-2951-4cf7-8ee1-daafe1fc5012","type":"Grid"},{"id":"7d93b897-4dd0-4181-978c-e376a5da6203","type":"GlyphRenderer"}],"title":{"id":"1195a6aa-7f8a-44c1-bbd4-d633aa6fdd4b","type":"Title"},"toolbar":{"id":"573373d2-1968-47f8-b492-127968b210f8","type":"Toolbar"},"x_range":{"id":"3a17212d-9016-44b2-a2e8-1af762517b3d","type":"Range1d"},"x_scale":{"id":"fd8c28cd-f7c3-413e-a630-7b7991fc2cef","type":"LinearScale"},"y_range":{"id":"835fed84-f10f-446e-bbc0-f754f21930d9","type":"Range1d"},"y_scale":{"id":"700675bb-548e-4bbb-8988-9275de48a25d","type":"LinearScale"}},"id":"4ef210da-a4b1-4a78-9335-687406d58382","subtype":"Figure","type":"Plot"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"d3fa7d91-cdfb-4525-bf62-5992d894a3f1","type":"ImageURL"},{"attributes":{"data_source":{"id":"0501457e-dc95-4b71-9807-344f953007e2","type":"ColumnDataSource"},"glyph":{"id":"d3fa7d91-cdfb-4525-bf62-5992d894a3f1","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"78c7dd36-c14c-44ac-9ea7-5ef44458549a","type":"ImageURL"},"selection_glyph":null,"view":{"id":"fa52ff24-0614-44f9-aca7-e034c2828a91","type":"CDSView"}},"id":"7d93b897-4dd0-4181-978c-e376a5da6203","type":"GlyphRenderer"},{"attributes":{"callback":null},"id":"835fed84-f10f-446e-bbc0-f754f21930d9","type":"Range1d"},{"attributes":{"source":{"id":"0501457e-dc95-4b71-9807-344f953007e2","type":"ColumnDataSource"}},"id":"fa52ff24-0614-44f9-aca7-e034c2828a91","type":"CDSView"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/AL8O12_000.png"],"url_num":["000"]},"selected":{"id":"5574ab17-d5f1-438e-a6ad-6c0bb584dec0","type":"Selection"},"selection_policy":{"id":"23a38fa7-d1b7-4360-b73a-c4987a0a3fd6","type":"UnionRenderers"}},"id":"0501457e-dc95-4b71-9807-344f953007e2","type":"ColumnDataSource"},{"attributes":{"plot":null,"text":""},"id":"1195a6aa-7f8a-44c1-bbd4-d633aa6fdd4b","type":"Title"},{"attributes":{},"id":"fd8c28cd-f7c3-413e-a630-7b7991fc2cef","type":"LinearScale"},{"attributes":{},"id":"440b6839-dcbd-448d-80fe-78106c55aae1","type":"BasicTicker"},{"attributes":{},"id":"820c4dba-ad07-461e-8452-2d9bfc4c0a8f","type":"BasicTickFormatter"},{"attributes":{"formatter":{"id":"d9457f70-0d95-41c6-adee-b83e78cc0c6f","type":"BasicTickFormatter"},"plot":{"id":"4ef210da-a4b1-4a78-9335-687406d58382","subtype":"Figure","type":"Plot"},"ticker":{"id":"6f5570d3-2c06-4b8f-93b5-a54fc099f232","type":"BasicTicker"},"visible":false},"id":"d490f686-4d99-435e-a094-0bd5faa94eb6","type":"LinearAxis"},{"attributes":{"dimension":1,"plot":{"id":"4ef210da-a4b1-4a78-9335-687406d58382","subtype":"Figure","type":"Plot"},"ticker":{"id":"440b6839-dcbd-448d-80fe-78106c55aae1","type":"BasicTicker"},"visible":false},"id":"a1d775f9-2951-4cf7-8ee1-daafe1fc5012","type":"Grid"}],"root_ids":["9aab8b51-3555-40a2-b8a9-23274ec0ef66"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"f4122cb1-5ba2-46ea-8b92-078d29cba5a6","roots":{"9aab8b51-3555-40a2-b8a9-23274ec0ef66":"9f0df5e7-ae9e-45ba-bee3-0b11fcab6635"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();