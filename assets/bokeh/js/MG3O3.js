(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("38595d0d-f3ec-4f9a-adb7-f5e47c32d8fc");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '38595d0d-f3ec-4f9a-adb7-f5e47c32d8fc' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"c298e911-37d1-41e6-81f2-c7aaed081d5d":{"roots":{"references":[{"attributes":{"args":{"source":{"id":"ac741cd7-5f71-4ae2-ab21-8013acabb260","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"633dd1ba-b86b-4b9a-842a-60b405be3200","type":"CustomJS"},{"attributes":{"plot":{"id":"f7733e5e-4554-412a-8927-08002882f05d","subtype":"Figure","type":"Plot"},"ticker":{"id":"7cbe8a86-7c9b-40a8-9028-1ec61997ede1","type":"BasicTicker"},"visible":false},"id":"5088c41e-9074-417d-a258-bcf7ae81cdbf","type":"Grid"},{"attributes":{},"id":"48d9225d-4878-4eb7-98bf-c29ca3a21cbb","type":"LinearScale"},{"attributes":{},"id":"4cac82da-7367-4911-ade1-4aa691b88618","type":"LinearScale"},{"attributes":{},"id":"68e91083-e78f-43fd-b909-d5686851fdac","type":"BasicTicker"},{"attributes":{"dimension":1,"plot":{"id":"f7733e5e-4554-412a-8927-08002882f05d","subtype":"Figure","type":"Plot"},"ticker":{"id":"68e91083-e78f-43fd-b909-d5686851fdac","type":"BasicTicker"},"visible":false},"id":"2c07cafd-a5b5-49e6-9d27-09415c7fec1b","type":"Grid"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"6e83cd4e-4f65-4588-9d49-6d9e45b7d8b3","type":"Toolbar"},{"attributes":{},"id":"ba2bd8c2-1a1c-4376-b272-72cc6326d2fd","type":"BasicTickFormatter"},{"attributes":{},"id":"7cbe8a86-7c9b-40a8-9028-1ec61997ede1","type":"BasicTicker"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/MG3O3_000.png"],"url_num":["000"]},"selected":{"id":"c7eca946-2b03-4670-a2cb-0670104bda09","type":"Selection"},"selection_policy":{"id":"97deb45c-2450-4abc-be79-0a0d1476421f","type":"UnionRenderers"}},"id":"ac741cd7-5f71-4ae2-ab21-8013acabb260","type":"ColumnDataSource"},{"attributes":{"plot":null,"text":""},"id":"101dfe28-43af-41c6-b745-d9e7ae5281a4","type":"Title"},{"attributes":{"children":[{"id":"a9ba9080-fccb-475a-b24e-8c1e95f68591","type":"WidgetBox"},{"id":"f7733e5e-4554-412a-8927-08002882f05d","subtype":"Figure","type":"Plot"}]},"id":"c5b6d038-a9f4-4c86-8367-b5bdb32cca9a","type":"Column"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"444f7051-db09-4a11-a60d-305a3fd0b3c0","type":"ImageURL"},{"attributes":{},"id":"23ee1a46-9507-4a50-a3fb-f9390a67aec8","type":"BasicTickFormatter"},{"attributes":{"formatter":{"id":"23ee1a46-9507-4a50-a3fb-f9390a67aec8","type":"BasicTickFormatter"},"plot":{"id":"f7733e5e-4554-412a-8927-08002882f05d","subtype":"Figure","type":"Plot"},"ticker":{"id":"7cbe8a86-7c9b-40a8-9028-1ec61997ede1","type":"BasicTicker"},"visible":false},"id":"5c80a952-55e4-4805-bcc2-6fe6b302a108","type":"LinearAxis"},{"attributes":{"source":{"id":"ac741cd7-5f71-4ae2-ab21-8013acabb260","type":"ColumnDataSource"}},"id":"8a21d72c-9163-46f3-8fd2-00335b146d08","type":"CDSView"},{"attributes":{"children":[{"id":"e2e4def9-3975-4d32-b378-d261dba9a7e5","type":"Slider"}]},"id":"a9ba9080-fccb-475a-b24e-8c1e95f68591","type":"WidgetBox"},{"attributes":{},"id":"c7eca946-2b03-4670-a2cb-0670104bda09","type":"Selection"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"b23d8a50-0154-48c3-9059-672a306ac00c","type":"ImageURL"},{"attributes":{"callback":{"id":"633dd1ba-b86b-4b9a-842a-60b405be3200","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"e2e4def9-3975-4d32-b378-d261dba9a7e5","type":"Slider"},{"attributes":{"below":[{"id":"5c80a952-55e4-4805-bcc2-6fe6b302a108","type":"LinearAxis"}],"left":[{"id":"ec772739-64fd-4572-99dd-71e0a772f506","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"5c80a952-55e4-4805-bcc2-6fe6b302a108","type":"LinearAxis"},{"id":"5088c41e-9074-417d-a258-bcf7ae81cdbf","type":"Grid"},{"id":"ec772739-64fd-4572-99dd-71e0a772f506","type":"LinearAxis"},{"id":"2c07cafd-a5b5-49e6-9d27-09415c7fec1b","type":"Grid"},{"id":"b01335de-916f-43fa-a089-d93d07f9694d","type":"GlyphRenderer"}],"title":{"id":"101dfe28-43af-41c6-b745-d9e7ae5281a4","type":"Title"},"toolbar":{"id":"6e83cd4e-4f65-4588-9d49-6d9e45b7d8b3","type":"Toolbar"},"x_range":{"id":"e7ba55f6-1b55-47c6-be4b-a880f40f8e2e","type":"Range1d"},"x_scale":{"id":"48d9225d-4878-4eb7-98bf-c29ca3a21cbb","type":"LinearScale"},"y_range":{"id":"f6de8c87-926a-4589-9936-1865af75325d","type":"Range1d"},"y_scale":{"id":"4cac82da-7367-4911-ade1-4aa691b88618","type":"LinearScale"}},"id":"f7733e5e-4554-412a-8927-08002882f05d","subtype":"Figure","type":"Plot"},{"attributes":{"formatter":{"id":"ba2bd8c2-1a1c-4376-b272-72cc6326d2fd","type":"BasicTickFormatter"},"plot":{"id":"f7733e5e-4554-412a-8927-08002882f05d","subtype":"Figure","type":"Plot"},"ticker":{"id":"68e91083-e78f-43fd-b909-d5686851fdac","type":"BasicTicker"},"visible":false},"id":"ec772739-64fd-4572-99dd-71e0a772f506","type":"LinearAxis"},{"attributes":{"callback":null},"id":"f6de8c87-926a-4589-9936-1865af75325d","type":"Range1d"},{"attributes":{"data_source":{"id":"ac741cd7-5f71-4ae2-ab21-8013acabb260","type":"ColumnDataSource"},"glyph":{"id":"444f7051-db09-4a11-a60d-305a3fd0b3c0","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"b23d8a50-0154-48c3-9059-672a306ac00c","type":"ImageURL"},"selection_glyph":null,"view":{"id":"8a21d72c-9163-46f3-8fd2-00335b146d08","type":"CDSView"}},"id":"b01335de-916f-43fa-a089-d93d07f9694d","type":"GlyphRenderer"},{"attributes":{"callback":null},"id":"e7ba55f6-1b55-47c6-be4b-a880f40f8e2e","type":"Range1d"},{"attributes":{},"id":"97deb45c-2450-4abc-be79-0a0d1476421f","type":"UnionRenderers"}],"root_ids":["c5b6d038-a9f4-4c86-8367-b5bdb32cca9a"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"c298e911-37d1-41e6-81f2-c7aaed081d5d","roots":{"c5b6d038-a9f4-4c86-8367-b5bdb32cca9a":"38595d0d-f3ec-4f9a-adb7-f5e47c32d8fc"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();