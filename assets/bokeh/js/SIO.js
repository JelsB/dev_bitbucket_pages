(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("e0a7b0d9-f706-41b3-be58-1e078346b043");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'e0a7b0d9-f706-41b3-be58-1e078346b043' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"9e971adf-c692-4b4e-a414-712d035d14d4":{"roots":{"references":[{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"2769b125-5f1a-4cf3-95e3-ac886b9f1d5f","type":"Toolbar"},{"attributes":{},"id":"edf31e07-6a3a-4c4f-845a-e5072ddbc4c2","type":"BasicTickFormatter"},{"attributes":{},"id":"b654c1f6-9190-49cf-8844-117bd072c7c0","type":"BasicTickFormatter"},{"attributes":{"callback":null},"id":"a6e967cd-1b83-4192-b29c-91f65ad4fa9d","type":"Range1d"},{"attributes":{"args":{"source":{"id":"68c74823-a2a0-411f-892c-a1e4f63e18d0","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"0bec8630-0c9f-4495-97fd-2e9721e6445c","type":"CustomJS"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"41743a82-5d45-4338-a4b1-ee4b63e321a5","type":"ImageURL"},{"attributes":{"children":[{"id":"32370a5a-28f5-4928-baff-95a5d060ca83","type":"Slider"}]},"id":"51a13382-3e8e-478f-af85-ac2036f7e95a","type":"WidgetBox"},{"attributes":{"formatter":{"id":"b654c1f6-9190-49cf-8844-117bd072c7c0","type":"BasicTickFormatter"},"plot":{"id":"50963802-716a-4875-afb9-e78117faeca1","subtype":"Figure","type":"Plot"},"ticker":{"id":"1e4405c7-d015-4c5f-a968-35735cc04301","type":"BasicTicker"},"visible":false},"id":"1cd004bf-3c1a-4078-96a0-55e8731e592d","type":"LinearAxis"},{"attributes":{"formatter":{"id":"edf31e07-6a3a-4c4f-845a-e5072ddbc4c2","type":"BasicTickFormatter"},"plot":{"id":"50963802-716a-4875-afb9-e78117faeca1","subtype":"Figure","type":"Plot"},"ticker":{"id":"d1feab45-61fe-4d15-a0f9-8ca54716522a","type":"BasicTicker"},"visible":false},"id":"16828e26-df55-4fdd-8ec2-ee9cf1c07a27","type":"LinearAxis"},{"attributes":{"data_source":{"id":"68c74823-a2a0-411f-892c-a1e4f63e18d0","type":"ColumnDataSource"},"glyph":{"id":"855210fb-499b-40c5-b6c5-cf4074efa251","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"41743a82-5d45-4338-a4b1-ee4b63e321a5","type":"ImageURL"},"selection_glyph":null,"view":{"id":"9be3f7ad-95d9-490a-a6f5-2358778f001e","type":"CDSView"}},"id":"d1df2bf2-9bd9-4660-95b6-adfbbc2fa473","type":"GlyphRenderer"},{"attributes":{"below":[{"id":"1cd004bf-3c1a-4078-96a0-55e8731e592d","type":"LinearAxis"}],"left":[{"id":"16828e26-df55-4fdd-8ec2-ee9cf1c07a27","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"1cd004bf-3c1a-4078-96a0-55e8731e592d","type":"LinearAxis"},{"id":"677ca699-4a10-412f-94b5-aa849983c65d","type":"Grid"},{"id":"16828e26-df55-4fdd-8ec2-ee9cf1c07a27","type":"LinearAxis"},{"id":"94e692d1-dc7c-43eb-8e32-2f4e7949947e","type":"Grid"},{"id":"d1df2bf2-9bd9-4660-95b6-adfbbc2fa473","type":"GlyphRenderer"}],"title":{"id":"4fe90ae1-98b1-4789-98b7-86df06477587","type":"Title"},"toolbar":{"id":"2769b125-5f1a-4cf3-95e3-ac886b9f1d5f","type":"Toolbar"},"x_range":{"id":"8af097f9-6164-4016-9481-eab9c8c1984d","type":"Range1d"},"x_scale":{"id":"e5f91bb2-32d5-4ced-9701-b91c940e1a9d","type":"LinearScale"},"y_range":{"id":"a6e967cd-1b83-4192-b29c-91f65ad4fa9d","type":"Range1d"},"y_scale":{"id":"841d198e-6fc1-45c6-93b0-5ee73d813768","type":"LinearScale"}},"id":"50963802-716a-4875-afb9-e78117faeca1","subtype":"Figure","type":"Plot"},{"attributes":{"dimension":1,"plot":{"id":"50963802-716a-4875-afb9-e78117faeca1","subtype":"Figure","type":"Plot"},"ticker":{"id":"d1feab45-61fe-4d15-a0f9-8ca54716522a","type":"BasicTicker"},"visible":false},"id":"94e692d1-dc7c-43eb-8e32-2f4e7949947e","type":"Grid"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/SIO_000.png"],"url_num":["000"]},"selected":{"id":"12b8892e-b859-4432-b41d-9c6635c18101","type":"Selection"},"selection_policy":{"id":"52b189d4-2e4c-4588-aac0-016774cb5498","type":"UnionRenderers"}},"id":"68c74823-a2a0-411f-892c-a1e4f63e18d0","type":"ColumnDataSource"},{"attributes":{"children":[{"id":"51a13382-3e8e-478f-af85-ac2036f7e95a","type":"WidgetBox"},{"id":"50963802-716a-4875-afb9-e78117faeca1","subtype":"Figure","type":"Plot"}]},"id":"6185d626-8abb-4cdb-9588-2a222be8ba3e","type":"Column"},{"attributes":{},"id":"d1feab45-61fe-4d15-a0f9-8ca54716522a","type":"BasicTicker"},{"attributes":{},"id":"841d198e-6fc1-45c6-93b0-5ee73d813768","type":"LinearScale"},{"attributes":{},"id":"1e4405c7-d015-4c5f-a968-35735cc04301","type":"BasicTicker"},{"attributes":{"plot":{"id":"50963802-716a-4875-afb9-e78117faeca1","subtype":"Figure","type":"Plot"},"ticker":{"id":"1e4405c7-d015-4c5f-a968-35735cc04301","type":"BasicTicker"},"visible":false},"id":"677ca699-4a10-412f-94b5-aa849983c65d","type":"Grid"},{"attributes":{"callback":{"id":"0bec8630-0c9f-4495-97fd-2e9721e6445c","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"32370a5a-28f5-4928-baff-95a5d060ca83","type":"Slider"},{"attributes":{"callback":null},"id":"8af097f9-6164-4016-9481-eab9c8c1984d","type":"Range1d"},{"attributes":{},"id":"52b189d4-2e4c-4588-aac0-016774cb5498","type":"UnionRenderers"},{"attributes":{},"id":"12b8892e-b859-4432-b41d-9c6635c18101","type":"Selection"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"855210fb-499b-40c5-b6c5-cf4074efa251","type":"ImageURL"},{"attributes":{"source":{"id":"68c74823-a2a0-411f-892c-a1e4f63e18d0","type":"ColumnDataSource"}},"id":"9be3f7ad-95d9-490a-a6f5-2358778f001e","type":"CDSView"},{"attributes":{"plot":null,"text":""},"id":"4fe90ae1-98b1-4789-98b7-86df06477587","type":"Title"},{"attributes":{},"id":"e5f91bb2-32d5-4ced-9701-b91c940e1a9d","type":"LinearScale"}],"root_ids":["6185d626-8abb-4cdb-9588-2a222be8ba3e"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"9e971adf-c692-4b4e-a414-712d035d14d4","roots":{"6185d626-8abb-4cdb-9588-2a222be8ba3e":"e0a7b0d9-f706-41b3-be58-1e078346b043"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();